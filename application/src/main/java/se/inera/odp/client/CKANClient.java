package se.inera.odp.client;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import se.inera.odp.core.exception.ODPException;
import se.inera.odp.core.utils.ClientUtil;

@Service
public class CKANClient {
	Logger logger = LoggerFactory.getLogger(CKANClient.class);

	// URL for creating a new datastore in CKAN
	@Value("${ckan.datastore.url}/action/datastore_create")
	private String CKAN_DATASTORE_CREATE_URL;
		
	// URL for deleting a resource from CKAN
	@Value("${ckan.datastore.url}/action/resource_delete")
	private String CKAN_RESOURCE_DELETE_URL;
	
	// URL for searching for a resource in CKAN
	@Value("${ckan.datastore.url}/action/resource_search?query=hash:")
	private String CKAN_RESOURCE_SEARCH_URL;
	
	// The maximum number of posts to retrieve from a CKAN resource in one single request.
	@Value("${ckan.datastore.search.limit}")
	private String CKAN_DATASTORE_SEARCH_LIMIT;
	
	// URL for searching CKAN's datastore
	@Value("${ckan.datastore.url}/action/datastore_search")
	private String CKAN_DATASTORE_SEARCH_URL;
		
	// URL for updating a datastore in CKAN
	@Value("${ckan.datastore.url}/action/datastore_upsert")
	private String CKAN_DATASTORE_UPDATE_URL;

	// URL for deleting a data in CKAN
	@Value("${ckan.datastore.url}/action/datastore_delete")
	private String CKAN_DATASTORE_DELETE_URL;

	// URL used for displaying files in a certain dataset in CKAN
	@Value("${ckan.datastore.url}/action/package_show?id={id}&limit={limit}")
	private String CKAN_PACKAGE_SHOW_URL;
	
	// URL used for displaying files in a certain dataset in CKAN
	@Value("${ckan.datastore.url}/action/package_search?include_private=true&fq=name:{name}")
	private String CKAN_PACKAGE_SEARCH_URL;
	
	// URL for deleting a data in CKAN
	@Value("${ckan.datastore.url}/action/user_list")
	private String CKAN_USER_LIST_URL;
	
	// URL for deleting a data in CKAN
	@Value("${ckan.datastore.url}/action/user_show")
	private String CKAN_USER_SHOW_URL;

	private WebClient webClient;

	@Autowired
	public CKANClient(WebClient webclient){
		this.webClient = webclient;
	}

	public CKANClient(String baseUrl) {
		this.webClient = WebClient.create(baseUrl);
	}

	private static <T> void catchErrors(ResponseEntity<T> clientResponse) {
		HttpStatus httpStatus = clientResponse.getStatusCode();
		if (!httpStatus.is2xxSuccessful()) {
			throw new ODPException(httpStatus, "Error encountered");
		}
	}

	public <T> ResponseEntity<T> getResource(String auth, String id, Class<T> clazz) {
		if(id == null)
			return null;

		return webClient.get()
			.uri(CKAN_PACKAGE_SHOW_URL, id, CKAN_DATASTORE_SEARCH_LIMIT)
			.header("authorization", auth)
			.exchange()
			.flatMap(response -> response.toEntity(clazz))
			.doOnError(e -> logger.error(e.getMessage()))
			.doOnSuccess(CKANClient::catchErrors)
			.block();
	}

	// http://localhost:5000/api/3/action/package_search?include_private=true&fq=odpid:nkk
	public <T> ResponseEntity<T> getResourceByOdpId(String auth, String id, Class<T> clazz) {
		if(id == null)
			return null;

		return webClient.get()
			.uri(CKAN_PACKAGE_SEARCH_URL, id)
			.header("authorization", auth)
			.exchange()
			.flatMap(response -> response.toEntity(clazz))
			.doOnError(e -> logger.error(e.getMessage()))
			.doOnSuccess(CKANClient::catchErrors)
			.block();
	}

		
		
	public <T> ResponseEntity<T> getData(String auth, String id, Map<String, String> params, Class<T> clazz) {
		if(id == null)
			return null;
		
		if(params == null)
			params = new HashMap<String, String>();
		
		params.put("id", id);
		params.putIfAbsent("limit", CKAN_DATASTORE_SEARCH_LIMIT);
			
		URI uri = ClientUtil.createUriWithParams(CKAN_DATASTORE_SEARCH_URL, params);

		return webClient.get()
			.uri(uri)
			.header("authorization", auth)
			.exchange()
			.flatMap(response -> response.toEntity(clazz))
			.doOnError(e -> logger.error(e.getMessage()))
			.doOnSuccess(CKANClient::catchErrors)
			.block();
	}
	
	public ResponseEntity<String> getResourceForId(String auth, String hashName) {
		if(hashName == null)
			return null;

		return webClient.get()
			.uri(CKAN_RESOURCE_SEARCH_URL + hashName)
			.header("authorization", auth)
			.exchange()
			.flatMap(response -> response.toEntity(String.class))
			.doOnError(e -> logger.error(e.getMessage()))
			.doOnSuccess(CKANClient::catchErrors)
			.block();
	}

	public ResponseEntity<String> createResource(String auth, String data) {
		return webClient.post()
			.uri(CKAN_DATASTORE_CREATE_URL)
			.header("authorization", auth)
			.body(BodyInserters.fromObject(data))
			.exchange()
			.flatMap(response -> response.toEntity(String.class))
			.doOnError(e -> logger.error(e.getMessage()))
			.doOnSuccess(CKANClient::catchErrors)
			.block();
	}
	
	public void updateResource(String data) {
		webClient.post()
			.uri(CKAN_DATASTORE_UPDATE_URL)
			.body(BodyInserters.fromObject(data))
			.exchange()
			.flatMap(response -> response.toEntity(String.class))
			.doOnError(e -> logger.error(e.getMessage()))
			.doOnSuccess(CKANClient::catchErrors)
			.block();
	}

	public ResponseEntity<String> deleteResource(String auth, String resource_id) {
		
		String resourceToDelete = "{\"id\":\"" + resource_id + "\"}";

		return webClient.post()
			.uri(CKAN_RESOURCE_DELETE_URL)
			.header("authorization", auth)
			.body(BodyInserters.fromObject(resourceToDelete))
			.exchange()
			.flatMap(response -> response.toEntity(String.class))
			.doOnError(e -> logger.error(e.getMessage()))
			.doOnSuccess(CKANClient::catchErrors)
			.block();
	}
	
	public ResponseEntity<String> deleteResource(String auth, String resource_id, String field, String row_id) {
		
		String recordToDelete = "{\"resource_id\": \""
				+ resource_id
				+ "\", \"force\":true,\"filters\": {\""+ field + "\" : \""
				+ row_id
				+ "\"}}";

		return webClient.post()
			.uri(CKAN_DATASTORE_DELETE_URL)
			.header("authorization", auth)
			.body(BodyInserters.fromObject(recordToDelete))
			.exchange()
			.flatMap(response -> response.toEntity(String.class))
			.doOnError(e -> logger.error(e.getMessage()))
			.doOnSuccess(CKANClient::catchErrors)
			.block();
	}
	
	public ResponseEntity<String> updateResource(String auth, String data) {
		return webClient.post()
			.uri(CKAN_DATASTORE_UPDATE_URL)
			.header("authorization", auth)
			.body(BodyInserters.fromObject(data))
			.exchange()
			.flatMap(response -> response.toEntity(String.class))
			.doOnError(e -> logger.error(e.getMessage()))
			.doOnSuccess(CKANClient::catchErrors)
			.block();
	}

	public ResponseEntity<String> listUsers(String auth) {
		return webClient.get()
			.uri(CKAN_USER_LIST_URL)
			.header("authorization", auth)
			.exchange()
			.flatMap(response -> response.toEntity(String.class))
			.doOnError(e -> logger.error(e.getMessage()))
			.doOnSuccess(CKANClient::catchErrors)
			.block();
	}
	
	public ResponseEntity<String> showUser(String auth, String id) {
		return webClient.get()
			.uri(CKAN_USER_SHOW_URL + "?id=" + id + "&include_datasets=true")
			.header("authorization", auth)
			.exchange()
			.flatMap(response -> response.toEntity(String.class))
			.doOnError(e -> logger.error(e.getMessage()))
			.doOnSuccess(CKANClient::catchErrors)
			.block();
	}

}
