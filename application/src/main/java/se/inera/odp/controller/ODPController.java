package se.inera.odp.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import se.inera.odp.core.exception.ODPAuthorizationException;
import se.inera.odp.core.response.ODPResponse;
import se.inera.odp.core.service.ODPServiceInterface;
import se.inera.odp.core.utils.ResponseLoggerMapper;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@RestController
@RequestMapping({""})
@Api(value="api", tags= {"CKAN Api"}, produces="application/json", consumes="application/json")
public class ODPController {

	Logger logger = LoggerFactory.getLogger(ODPController.class);

	private static final String X_ODP_ADAPTER_GET_URL = "x-odp-adapter-get-url";
	
	@Autowired
	private ODPServiceInterface ckanService;

	@Autowired
	private ResponseLoggerMapper responseMapper;

	/**
	 * @deprecated
	 * @param auth
	 * @param url
	 * @param headers
	 * @param dataset_id
	 * @param resource_id
	 * @param params
	 * @return
	 */
	@GetMapping("/get/{dataset_id}/{resource_id}")
	public ResponseEntity<String> getResourceById(
			@RequestHeader(value=AUTHORIZATION, required=false ) String auth, 
			@RequestHeader(value=X_ODP_ADAPTER_GET_URL, required=false ) String url,
			@RequestHeader Map<String,String> headers,
			@PathVariable String dataset_id, @PathVariable String resource_id,
			@RequestParam Map<String,String> params) {
		
		String result = ckanService.getResourceById(dataset_id, resource_id, params, auth, url);
		responseMapper.responseMessage(HttpStatus.OK, "OK", 1);
		return new ResponseEntity<String>(result, HttpStatus.OK);
	}
	
	@GetMapping("/getlist/{dataset_id}/{resource_id}")
	public ResponseEntity<List<?>> getResourceAsList (
			@RequestHeader(value=AUTHORIZATION, required=false ) String auth, 
			@RequestHeader(value=X_ODP_ADAPTER_GET_URL, required=false ) String url,
			@RequestHeader Map<String,String> headers,
			@PathVariable String dataset_id, @PathVariable String resource_id,
			@RequestParam Map<String,String> params) {
		
		List<?> result = ckanService.getResourceAsList(dataset_id, resource_id, params, auth, url);
		responseMapper.responseMessage(HttpStatus.OK, "OK", result.size());
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@GetMapping("/getobject/{dataset_id}/{resource_id}")
	public ResponseEntity<ODPResponse> getResourceAsObject (
			@RequestHeader(value=AUTHORIZATION, required=false ) String auth, 
			@RequestHeader(value=X_ODP_ADAPTER_GET_URL, required=false ) String url,
			@RequestHeader Map<String,String> headers,
			@PathVariable String dataset_id, @PathVariable String resource_id,
			@RequestParam Map<String,String> params) {
		
		ODPResponse result = ckanService.getResourceAsObject(dataset_id, resource_id, params, auth, url);
		responseMapper.responseMessage(HttpStatus.OK, "OK", result.getTotal());
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@PostMapping(value="/save")
	public ResponseEntity<String> createResource(
			@RequestHeader(value=AUTHORIZATION, required=false ) String auth, 
			@RequestBody String data) {
 
		if(auth == null)
			throw new ODPAuthorizationException();
			
		String url = "/api/save";
		String result = ckanService.createResource(auth, data);
		responseMapper.responseMessage(HttpStatus.CREATED, "OK");
		return new ResponseEntity<String>(result, HttpStatus.CREATED);
	}
	
	@GetMapping("/ping")
	public ResponseEntity<String> getPingResponse() {
		return new ResponseEntity<String>("OK", HttpStatus.OK);
	}	
	
	@DeleteMapping("/delete/{dataset_id}/{resource_id}")
	public ResponseEntity<String> deleteResource(
			@RequestHeader(value=AUTHORIZATION) String auth, 
			@PathVariable String dataset_id, @PathVariable String resource_id) throws IOException {

		String response = ckanService.deleteResource(auth, dataset_id, resource_id);
		responseMapper.responseMessage(HttpStatus.OK, "OK");
		return new ResponseEntity<String>(response, HttpStatus.OK);
	}
	
	@DeleteMapping("/delete/{dataset_id}/{resource_id}/{field}/{row_id}")
	public ResponseEntity<String> deleteResource(
			@RequestHeader(value=AUTHORIZATION) String auth, 
			@PathVariable String dataset_id, @PathVariable String resource_id, @PathVariable String field, @PathVariable String row_id) throws IOException {

		String response = ckanService.deleteResource(auth, dataset_id, resource_id, field, row_id);
		responseMapper.responseMessage(HttpStatus.OK, "OK");
		return new ResponseEntity<String>(response, HttpStatus.OK);
	}
	
	@PutMapping("/update/{dataset_id}/{resource_id}")
	public ResponseEntity<String> updateResource(
			@RequestHeader(value=AUTHORIZATION) String auth, 
			@PathVariable String dataset_id, @PathVariable String resource_id,
			@RequestBody String data) throws IOException {

		String response = ckanService.updateResource(auth, dataset_id, resource_id, data);
		responseMapper.responseMessage(HttpStatus.OK, "OK");
		return new ResponseEntity<String>(response, HttpStatus.OK);
	}
	
}
