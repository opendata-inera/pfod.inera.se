package se.inera.odp.service;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import se.inera.odp.client.CKANClient;
import se.inera.odp.core.exception.ODPException;
import se.inera.odp.core.request.CKANError;
import se.inera.odp.core.request.CKANResponse;
import se.inera.odp.core.request.CKANResult;
import se.inera.odp.core.request.LinkType;
import se.inera.odp.core.response.ODPResponse;
import se.inera.odp.core.service.ODPServiceInterface;

import static se.inera.odp.core.exception.ErrorCodes.*;

@Service
public class ODPService implements ODPServiceInterface {

	// Supported ckan query parameters
	private static final ArrayList<String> qparams = new ArrayList<>(
			Arrays.asList(
					"limit", 
					"offset", 
					"fields", 
					"sort"
				));
	
	// Not supported ckan query parameters
	private static final ArrayList<String> uparams = new ArrayList<>(
			Arrays.asList(	
				"resource_id",
				"filters",
				"q",
				"distinct", 
				"plain",
				"language", 
				"include_total", 
				"records_format"
			));
	
	@Autowired
	CKANClient ckanClient;

//	@Autowired
	ObjectMapper mapper;
	
	/* (non-Javadoc)
	 * @see se.inera.odp.service.ODPServiceInterface#setMapper(com.fasterxml.jackson.databind.ObjectMapper)
	 */
	@Override
	@Autowired
	public void setMapper(ObjectMapper mapper){
		this.mapper = mapper;
	}


	/* (non-Javadoc)
	 * @see se.inera.odp.service.ODPServiceInterface#getResourceById(java.lang.String, java.lang.String, java.util.Map, java.lang.String, java.lang.String)
	 */
	@Override
	public String getResourceById(String dataset_id, String resource_name, Map<String, String> params, String auth, String url) {
		
		CKANResult ckanResult = getResourceInternal(dataset_id, resource_name, params, auth, url);

		List<Map<String, ?>> ckanRecords = ckanResult.getRecords();
		
		try {		
			if(ckanResult.getTotal() == ckanRecords.size()) {
				return mapper.writeValueAsString(ckanRecords);				
			} else {			
				ODPResponse odpResponse = new ODPResponse();
				odpResponse.setRecords(ckanRecords);
				reformatLinks(ckanResult.getLinks(), dataset_id, resource_name, url);
				odpResponse.setLinks(ckanResult.getLinks());
				odpResponse.setOffset(ckanResult.getOffset());
				odpResponse.setLimit(ckanResult.getLimit());
				odpResponse.setTotal(ckanResult.getTotal());
				return mapper.writeValueAsString(odpResponse);
			}						
		} catch (IOException e) {
			throw new ODPException(HttpStatus.INTERNAL_SERVER_ERROR, "IOException : " + e.getMessage(), ERROR_CODE_JSON_WRITE);
		}
	}

	private CKANResult getResourceInternal(String dataset_id, String resource_name, Map<String, String> params, String auth, String url) {
		
		String resourceId = fetchResourceId(auth, dataset_id, resource_name);

		// Get result set
		computeQuery(params);
		ResponseEntity<CKANResponse> response = null;
		try {
			response = ckanClient.getData(auth, resourceId, params, CKANResponse.class);
		} catch (RuntimeException e) {
			throw new ODPException(createStatus(response), e.getMessage(), ERROR_CODE_CKAN_DATASTORE_SEARCH);			
		}
		
		CKANResponse ckanResponse = response.getBody();

		if(!ckanResponse.getSuccess()) {
			throw new ODPException(HttpStatus.NOT_FOUND, ckanResponse.getError().getMessage(), ERROR_CODE_CKAN_DATASTORE_SEARCH);
		}
		
		CKANResult ckanResult = ckanResponse.getResult();
		List<Map<String, ?>> ckanRecords = ckanResult.getRecords();
		
		for(Map<String, ?> rec : ckanRecords) {
			rec.remove("_id");
		}
		
		return ckanResult;
	}
	/* (non-Javadoc)
	 * @see se.inera.odp.service.ODPServiceInterface#getResourceAsList(java.lang.String, java.lang.String, java.util.Map, java.lang.String, java.lang.String)
	 */
	@Override
	public List getResourceAsList(String dataset_id, String resource_name, Map<String, String> params, String auth, String url) {
		
		CKANResult ckanResult = getResourceInternal(dataset_id, resource_name, params, auth, url);
		return ckanResult.getRecords();
	}
	
	/* (non-Javadoc)
	 * @see se.inera.odp.service.ODPServiceInterface#getResourceAsObject(java.lang.String, java.lang.String, java.util.Map, java.lang.String, java.lang.String)
	 */
	@Override
	public ODPResponse getResourceAsObject(String dataset_id, String resource_name, Map<String, String> params, String auth, String url) {
		CKANResult ckanResult = getResourceInternal(dataset_id, resource_name, params, auth, url);
		List<Map<String, ?>> ckanRecords = ckanResult.getRecords();
		ODPResponse odpResponse = new ODPResponse();
		odpResponse.setRecords(ckanRecords);
		reformatLinks(ckanResult.getLinks(), dataset_id, resource_name, url);
		odpResponse.setLinks(ckanResult.getLinks());
		odpResponse.setOffset(ckanResult.getOffset());
		odpResponse.setLimit(ckanResult.getLimit());
		odpResponse.setTotal(ckanResult.getTotal());
		return odpResponse;	
	}
 
	private void reformatLinks(LinkType lt, String dataset, String resource, String url) {
		
		if(lt.getStart() != null)
			lt.setStart(modifyUrl(lt.getStart(), url));
			
		if(lt.getNext() != null) {		
			lt.setNext(modifyUrl(lt.getNext(), url));
		}
		if(lt.getPrev() != null) {			
			lt.setPrev(modifyUrl(lt.getPrev(), url));
		}

	}

	private String modifyUrl(String url, String newHost) {
	    MultiValueMap<String, String> parameters =
	            UriComponentsBuilder.fromUriString(url).build().getQueryParams();
	    MultiValueMap<String, String> newParams = new LinkedMultiValueMap<String, String>();
	    parameters.forEach((k,v) -> {if(!"id".equalsIgnoreCase(k)) newParams.put(k, v);});

	    UriComponents uriComponents = UriComponentsBuilder.fromHttpUrl(newHost).queryParams(newParams).build();
	    return uriComponents.toUriString();
	}
	
	/* (non-Javadoc)
	 * @see se.inera.odp.service.ODPServiceInterface#createResource(java.lang.String, java.lang.String)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public String createResource(String auth, String data) {
		Map<String, Object> map;
		try {
			map = mapper.readValue(data, Map.class);
		} catch (IOException e) {
			throw new ODPException(HttpStatus.INTERNAL_SERVER_ERROR, "IOException : " + e.getMessage(), ERROR_CODE_JSON_PARSE);
		}
		Map<String, Object> innerMap = (Map<String, Object>)map.get("resource");
		String resourceName = (String)innerMap.get("name");
		String datasetName = (String)innerMap.get("package_id");
		String oldResourceId = null;
		try {
			oldResourceId = fetchResourceId(auth, datasetName, resourceName);
		}
		catch (ODPException e) {
		}
		if (oldResourceId != null) {
			ckanClient.deleteResource(auth, oldResourceId);
		}
		ResponseEntity<String> response = ckanClient.createResource(auth, data);
		Map<String, ?> resultMap = null;
		String returnString = null;
		try {
			resultMap = mapper.readValue(response.getBody(), Map.class);
			resultMap.remove("help");
			resultMap.remove("result");
			returnString = mapper.writeValueAsString(resultMap);
		} catch (IOException e) {
			throw new ODPException(HttpStatus.INTERNAL_SERVER_ERROR, "IOException : " + e.getMessage(), ERROR_CODE_JSON_WRITE);
		}
		return returnString;
	}
	
	/* (non-Javadoc)
	 * @see se.inera.odp.service.ODPServiceInterface#deleteResource(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public String deleteResource(String auth, String dataset_id, String resource_name) throws IOException {
		String resourceId = fetchResourceId(auth, dataset_id, resource_name);
		ResponseEntity<String> response = null;
		try {
			response = ckanClient.deleteResource(auth, resourceId);
		} catch (RuntimeException e) {
			throw new ODPException(createStatus(response), e.getMessage(), ERROR_CODE_CKAN_DATASTORE_DELETE);			
		}
		
		@SuppressWarnings("unchecked")
		Map<String, ?> map = mapper.readValue(response.getBody(), Map.class);
		map.remove("result");
		map.remove("help");
		return mapper.writeValueAsString(map);
	}

	/* (non-Javadoc)
	 * @see se.inera.odp.service.ODPServiceInterface#deleteResource(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public String deleteResource(String auth, String dataset_id, String resource_name, String field, String row_id) throws IOException {
		String resourceId = fetchResourceId(auth, dataset_id, resource_name);
		ResponseEntity<String> response = null;
		try {
			response = ckanClient.deleteResource(auth, resourceId, field, row_id);
		} catch (RuntimeException e) {
			throw new ODPException(createStatus(response), e.getMessage(), ERROR_CODE_CKAN_DATASTORE_DELETE);			
		}
		
		@SuppressWarnings("unchecked")
		Map<String, ?> map = mapper.readValue(response.getBody(), Map.class);
		map.remove("result");
		map.remove("help");
		return mapper.writeValueAsString(map);
	}

	/* (non-Javadoc)
	 * @see se.inera.odp.service.ODPServiceInterface#updateResource(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public String updateResource(String auth, String dataset_id, String resource_name, String data) throws JsonParseException, JsonMappingException, IOException {
		
		String resourceId = fetchResourceId(auth, dataset_id, resource_name);
			
		StringBuilder strBldr = new StringBuilder();
		strBldr.append("{\"resource_id\": \"" + resourceId + "\", \"force\": true, ");
		int recordsIndex = data.indexOf("\"records\"");
		if(recordsIndex == -1) {
			recordsIndex = 0;
			strBldr.append("\"records\":[");			
			strBldr.append(data);
			strBldr.append("]");			
		} else {
			int lastIndexOfCurlyBrace = data.lastIndexOf("}");
			strBldr.append(data.substring(recordsIndex, lastIndexOfCurlyBrace));
		}
		strBldr.append(", \"method\" : \"upsert\" }");
		
		ResponseEntity<String> response = null;
		try {
			response = ckanClient.updateResource(auth, strBldr.toString());
		} catch (RuntimeException e) {
			throw new ODPException(createStatus(response), e.getMessage(), ERROR_CODE_CKAN_DATASTORE_UPDATE);			
		}			
		
		@SuppressWarnings("unchecked")
		Map<String, ?> map = mapper.readValue(response.getBody(), Map.class);
		map.remove("help");
		map.remove("method");
		map.remove("resource_id");
		map.remove("result");
		return mapper.writeValueAsString(map);
	}

	/*
	 * Private methods
	 */
	
	private void computeQuery(Map<String, String> params) {
		try {
			Map<String, String> filters = new HashMap<>();
			
			Iterator<Map.Entry<String,String>> iter = params.entrySet().iterator();
			while (iter.hasNext()) {
			    Map.Entry<String,String> entry = iter.next();
				if(uparams.contains(entry.getKey())) {
			        iter.remove();		    
				} else if(!qparams.contains(entry.getKey())) {					
					filters.put(entry.getKey(), URLDecoder.decode(entry.getValue(), "UTF-8"));
			        iter.remove();
				}
			}
			
			if(filters.size()>0)
				params.put("filters", mapper.writeValueAsString(filters));
			
		} catch (IOException e) {
			throw new ODPException(HttpStatus.INTERNAL_SERVER_ERROR, "IOException : " + e.getMessage(), ERROR_CODE_JSON_WRITE);
		}
	}
	
	@SuppressWarnings("unchecked")
	private Map<String, ?> createResultAsMap(String value) {
		try {
			Map<String, Object> map = mapper.readValue(value, Map.class);
			
			Boolean success = (Boolean) map.get("success");
			if(!success) {
				CKANError error = new CKANError(map);
				throw new ODPException(HttpStatus.INTERNAL_SERVER_ERROR, error.getMessage(), ERROR_CODE_CKAN_PACKAGE_SHOW);
			}
			return (Map<String, Object>)map.get("result");
			
		} catch(IOException e) {
			throw new ODPException("IOException : " + e.getMessage());			
		}			
	}

	@SuppressWarnings("unchecked")
	private String fetchResourceId(String auth, String dataset_id, String resource_name) {

		ResponseEntity<String> result = null;
		try {
			result = ckanClient.getResourceByOdpId(auth, dataset_id, String.class);
		} catch (HttpClientErrorException e) {			
			throw new ODPException(e.getStatusCode(), e.getMessage(), ERROR_CODE_CKAN_PACKAGE_SHOW);					
		} catch (RuntimeException e) {
			throw new ODPException(createStatus(result), e.getMessage(), ERROR_CODE_CKAN_PACKAGE_SHOW);			
		}
		Map<String, ?> resultMap = createResultAsMap(result.getBody());
		List<Map<String, ?>> results = (List<Map<String, ?>>)resultMap.get("results");
		
		// There should only be one result
		if(results == null || results.size() == 0)
			throw new ODPException(HttpStatus.NOT_FOUND, "Datasetet " + dataset_id + " saknas!", ERROR_CODE_CKAN_PACKAGE_SHOW);
		else if(results.size() > 1)
			throw new ODPException(HttpStatus.INTERNAL_SERVER_ERROR, "Datasetet " + dataset_id + " har dubletter!", ERROR_CODE_CKAN_PACKAGE_SHOW);
			
		Map<String, ?> dataset = (Map<String, ?>) (results.get(0));	
		List<Map<String, String>> resources = (List<Map<String, String>>)dataset.get("resources");	
		
		Optional<Map<String, String>> resource = resources.stream().filter(r -> resource_name.equals(r.get("name"))).findFirst();
	
		if(!resource.isPresent())
			throw new ODPException(HttpStatus.NOT_FOUND, "Resursen " + resource_name + " saknas!", ERROR_CODE_CKAN_PACKAGE_SHOW);
		
		Map<String, String> resourceMap = resource.get();
		
		return resourceMap.get("id");
			
	}
	
	private HttpStatus createStatus(ResponseEntity<?> response) {
		
		if(response == null)
			return HttpStatus.INTERNAL_SERVER_ERROR;
		
		if(response.getStatusCode() == null)
			return HttpStatus.INTERNAL_SERVER_ERROR;
		
		return response.getStatusCode();
	}
}
