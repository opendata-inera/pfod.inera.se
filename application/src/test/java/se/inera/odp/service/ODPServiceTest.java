package se.inera.odp.service;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import com.fasterxml.jackson.databind.ObjectMapper;

import se.inera.odp.client.CKANClient;
import se.inera.odp.core.request.CKANResponse;
import se.inera.odp.core.request.CKANResult;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ODPServiceTest {

	@InjectMocks
	private ODPService odpService;
	
	@Mock
	private CKANClient ckanClient;
	
	@Autowired
	private ObjectMapper mapper;

	@Before
	public void setUp() throws Exception {
		mapper = new ObjectMapper();
		odpService.setMapper(mapper);
		MockitoAnnotations.initMocks(this);
	}

	
	@SuppressWarnings("unchecked")
	@Test
	public void testGetResourceById() throws IOException {

		String resourceString = "{ \"help\": \"http://localhost:5000/api/3/action/help_show?name=package_search\", \"success\": true, \"result\": { \"count\": 1, \"sort\": \"score desc, metadata_modified desc\", \"facets\": {}, \"results\": [ { \"license_title\": \"Creative Commons Attribution\", \"maintainer\": \"\", \"relationships_as_object\": [], \"private\": false, \"maintainer_email\": \"\", \"num_tags\": 1, \"id\": \"249c27c1-3112-43f2-9504-f6230758a3b6\", \"metadata_created\": \"2019-08-27T12:46:10.123557\", \"metadata_modified\": \"2019-10-14T13:37:07.701330\", \"author\": \"\", \"author_email\": \"\", \"state\": \"active\", \"version\": \"\", \"creator_user_id\": \"cf926618-1fbd-4bdb-85f5-7d822f21c6fe\", \"type\": \"dataset\", \"resources\": [ { \"mimetype\": null, \"cache_url\": null, \"hash\": \"\", \"description\": \"\", \"name\": \"abc\", \"format\": \"\", \"url\": \"\", \"datastore_active\": false, \"cache_last_updated\": null, \"package_id\": \"249c27c1-3112-43f2-9504-f6230758a3b6\", \"created\": \"2019-08-27T12:46:49.113026\", \"state\": \"active\", \"mimetype_inner\": null, \"last_modified\": null, \"position\": 0, \"revision_id\": \"33827c80-680b-4ad4-a786-83e0b95f7e89\", \"url_type\": null, \"id\": \"52481432-632b-44b6-aa55-93a922ed0e3f\", \"resource_type\": null, \"size\": null }, { \"mimetype\": null, \"cache_url\": null, \"hash\": \"nkk-v1-documents__\", \"description\": \"\", \"name\": \"nkk-v1-documents\", \"format\": \"\", \"url\": \"http://localhost:5000/datastore/dump/4e91eb91-9c51-44bc-a016-83949c535697\", \"datastore_active\": true, \"cache_last_updated\": null, \"package_id\": \"249c27c1-3112-43f2-9504-f6230758a3b6\", \"created\": \"2019-10-07T09:45:34.816671\", \"state\": \"active\", \"mimetype_inner\": null, \"last_modified\": null, \"position\": 1, \"revision_id\": \"d5303102-a4de-4efb-92ab-56271ec92dfe\", \"url_type\": \"datastore\", \"id\": \"4e91eb91-9c51-44bc-a016-83949c535697\", \"resource_type\": null, \"size\": null } ], \"num_resources\": 2, \"tags\": [ { \"vocabulary_id\": null, \"state\": \"active\", \"display_name\": \"nkk\", \"id\": \"49607753-7046-48be-8ce5-52a6e34e72a4\", \"name\": \"nkk\" } ], \"groups\": [], \"license_id\": \"cc-by\", \"relationships_as_subject\": [], \"organization\": { \"description\": \"\", \"created\": \"2019-06-11T15:03:48.963832\", \"title\": \"Inera\", \"name\": \"inera\", \"is_organization\": true, \"state\": \"active\", \"image_url\": \"\", \"revision_id\": \"cf28f021-40b6-4bbb-9078-60e60f381aa5\", \"type\": \"organization\", \"id\": \"a7d3a8bc-2489-416a-a920-e77d57c70549\", \"approval_status\": \"approved\" }, \"name\": \"nkk\", \"isopen\": true, \"url\": \"\", \"notes\": \"nkk\", \"owner_org\": \"a7d3a8bc-2489-416a-a920-e77d57c70549\", \"extras\": [ { \"key\": \"odpid\", \"value\": \"NKK\" } ], \"license_url\": \"http://www.opendefinition.org/licenses/cc-by\", \"title\": \"NKK\", \"revision_id\": \"7deadcba-59ec-4fd2-8fb2-e832497e735d\" } ], \"search_facets\": {} } }";
	
		ResponseEntity<String> resultEntity = new ResponseEntity<>(resourceString, HttpStatus.OK);
		
		when(ckanClient.getResourceByOdpId(anyString(), anyString(), eq(String.class))).thenReturn(resultEntity);
		
		String mockedGetDataString = "{\"testfield1\":\"test1\",\"testfield2\":\"test2\"}";
		Map<String, ?> map = mapper.readValue(mockedGetDataString, Map.class);
				
		List<Map<String, ?>> ckanRecords = new ArrayList<>();
		ckanRecords.add(map);
		
		CKANResult ckanResult = new CKANResult(); 
		ckanResult.setRecords(ckanRecords);
		ckanResult.setTotal(ckanRecords.size());
		
		CKANResponse ckanResponse = new CKANResponse();
		ckanResponse.setResult(ckanResult);
		ckanResponse.setSuccess(true);
		
		ResponseEntity<CKANResponse> responseEntity = new ResponseEntity<>(ckanResponse, HttpStatus.OK);
		
		when(ckanClient.getData(any(), any(), anyMap(), eq(CKANResponse.class))).thenReturn(responseEntity);
		
		String response = odpService.getResourceById("nkk", "nkk-v1-documents", new HashMap<String, String>(), "5e90db9b-0e4a-4b14-b5f6-755dbffccadb", "");
		
		assertNotNull(response);
		assertEquals(response, "[" + mockedGetDataString + "]");
	}

	@Test
	public void testCreateResource() {
		
		String getResource = "{\"help\": \"http://localhost:5000/api/3/action/help_show?name=package_show\", \"success\": true, \"result\": {\"license_title\": \"Creative Commons Attribution\", \"maintainer\": \"\", \"relationships_as_object\": [], \"private\": false, \"maintainer_email\": \"\", \"num_tags\": 0, \"id\": \"adfb540c-2ee9-4ce6-85e9-bc84be190d0f\", \"metadata_created\": \"2019-07-01T09:28:30.240921\", \"metadata_modified\": \"2019-09-27T10:23:21.782439\", \"author\": \"\", \"author_email\": \"\", \"state\": \"active\", \"version\": \"\", \"creator_user_id\": \"5e9ffcea-0da6-4236-8456-4345138bd8de\", \"type\": \"dataset\", \"resources\": [{\"mimetype\": null, \"cache_url\": null, \"hash\": \"kik-v1-codeSystems__\", \"description\": \"\", \"name\": \"kik-v1-codeSystems\", \"format\": \"\", \"url\": \"http://localhost:5000/datastore/dump/5c8d97b8-1b4f-4734-bbd4-34d6f0e056f0\", \"datastore_active\": true, \"cache_last_updated\": null, \"package_id\": \"adfb540c-2ee9-4ce6-85e9-bc84be190d0f\", \"created\": \"2019-09-26T07:40:39.745390\", \"state\": \"active\", \"mimetype_inner\": null, \"last_modified\": null, \"position\": 0, \"revision_id\": \"9853d430-0fbb-41f1-92a5-1d9d9291743f\", \"url_type\": \"datastore\", \"id\": \"5c8d97b8-1b4f-4734-bbd4-34d6f0e056f0\", \"resource_type\": null, \"size\": null}, {\"mimetype\": null, \"cache_url\": null, \"hash\": \"kik-v1-codes__\", \"description\": \"\", \"name\": \"kik-v1-codes\", \"format\": \"\", \"url\": \"http://localhost:5000/datastore/dump/2216a0d4-4f0d-407c-8fe3-e3741b646711\", \"datastore_active\": true, \"cache_last_updated\": null, \"package_id\": \"adfb540c-2ee9-4ce6-85e9-bc84be190d0f\", \"created\": \"2019-09-27T10:22:57.755422\", \"state\": \"active\", \"mimetype_inner\": null, \"last_modified\": null, \"position\": 1, \"revision_id\": \"75a59988-c31c-4375-aafc-1917ca023609\", \"url_type\": \"datastore\", \"id\": \"2216a0d4-4f0d-407c-8fe3-e3741b646711\", \"resource_type\": null, \"size\": null}, {\"mimetype\": null, \"cache_url\": null, \"hash\": \"kik-v1-measures__\", \"description\": \"\", \"name\": \"kik-v1-measures\", \"format\": \"\", \"url\": \"http://localhost:5000/datastore/dump/f251bad7-3335-4da8-92df-070f2ddd3fe0\", \"datastore_active\": true, \"cache_last_updated\": null, \"package_id\": \"adfb540c-2ee9-4ce6-85e9-bc84be190d0f\", \"created\": \"2019-09-27T10:23:21.612883\", \"state\": \"active\", \"mimetype_inner\": null, \"last_modified\": null, \"position\": 2, \"revision_id\": \"a4c1c868-26a6-4a7e-935c-7e5fd39fa688\", \"url_type\": \"datastore\", \"id\": \"f251bad7-3335-4da8-92df-070f2ddd3fe0\", \"resource_type\": null, \"size\": null}], \"num_resources\": 3, \"tags\": [], \"groups\": [], \"license_id\": \"cc-by\", \"relationships_as_subject\": [], \"organization\": null, \"name\": \"kvalitetsindikatorer\", \"isopen\": true, \"url\": \"\", \"notes\": \"\", \"owner_org\": null, \"extras\": [], \"license_url\": \"http://www.opendefinition.org/licenses/cc-by\", \"title\": \"Kvalitetsindikatorer\", \"revision_id\": \"37878b73-1a95-4124-9218-ab8a3ea5a31d\"}}";
		String createResource = "{\"help\": \"http://localhost:5000/api/3/action/help_show?name=datastore_create\", \"success\": true, \"result\": {\"resource\": {\"url\": \"_datastore_only_resource\", \"mimetype\": null, \"package_id\": \"kvalitetsindikatorer\", \"hash\": \"kik-v1-codes__\", \"name\": \"kik-v1-codes\"}, \"primary_key\": \"id\", \"resource_id\": \"0724b405-8b3c-4dd9-a95b-021208d4b60a\", \"fields\": [{\"type\": \"text\", \"id\": \"id\"}, {\"type\": \"text\", \"id\": \"codeId\"}, {\"type\": \"text\", \"id\": \"displayName\"}, {\"type\": \"text\", \"id\": \"codeSystemOID\"}, {\"type\": \"text\", \"id\": \"definition\"}, {\"type\": \"bool\", \"id\": \"active\"}], \"method\": \"insert\"}}";
		String deleteResource = "{\"help\": \"http://localhost:5000/api/3/action/help_show?name=resource_delete\", \"success\": true, \"result\": null}";		
		
		ResponseEntity<String> responseEntityGetResource = new ResponseEntity<>(getResource, HttpStatus.OK);
		ResponseEntity<String> responseEntityCreateResource = new ResponseEntity<>(createResource, HttpStatus.OK);
		ResponseEntity<String> responseEntityDeleteResource = new ResponseEntity<>(deleteResource, HttpStatus.OK);
		
		when(ckanClient.getResourceByOdpId(any(), any(), eq(String.class))).thenReturn(responseEntityGetResource);
		when(ckanClient.createResource(any(), any())).thenReturn(responseEntityCreateResource);
		when(ckanClient.deleteResource(any(), any())).thenReturn(responseEntityDeleteResource);
		
		String data = "{\n" + 
				"	\"resource\": {\n" + 
				"		\"package_id\": \"kvalitetsindikatorer\",\n" + 
				"		\"name\": \"kik-v1-codes\",\n" + 
				"   		\"hash\": \"kik-v1-codes__\"\n" + 
				"	},\n" + 
				"	\"fields\": [{\n" + 
				"		\"id\": \"id\",\n" + 
				"		\"type\": \"text\"\n" + 
				"	},\n" + 
				"	{\n" + 
				"		\"id\": \"codeId\",\n" + 
				"		\"type\": \"text\"\n" + 
				"	},\n" + 
				"	{\n" + 
				"		\"id\": \"displayName\",\n" + 
				"		\"type\": \"text\"\n" + 
				"	},\n" + 
				"	{\n" + 
				"		\"id\": \"codeSystemOID\",\n" + 
				"		\"type\": \"text\"\n" + 
				"	},\n" + 
				"	{\n" + 
				"		\"id\": \"definition\",\n" + 
				"		\"type\": \"text\"\n" + 
				"	},\n" + 
				"	{\n" + 
				"		\"id\": \"active\",\n" + 
				"		\"type\": \"bool\"\n" + 
				"	}],\n" + 
				"	\"primary_key\": \"id\",\n" + 
				"	\"records\": [\n" + 
				"{\n" + 
				"\"id\": \"002b860b-e7cd-43e2-bb61-a8587d3712ea\",\n" + 
				"\"codeId\": \"Mage- och tarmsjukvård\",\n" + 
				"\"displayName\": \"Mage- och tarmsjukvård\",\n" + 
				"\"codeSystemOID\": \"54353f603154934aec4f4b79\",\n" + 
				"\"definition\": null,\n" + 
				"\"active\": false\n" + 
				"},\n" + 
				"{\n" + 
				"\"id\": \"01b26d6c-b20d-415f-9dc3-5527e5f599a9\",\n" + 
				"\"codeId\": \"Målnivå Socialstyrelsen\",\n" + 
				"\"displayName\": \"Målnivå Socialstyrelsen\",\n" + 
				"\"codeSystemOID\": \"54353f603154934aec4f4b79\",\n" + 
				"\"definition\": \"<p>Kategorin målnivå Socialstyrelsen innefattar alla indikatorer som Socialstyrelsen har satt kvantifierade målnivåer för i sina nationella riktlinjer.&nbsp;</p>\\n\",\n" + 
				"\"active\": true\n" + 
				"}]\n" + 
				"	\n" + 
				"}";
		
		String correctResponse = "{\"success\":true}";
		
		String response = odpService.createResource("5e90db9b-0e4a-4b14-b5f6-755dbffccadb", data);
		
		assertNotNull(response);
		assertEquals(response, correctResponse);
	}

	@Test
	public void testDeleteResource() throws IOException {
		String resourceString = "{ \"help\": \"http://localhost:5000/api/3/action/help_show?name=package_search\", \"success\": true, \"result\": { \"count\": 1, \"sort\": \"score desc, metadata_modified desc\", \"facets\": {}, \"results\": [ { \"license_title\": \"Creative Commons Attribution\", \"maintainer\": \"\", \"relationships_as_object\": [], \"private\": false, \"maintainer_email\": \"\", \"num_tags\": 1, \"id\": \"249c27c1-3112-43f2-9504-f6230758a3b6\", \"metadata_created\": \"2019-08-27T12:46:10.123557\", \"metadata_modified\": \"2019-10-14T13:37:07.701330\", \"author\": \"\", \"author_email\": \"\", \"state\": \"active\", \"version\": \"\", \"creator_user_id\": \"cf926618-1fbd-4bdb-85f5-7d822f21c6fe\", \"type\": \"dataset\", \"resources\": [ { \"mimetype\": null, \"cache_url\": null, \"hash\": \"\", \"description\": \"\", \"name\": \"abc\", \"format\": \"\", \"url\": \"\", \"datastore_active\": false, \"cache_last_updated\": null, \"package_id\": \"249c27c1-3112-43f2-9504-f6230758a3b6\", \"created\": \"2019-08-27T12:46:49.113026\", \"state\": \"active\", \"mimetype_inner\": null, \"last_modified\": null, \"position\": 0, \"revision_id\": \"33827c80-680b-4ad4-a786-83e0b95f7e89\", \"url_type\": null, \"id\": \"52481432-632b-44b6-aa55-93a922ed0e3f\", \"resource_type\": null, \"size\": null }, { \"mimetype\": null, \"cache_url\": null, \"hash\": \"nkk-v1-documents__\", \"description\": \"\", \"name\": \"nkk-v1-documents\", \"format\": \"\", \"url\": \"http://localhost:5000/datastore/dump/4e91eb91-9c51-44bc-a016-83949c535697\", \"datastore_active\": true, \"cache_last_updated\": null, \"package_id\": \"249c27c1-3112-43f2-9504-f6230758a3b6\", \"created\": \"2019-10-07T09:45:34.816671\", \"state\": \"active\", \"mimetype_inner\": null, \"last_modified\": null, \"position\": 1, \"revision_id\": \"d5303102-a4de-4efb-92ab-56271ec92dfe\", \"url_type\": \"datastore\", \"id\": \"4e91eb91-9c51-44bc-a016-83949c535697\", \"resource_type\": null, \"size\": null } ], \"num_resources\": 2, \"tags\": [ { \"vocabulary_id\": null, \"state\": \"active\", \"display_name\": \"nkk\", \"id\": \"49607753-7046-48be-8ce5-52a6e34e72a4\", \"name\": \"nkk\" } ], \"groups\": [], \"license_id\": \"cc-by\", \"relationships_as_subject\": [], \"organization\": { \"description\": \"\", \"created\": \"2019-06-11T15:03:48.963832\", \"title\": \"Inera\", \"name\": \"inera\", \"is_organization\": true, \"state\": \"active\", \"image_url\": \"\", \"revision_id\": \"cf28f021-40b6-4bbb-9078-60e60f381aa5\", \"type\": \"organization\", \"id\": \"a7d3a8bc-2489-416a-a920-e77d57c70549\", \"approval_status\": \"approved\" }, \"name\": \"nkk\", \"isopen\": true, \"url\": \"\", \"notes\": \"nkk\", \"owner_org\": \"a7d3a8bc-2489-416a-a920-e77d57c70549\", \"extras\": [ { \"key\": \"odpid\", \"value\": \"NKK\" } ], \"license_url\": \"http://www.opendefinition.org/licenses/cc-by\", \"title\": \"NKK\", \"revision_id\": \"7deadcba-59ec-4fd2-8fb2-e832497e735d\" } ], \"search_facets\": {} } }";
		ResponseEntity<String> resultEntity = new ResponseEntity<>(resourceString, HttpStatus.OK);
		when(ckanClient.getResourceByOdpId(anyString(), anyString(), eq(String.class))).thenReturn(resultEntity);
		
		String deleteResource = "{\"help\": \"http://localhost:5000/api/3/action/help_show?name=resource_delete\", \"success\": true, \"result\": null}";
		ResponseEntity<String> responseEntityDeleteResource = new ResponseEntity<>(deleteResource, HttpStatus.OK);
		when(ckanClient.deleteResource(any(), any())).thenReturn(responseEntityDeleteResource);

		String response = odpService.deleteResource("5e90db9b-0e4a-4b14-b5f6-755dbffccadb", "nkk", "nkk-v1-documents");
		
		String correctResponse = "{\"success\":true}";
		
		assertNotNull(response);
		assertEquals(response, correctResponse);
	}

	@Test
	public void testUpdateResource() throws IOException {
		String resourceString = "{ \"help\": \"http://localhost:5000/api/3/action/help_show?name=package_search\", \"success\": true, \"result\": { \"count\": 1, \"sort\": \"score desc, metadata_modified desc\", \"facets\": {}, \"results\": [ { \"license_title\": \"Creative Commons Attribution\", \"maintainer\": \"\", \"relationships_as_object\": [], \"private\": false, \"maintainer_email\": \"\", \"num_tags\": 1, \"id\": \"249c27c1-3112-43f2-9504-f6230758a3b6\", \"metadata_created\": \"2019-08-27T12:46:10.123557\", \"metadata_modified\": \"2019-10-14T13:37:07.701330\", \"author\": \"\", \"author_email\": \"\", \"state\": \"active\", \"version\": \"\", \"creator_user_id\": \"cf926618-1fbd-4bdb-85f5-7d822f21c6fe\", \"type\": \"dataset\", \"resources\": [ { \"mimetype\": null, \"cache_url\": null, \"hash\": \"\", \"description\": \"\", \"name\": \"abc\", \"format\": \"\", \"url\": \"\", \"datastore_active\": false, \"cache_last_updated\": null, \"package_id\": \"249c27c1-3112-43f2-9504-f6230758a3b6\", \"created\": \"2019-08-27T12:46:49.113026\", \"state\": \"active\", \"mimetype_inner\": null, \"last_modified\": null, \"position\": 0, \"revision_id\": \"33827c80-680b-4ad4-a786-83e0b95f7e89\", \"url_type\": null, \"id\": \"52481432-632b-44b6-aa55-93a922ed0e3f\", \"resource_type\": null, \"size\": null }, { \"mimetype\": null, \"cache_url\": null, \"hash\": \"nkk-v1-documents__\", \"description\": \"\", \"name\": \"nkk-v1-documents\", \"format\": \"\", \"url\": \"http://localhost:5000/datastore/dump/4e91eb91-9c51-44bc-a016-83949c535697\", \"datastore_active\": true, \"cache_last_updated\": null, \"package_id\": \"249c27c1-3112-43f2-9504-f6230758a3b6\", \"created\": \"2019-10-07T09:45:34.816671\", \"state\": \"active\", \"mimetype_inner\": null, \"last_modified\": null, \"position\": 1, \"revision_id\": \"d5303102-a4de-4efb-92ab-56271ec92dfe\", \"url_type\": \"datastore\", \"id\": \"4e91eb91-9c51-44bc-a016-83949c535697\", \"resource_type\": null, \"size\": null } ], \"num_resources\": 2, \"tags\": [ { \"vocabulary_id\": null, \"state\": \"active\", \"display_name\": \"nkk\", \"id\": \"49607753-7046-48be-8ce5-52a6e34e72a4\", \"name\": \"nkk\" } ], \"groups\": [], \"license_id\": \"cc-by\", \"relationships_as_subject\": [], \"organization\": { \"description\": \"\", \"created\": \"2019-06-11T15:03:48.963832\", \"title\": \"Inera\", \"name\": \"inera\", \"is_organization\": true, \"state\": \"active\", \"image_url\": \"\", \"revision_id\": \"cf28f021-40b6-4bbb-9078-60e60f381aa5\", \"type\": \"organization\", \"id\": \"a7d3a8bc-2489-416a-a920-e77d57c70549\", \"approval_status\": \"approved\" }, \"name\": \"nkk\", \"isopen\": true, \"url\": \"\", \"notes\": \"nkk\", \"owner_org\": \"a7d3a8bc-2489-416a-a920-e77d57c70549\", \"extras\": [ { \"key\": \"odpid\", \"value\": \"NKK\" } ], \"license_url\": \"http://www.opendefinition.org/licenses/cc-by\", \"title\": \"NKK\", \"revision_id\": \"7deadcba-59ec-4fd2-8fb2-e832497e735d\" } ], \"search_facets\": {} } }";
		ResponseEntity<String> resultEntity = new ResponseEntity<>(resourceString, HttpStatus.OK);
		when(ckanClient.getResourceByOdpId(anyString(), anyString(), eq(String.class))).thenReturn(resultEntity);
		
		String updateResource = "{\"help\": \"http://localhost:5000/api/3/action/help_show?name=datastore_upsert\", \"success\": true, \"result\": {\"records\": [{\"testfield2\": \"blabla\", \"testfield1\": \"test1\"}], \"method\": \"upsert\", \"resource_id\": \"a0fb2bf5-dc35-40e8-8ccf-d2ff73c57dd4\"}}";
		ResponseEntity<String> resultEntityUpdateResource = new ResponseEntity<>(updateResource, HttpStatus.OK);
		when(ckanClient.updateResource(anyString(), anyString())).thenReturn(resultEntityUpdateResource);
		
		String data = "{ \n" + 
				"  \"records\": [ \n" + 
				"  	{ \"testfield1\":\"test1\", \"testfield2\": \"blabla\" }\n" + 
				"   ]\n" + 
				"}";
		
		String response = odpService.updateResource("5e90db9b-0e4a-4b14-b5f6-755dbffccadb", "nkk", "nkk-v1-documents", data);
		
		String correctResponse = "{\"success\":true}";
		
		assertNotNull(response);
		assertEquals(response, correctResponse);
	}

}

