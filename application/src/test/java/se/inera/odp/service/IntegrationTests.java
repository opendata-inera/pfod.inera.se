package se.inera.odp.service;

import static org.assertj.core.api.Assertions.assertThat;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.Console;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.StopWatch;
import se.inera.odp.controller.ODPController;
import se.inera.odp.core.exception.ODPException;
import se.inera.odp.core.request.ODPRequest;
import se.inera.odp.core.response.ODPResponse;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class IntegrationTests {

    private static final int NUMBER_OF_RUNS = 10;
    private static final String TEST_DATASET = "test";
    private static final String TEST_RESOURCE = "test-v1-measures";

    Logger logger = LoggerFactory.getLogger(ODPController.class);

    @Autowired
    private ODPService ckanService;

    @Autowired
    ObjectMapper mapper;

    @Value("${app.dataset.key:}")
    private String authKey;

    private String dataToPost;

    /**
     * These tests are only for developer use.
     * These tests are for stress testing different CKAN-environments.
     * The CKAN-environments to test should be specified in application-test.
     * -    The url to the environments in ckan.datastore.url
     * -    The auth-key to the environment in app.dataset.key
     */

    private void loadData() {
        try {
            Resource resource = new ClassPathResource("measures.json");
            InputStream inputStream = resource.getInputStream();

            byte[] bdata = FileCopyUtils.copyToByteArray(inputStream);
            dataToPost = new String(bdata, StandardCharsets.UTF_8);
        } catch (IOException e) {
            logger.error("IOException", e);
        }
    }

    private <T> T readValue(InputStream src, Class<T> valueType) {
        try {
            return mapper.readValue(src, valueType);
        } catch (IOException e) {
            throw new ODPException(e.getClass().getName() + ":" + e.getMessage());
        }
    }

    private ODPRequest<Object> createResourceDefinition() {
        String definition = "def/definition_test_v1_measures.json";
        InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(definition);
        ODPRequest<Object> req = readValue(is, ODPRequest.class);
        return req;
    }

    private String prepDataToSave() {
        List<Object> records = null;
        try {
            records = mapper.readValue(dataToPost, List.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ODPRequest<Object> req = createResourceDefinition();
        req.setRecords(records);

        String data = null;
        try {
            data = mapper.writeValueAsString(req);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return data;
    }

    @Ignore
    @Test
    public void StressTest_Post() {
        StopWatch sw = new StopWatch();
        loadData();
        String data = prepDataToSave();

        for (int i = 0; i < NUMBER_OF_RUNS; i++) {
            System.out.println("Start test nr " + i);
            sw.start("Run nr: " + i);

            String resp = ckanService.createResource(authKey, data);
            assertThat(resp.contains("success")).isTrue();
            System.out.println("Test " + i + ":\tResource created!");

            ODPResponse res = ckanService
                .getResourceAsObject(TEST_DATASET, TEST_RESOURCE, new HashMap<String, String>(), authKey, "http://localhost/");

            assertThat(res).isNotNull();
            assertThat(res.getRecords()).isNotNull();

            sw.stop();
            System.out.println("Test nr " + i + ". Time elapsed: " + sw.getLastTaskTimeMillis());
        }

        System.out.println(sw.prettyPrint());
        System.out.println("Avarage time: " + sw.getTotalTimeMillis()/NUMBER_OF_RUNS);
    }

    @Ignore
    @Test
    public void StressTest_Update() throws IOException {
        loadData();
        String data = prepDataToSave();
        StopWatch sw = new StopWatch();
        for (int i = 0; i < NUMBER_OF_RUNS; i++) {
            System.out.println("Start test nr " + i);
            sw.start("Run nr: " + i);

            String resp = ckanService.updateResource(authKey, TEST_DATASET, TEST_RESOURCE, data);
            assertThat(resp.contains("success")).isTrue();
            System.out.println("Test " + i + ":\tResource update!");

            ODPResponse res = ckanService
                .getResourceAsObject(TEST_DATASET, TEST_RESOURCE, new HashMap<String, String>(), authKey, "http://localhost/");

            assertThat(res).isNotNull();
            assertThat(res.getRecords()).isNotNull();

            sw.stop();
            System.out.println("Test nr " + i + ". Time elapsed: " + sw.getLastTaskTimeMillis());
        }

        System.out.println(sw.prettyPrint());
        System.out.println("Avarage time: " + sw.getTotalTimeMillis()/NUMBER_OF_RUNS);
    }

    @Ignore
    @Test
    public void StressTest_Get() {
        StopWatch sw = new StopWatch();
        for (int i = 0; i < NUMBER_OF_RUNS; i++) {
            System.out.println("Start test nr " + i);
            sw.start("Run nr: " + i);

            ODPResponse res = ckanService
                .getResourceAsObject(TEST_DATASET, TEST_RESOURCE, new HashMap<String, String>(), authKey, "http://localhost/");

            assertThat(res).isNotNull();
            assertThat(res.getRecords()).isNotNull();

            sw.stop();
            System.out.println("Test nr " + i + ". Time elapsed: " + sw.getLastTaskTimeMillis());
        }

        System.out.println(sw.prettyPrint());
        System.out.println("Avarage time: " + sw.getTotalTimeMillis()/NUMBER_OF_RUNS);
    }
}
