package se.inera.odp.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;
import se.inera.odp.client.CKANClient;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CKANClientTest {

    private CKANClient ckanClient;

    public static MockWebServer mockBackEnd;

    private static int port;

	@BeforeClass
    public static void setUp() throws Exception {
	    mockBackEnd = new MockWebServer();
	    mockBackEnd.start();
	    port = mockBackEnd.getPort();
    }

    @AfterClass
    public static void tearDown() throws IOException {
	    mockBackEnd.shutdown();
    }

    @Before
    public void init(){
	    String baseUrl = String.format("http://localhost:%s", port);
        ckanClient = new CKANClient(baseUrl);
        MockitoAnnotations.initMocks(this);
        ReflectionTestUtils.setField(ckanClient, "CKAN_DATASTORE_CREATE_URL", String.format("http://localhost:%s/api/action/datastore_create", port));
        ReflectionTestUtils.setField(ckanClient, "CKAN_RESOURCE_DELETE_URL", String.format("http://localhost:%s/api/action/resource_delete", port));
        ReflectionTestUtils
            .setField(ckanClient, "CKAN_RESOURCE_SEARCH_URL", String.format("http://localhost:%s/api/action/resource_search?query=hash:", port));
        ReflectionTestUtils.setField(ckanClient, "CKAN_DATASTORE_SEARCH_LIMIT", "500000");
        ReflectionTestUtils.setField(ckanClient, "CKAN_DATASTORE_SEARCH_URL", String.format("http://localhost:%s/api/action/datastore_search", port));
        ReflectionTestUtils.setField(ckanClient, "CKAN_DATASTORE_UPDATE_URL", String.format("http://localhost:%s/api/action/datastore_upsert", port));
        ReflectionTestUtils
            .setField(ckanClient, "CKAN_PACKAGE_SHOW_URL", String.format("http://localhost:%s/api/action/package_show?id={id}&limit={limit}", port));
        ReflectionTestUtils
            .setField(ckanClient, "CKAN_PACKAGE_SEARCH_URL", String.format("http://localhost:%s/api/action/package_search?id=odpid:{id}", port));
    }

    @Test
    public void testGetResource() {
        String responseString = String.format("{\"help\": \"http://localhost:%s/api/3/action/help_show?name=resource_search\", \"success\": true, \"result\": {\"count\": 1, \"results\": [{\"mimetype\": null, \"cache_url\": null, \"state\": \"active\", \"hash\": \"testingLinda2__\", \"description\": \"\", \"format\": \"\", \"url\": \"http://localhost:5000/datastore/dump/46a230e1-ad17-4c02-871b-d3af50fde3d0\", \"datastore_active\": true, \"created\": \"2019-08-13T12:25:39.976788\", \"cache_last_updated\": null, \"package_id\": \"696209af-af22-40b3-924d-94aeb63751ab\", \"mimetype_inner\": null, \"last_modified\": null, \"position\": 1, \"revision_id\": \"ca3c61ba-4581-4847-bb9c-d2d4a79111a4\", \"size\": null, \"url_type\": \"datastore\", \"id\": \"46a230e1-ad17-4c02-871b-d3af50fde3d0\", \"resource_type\": null, \"name\": \"testingLinda2\"}]}}", port);
        mockBackEnd.enqueue(new MockResponse().setBody(responseString).addHeader("Content-Type", "application/json"));

        ResponseEntity<String> response = ckanClient
            .getResourceByOdpId("5e90db9b-0e4a-4b14-b5f6-755dbffccadb", "testingLinda2__", String.class);

        assertNotNull(response);
        assertEquals(responseString, response.getBody());
    }

    @Test
    public void testGetData() {
        String responseString = String.format("{\"help\": \"http://localhost:%s/api/3/action/help_show?name=resource_search\", \"success\": true, \"result\": {\"count\": 1, \"results\": [{\"mimetype\": null, \"cache_url\": null, \"state\": \"active\", \"hash\": \"testingLinda2__\", \"description\": \"\", \"format\": \"\", \"url\": \"http://localhost:5000/datastore/dump/46a230e1-ad17-4c02-871b-d3af50fde3d0\", \"datastore_active\": true, \"created\": \"2019-08-13T12:25:39.976788\", \"cache_last_updated\": null, \"package_id\": \"696209af-af22-40b3-924d-94aeb63751ab\", \"mimetype_inner\": null, \"last_modified\": null, \"position\": 1, \"revision_id\": \"ca3c61ba-4581-4847-bb9c-d2d4a79111a4\", \"size\": null, \"url_type\": \"datastore\", \"id\": \"46a230e1-ad17-4c02-871b-d3af50fde3d0\", \"resource_type\": null, \"name\": \"testingLinda2\"}]}}", port);
        mockBackEnd.enqueue(new MockResponse().setBody(responseString).addHeader("Content-Type", "application/json"));

        ResponseEntity<String> response = ckanClient.getData("5e90db9b-0e4a-4b14-b5f6-755dbffccadb", "testingLinda2__", null, String.class);

        assertNotNull(response);
        assertEquals(responseString, response.getBody());
    }

    @Test
    public void testGetResourceForId() {

        String responseString = String.format("{\"help\": \"http://localhost:%s/api/3/action/help_show?name=resource_search\", \"success\": true, \"result\": {\"count\": 1, \"results\": [{\"mimetype\": null, \"cache_url\": null, \"state\": \"active\", \"hash\": \"testingLinda2__\", \"description\": \"\", \"format\": \"\", \"url\": \"http://localhost:5000/datastore/dump/46a230e1-ad17-4c02-871b-d3af50fde3d0\", \"datastore_active\": true, \"created\": \"2019-08-13T12:25:39.976788\", \"cache_last_updated\": null, \"package_id\": \"696209af-af22-40b3-924d-94aeb63751ab\", \"mimetype_inner\": null, \"last_modified\": null, \"position\": 1, \"revision_id\": \"ca3c61ba-4581-4847-bb9c-d2d4a79111a4\", \"size\": null, \"url_type\": \"datastore\", \"id\": \"46a230e1-ad17-4c02-871b-d3af50fde3d0\", \"resource_type\": null, \"name\": \"testingLinda2\"}]}}", port);
        mockBackEnd.enqueue(new MockResponse().setBody(responseString).addHeader("Content-Type", "application/json"));

        ResponseEntity<String> response = ckanClient.getResourceForId("5e90db9b-0e4a-4b14-b5f6-755dbffccadb", "testingLinda2__");

        assertNotNull(response);
        assertEquals(responseString, response.getBody());
    }

    @Test
    public void testCreateResource() {
        String responseString = String.format("{\"help\": \"http://localhost:%s/api/3/action/help_show?name=datastore_create\", \"success\": true, \"result\": {\"resource\": {\"url\": \"_datastore_only_resource\", \"mimetype\": null, \"package_id\": \"test\", \"hash\": \"testingLinda4__\", \"name\": \"testingLinda4\"}, \"primary_key\": \"testfield1\", \"resource_id\": \"d4f399c0-3a2a-49da-a050-d82592ca4955\", \"fields\": [{\"type\": \"text\", \"id\": \"testfield1\"}, {\"type\": \"text\", \"id\": \"testfield2\"}], \"method\": \"insert\"}}", port);
        mockBackEnd.enqueue(new MockResponse().setBody(responseString).addHeader("Content-Type", "application/json"));

        String data = "{\n" +
            "	\"resource\": {\n" +
            "		\"package_id\": \"test\",\n" +
            "		\"name\": \"testingLinda4\",\n" +
            "   		\"hash\": \"testingLinda4__\"\n" +
            "	},\n" +
            "	\"fields\": [{\n" +
            "		\"id\": \"testfield1\",\n" +
            "		\"type\": \"text\"\n" +
            "	},\n" +
            "	{\n" +
            "		\"id\": \"testfield2\",\n" +
            "		\"type\": \"text\"\n" +
            "	}\n" +
            "	],\n" +
            "	\"primary_key\": \"testfield1\",\n" +
            "	\"records\": [\n" +
            "{\n" +
            "\"testfield1\": \"test1\",\n" +
            "\"testfield2\": \"test2\"\n" +
            "}\n" +
            "]\n" +
            "}";

        ResponseEntity<String> response = ckanClient.createResource("5e90db9b-0e4a-4b14-b5f6-755dbffccadb", data);

        assertNotNull(response);
        assertEquals(responseString, response.getBody());
    }

    @Test
    public void testDeleteResource() {
        String responseString = String.format("{\"help\": \"http://localhost:%s/api/3/action/help_show?name=resource_delete\", \"success\": true, \"result\": null}", port);
        mockBackEnd.enqueue(new MockResponse().setBody(responseString).addHeader("Content-Type", "application/json"));

        ResponseEntity<String> response = ckanClient
            .deleteResource("5e90db9b-0e4a-4b14-b5f6-755dbffccadb", "5e96840b-16e7-4fc2-b4c1-0aa933a6fc0d");

        assertNotNull(response);
        assertEquals(responseString, response.getBody());
    }

    @Test
    public void testUpdateResource() {
        String responseString = String.format("{\"help\": \"http://localhost:%s/api/3/action/help_show?name=datastore_upsert\", \"success\": true, \"result\": {\"records\": [{\"testfield2\": \"blablabla\", \"testfield1\": \"test1\"}], \"method\": \"upsert\", \"resource_id\": \"701e888a-d0c7-4308-82dc-2d841e8c603e\"}}", port);
        mockBackEnd.enqueue(new MockResponse().setBody(responseString).addHeader("Content-Type", "application/json"));

        String data = "{\"resource_id\": \"701e888a-d0c7-4308-82dc-2d841e8c603e\", \"force\": true, \"records\": [ \n" +
            "  	{ \"testfield1\":\"test1\", \"testfield2\": \"blablabla\" }\n" +
            "   ]\n" +
            ", \"method\" : \"upsert\" }";

        ResponseEntity<String> response = ckanClient.updateResource("5e90db9b-0e4a-4b14-b5f6-755dbffccadb", data);

        assertNotNull(response);
        assertEquals(responseString, response.getBody());
    }

}