package se.inera.odp.core.utils.ssl;

import se.inera.odp.core.utils.ssl.PemKeyManagerCreator;

import javax.net.ssl.KeyManager;

import org.junit.Assert;
import org.junit.Test;

import java.nio.file.NoSuchFileException;
import java.security.cert.CertificateException;

/**
 * Created by davsan on 2018-04-12.
 */
public class PemKeyManagerCreatorTest {

    @Test
    public void shouldThrowNoSuchFileExceptionWhenCertFileIsNotFound(){
        Throwable exception = null;
        try{
            PemKeyManagerCreator.createKeyManagers("dummy/path/to/pemCert");
        }catch(Exception e){
            exception = e;
        }
        Assert.assertNotNull(exception);
        Assert.assertSame(exception.getClass(),NoSuchFileException.class);
    }

    @Test
    public void shouldCreateKeyManagersFromCertWithPrivateKey(){
        String certPath = CertFileHandler.getPemCertPath(CertFileHandler.PemCertType.PRIVATE_KEY);
        try{
            KeyManager[] keyManagers = PemKeyManagerCreator.createKeyManagers(certPath);
            Assert.assertNotNull(keyManagers);
            Assert.assertTrue(keyManagers.length == 1);
        }catch(Exception e){
            Assert.fail();
        }
    }

    @Test
    public void shouldCreateKeyManagersFromCertWithRsaPrivateKey(){
        String certPath = CertFileHandler.getPemCertPath(CertFileHandler.PemCertType.RSA_PRIVATE_KEY);
        try{
            KeyManager[] keyManagers = PemKeyManagerCreator.createKeyManagers(certPath);
            Assert.assertNotNull(keyManagers);
            Assert.assertTrue(keyManagers.length == 1);
        }catch(Exception e){
            Assert.fail();
        }
    }

    @Test
    public void shouldThrowCertificateExceptionIfCertificateDelimiterNotFoundInCert(){
        Throwable exception = null;
        try{
            String certPath = CertFileHandler.getPemCertPath(CertFileHandler.PemCertType.MISSING_CERTIFICATE);
            PemKeyManagerCreator.createKeyManagers(certPath);
        }catch(Exception e){
            exception = e;
        }
        Assert.assertNotNull(exception);
        Assert.assertSame(exception.getClass(), CertificateException.class);
    }

    @Test
    public void shouldThrowCertificateExceptionIfKeyDelimiterNotFoundInCert(){
        Throwable exception = null;
        try{
            String certPath = CertFileHandler.getPemCertPath(CertFileHandler.PemCertType.MISSING_KEY);
            PemKeyManagerCreator.createKeyManagers(certPath);
        }catch(Exception e){
            exception = e;
        }
        Assert.assertNotNull(exception);
        Assert.assertSame(exception.getClass(), CertificateException.class);
    }
}
