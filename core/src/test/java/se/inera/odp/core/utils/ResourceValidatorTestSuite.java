package se.inera.odp.core.utils;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
	ResourceValidatorTestsForKik.class,
	ResourceValidatorTestsForNkk.class
})
public class ResourceValidatorTestSuite {

}