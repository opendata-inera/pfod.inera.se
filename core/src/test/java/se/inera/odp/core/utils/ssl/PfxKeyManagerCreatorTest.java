package se.inera.odp.core.utils.ssl;

import javax.net.ssl.KeyManager;

import org.junit.Assert;
import org.junit.Test;

import se.inera.odp.core.utils.ssl.PfxKeyManagerCreator;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by davsan on 2018-04-12.
 */
public class PfxKeyManagerCreatorTest {

    @Test
    public void shouldThrowFileNotFoundExceptionWhenCertFileIsNotFound(){
        Throwable exception = null;
        try{
            PfxKeyManagerCreator.createKeyManagers("dummy/path/to/pfxCert", "");
        }catch(Exception e){
            exception = e;
        }
        Assert.assertNotNull(exception);
        Assert.assertSame(exception.getClass(), FileNotFoundException.class);
    }

    @Test
    public void shouldThrowExceptionWhenPasswordIsWrong(){
        Throwable exception = null;
        try{
            String certPath = CertFileHandler.getPfxCertPath();
            String password = CertFileHandler.getPfxWrongPassword();
            PfxKeyManagerCreator.createKeyManagers(certPath, password);
        }catch(Exception e){
            exception = e;
        }
        Assert.assertNotNull(exception);
        Assert.assertSame(exception.getClass(), IOException.class);
        Assert.assertTrue(exception.getMessage().contains("keystore password was incorrect"));
    }

    @Test
    public void shouldCreateKeyManagersFromCertWithCorrectPassword(){
        try{
            String certPath = CertFileHandler.getPfxCertPath();
            String password = CertFileHandler.getPfxPassword();
            KeyManager[] keyManagers = PfxKeyManagerCreator.createKeyManagers(certPath, password);
            Assert.assertNotNull(keyManagers);
            Assert.assertTrue(keyManagers.length == 1);
        }catch(Exception e){
            Assert.fail();
        }
    }


}
