package se.inera.odp.core.utils.ssl;

import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;

import org.junit.Assert;
import org.junit.Test;

import se.inera.odp.core.utils.ssl.SSLContextCreator;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

/**
 * Created by davsan on 2018-04-12.
 */
public class SSLContextCreatorTest {

    @Test
    public void shouldThrowNoSuchAlgorithmExceptionWhenProtocolIsInvalid(){
        Throwable exception = null;
        try{
            TrustManager[] trustManagers = null;
            KeyManager[] keyManagers = null;
            SSLContextCreator.createSSLContext("INVALID-PROTOCOL", keyManagers, trustManagers);
        }catch(Exception e){
            exception = e;
        }
        Assert.assertNotNull(exception);
        Assert.assertSame(exception.getClass(), NoSuchAlgorithmException.class);
    }

    @Test
    public void shouldThrowNullPointerExceptionWhenProtocolIsNull(){
        Throwable exception = null;
        try{
            TrustManager[] trustManagers = null;
            KeyManager[] keyManagers = null;
            SSLContextCreator.createSSLContext(null, keyManagers, trustManagers);
        }catch(Exception e){
            exception = e;
        }
        Assert.assertNotNull(exception);
        Assert.assertSame(exception.getClass(), NullPointerException.class);
    }

    @Test
    public void shouldThrowIOExceptionWhenPfxPasswordIsIncorrect(){
        Throwable exception = null;
        try{
            String pfxCertPath = CertFileHandler.getPfxCertPath();
            String trustCertPath = CertFileHandler.getTrustCertPath();
            String password = CertFileHandler.getPfxWrongPassword();
            SSLContextCreator.createSSLContext(pfxCertPath, password, trustCertPath);
        }catch(Exception e){
            exception = e;
        }
        Assert.assertNotNull(exception);
        Assert.assertSame(exception.getClass(), IOException.class);
        Assert.assertTrue(exception.getMessage().contains("keystore password was incorrect"));
    }

    @Test
    public void shouldCreateSSLContextFromTrustCert(){
        try{
            String trustCertPath = CertFileHandler.getTrustCertPath();
            SSLContext sslContext = SSLContextCreator.createSSLContext(trustCertPath);
            Assert.assertNotNull(sslContext);
            Assert.assertSame(sslContext.getProtocol(),"TLS");
        }catch(Exception e){
            Assert.fail();
        }
    }

    @Test
    public void shouldCreateSSLContextFromPfxCertAndTrustCert(){
        try{
            String pfxCertPath = CertFileHandler.getPfxCertPath();
            String trustCertPath = CertFileHandler.getTrustCertPath();
            String password = CertFileHandler.getPfxPassword();
            SSLContext sslContext = SSLContextCreator.createSSLContext(pfxCertPath, password, trustCertPath);
            Assert.assertNotNull(sslContext);
            Assert.assertSame(sslContext.getProtocol(),"TLS");
        }catch(Exception e){
            Assert.fail();
        }
    }

    @Test
    public void shouldCreateSSLContextFromPemCertAndTrustCert(){
        try{
            String pemCertPath = CertFileHandler.getPemCertPath(CertFileHandler.PemCertType.PRIVATE_KEY);
            String trustCertPath = CertFileHandler.getTrustCertPath();
            SSLContext sslContext = SSLContextCreator.createSSLContext(pemCertPath, trustCertPath);
            Assert.assertNotNull(sslContext);
            Assert.assertSame(sslContext.getProtocol(),"TLS");
        }catch(Exception e){
            Assert.fail();
        }
    }

    @Test
    public void shouldCreateSSLContextFromPemCertAndAcceptingUntrustedCerts(){
        try{
            String pemCertPath = CertFileHandler.getPemCertPath(CertFileHandler.PemCertType.PRIVATE_KEY);
            SSLContext sslContext = SSLContextCreator.createSSLContext(pemCertPath, true);
            Assert.assertNotNull(sslContext);
            Assert.assertSame(sslContext.getProtocol(),"TLS");
        }catch(Exception e){
            Assert.fail();
        }
    }

    @Test
    public void shouldCreateSSLContextFromPemCertAndNotAcceptingUntrustedCerts(){
        try{
            String pemCertPath = CertFileHandler.getPemCertPath(CertFileHandler.PemCertType.PRIVATE_KEY);
            SSLContext sslContext = SSLContextCreator.createSSLContext(pemCertPath, false);
            Assert.assertNotNull(sslContext);
            Assert.assertSame(sslContext.getProtocol(),"TLS");
        }catch(Exception e){
            Assert.fail();
        }
    }

    @Test
    public void shouldCreateSSLContextFromPfxCertAndAcceptingUntrustedCerts(){
        try{
            String pfxCertPath = CertFileHandler.getPfxCertPath();
            String password = CertFileHandler.getPfxPassword();
            SSLContext sslContext = SSLContextCreator.createSSLContext(pfxCertPath, password, true);
            Assert.assertNotNull(sslContext);
            Assert.assertSame(sslContext.getProtocol(),"TLS");
        }catch(Exception e){
            Assert.fail();
        }
    }

    @Test
    public void shouldCreateSSLContextFromPfxCertAndNotAcceptingUntrustedCerts(){
        try{
            String pfxCertPath = CertFileHandler.getPfxCertPath();
            String password = CertFileHandler.getPfxPassword();
            SSLContext sslContext = SSLContextCreator.createSSLContext(pfxCertPath, password, false);
            Assert.assertNotNull(sslContext);
            Assert.assertSame(sslContext.getProtocol(),"TLS");
        }catch(Exception e){
            Assert.fail();
        }
    }

    @Test
    public void shouldCreateSSLContextWithoutClientCertAndAcceptingUntrustedCerts(){
        try{
            SSLContext sslContext = SSLContextCreator.createSSLContext( true);
            Assert.assertNotNull(sslContext);
            Assert.assertSame(sslContext.getProtocol(),"TLS");
        }catch(Exception e){
            Assert.fail();
        }
    }

}
