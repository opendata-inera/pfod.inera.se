package se.inera.odp.core.utils;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.eq;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import se.inera.odp.core.exception.ODPException;
import se.inera.odp.core.request.ODPRequest;
import se.inera.odp.core.utils.ResourceValidator;

@RunWith(SpringRunner.class)
public class ResourceValidatorTestsForKik {
	
	private ObjectMapper mapper = new ObjectMapper();
	
	private ResourceValidator validator = new ResourceValidator();
	
	private String v2ChecksumsData = "[{\"id\":\"checkSumId1\",\"checksum\":\"checkSum1\"},{\"id\":\"checkSumId1\",\"checksum\":\"checkSum2\"}]";
	
	private String v2IncorrectChecksumsData = "[{\"id\": false,\"checksum\":\"checkSum1\"},{\"id\":\"checkSumId1\",\"checksum\":\"checkSum2\"}]";
	
	private String v2ChecksumsMockedResult = "{\"resource\":{\"package_id\":\"kvalitetsindikatorer\",\"name\":\"kik-v2-checksums\","
			+ "\"hash\":\"kik-v2-checksums__\"},\"fields\":[{\"id\":\"id\",\"type\":\"text\"},{\"id\":\"checksum\","
			+ "\"type\":\"text\"}],\"primary_key\":\"id\"}";
	
	private String v3ChecksumsData = "[{\"id\":\"checkSumId1\",\"checksum\":\"checkSum1\"},{\"id\":\"checkSumId1\",\"checksum\":\"checkSum2\"}]";
	
	private String v3IncorrectChecksumsData = "[{\"id\": false,\"checksum\":\"checkSum1\"},{\"id\":\"checkSumId1\",\"checksum\":\"checkSum2\"}]";	
	
	private String v3ChecksumsMockedResult = "{\"resource\":{\"package_id\":\"kvalitetsindikatorer\","
			+ "\"name\":\"kik-v3-checksums\",\"hash\":\"kik-v3-checksums__\"},\"fields\":[{\"id\":\"id\",\"type\":\"text\"},"
			+ "{\"id\":\"checksum\",\"type\":\"text\"}],\"primary_key\":\"id\"}";
	
	private String v2CodesData = "[{\"definition\":\"30 år till 64 år och 364 dagar\",\"displayName\":\"Ålder 30 - 64\",\"codeSystemOID\":"
			+ "\"19bb7abe-7de4-420f-8259-7aca0a461b6f\",\"codeId\":\"30 - 64\",\"active\":true,\"id\":\"74df40e2-e615-4a65-bf6d-bc7b5e72d551\"},"
			+ "{\"definition\":\"Dagar\",\"displayName\":\"Dagar\",\"codeSystemOID\":\"544678f431509a480440ecaf\",\"codeId\":\"Dagar\","
			+ "\"active\":true,\"id\":\"76784d8b-d273-4a98-8ad8-e821004ba314\"}]";
	
	private String v2IncorrectCodesData = "[{\"definition\":false,\"displayName\":\"Ålder 30 - 64\",\"codeSystemOID\":"
			+ "\"19bb7abe-7de4-420f-8259-7aca0a461b6f\",\"codeId\":\"30 - 64\",\"active\":true,\"id\":\"74df40e2-e615-4a65-bf6d-bc7b5e72d551\"},"
			+ "{\"definition\":\"Dagar\",\"displayName\":\"Dagar\",\"codeSystemOID\":\"544678f431509a480440ecaf\",\"codeId\":\"Dagar\","
			+ "\"active\":true,\"id\":\"76784d8b-d273-4a98-8ad8-e821004ba314\"}]";
	
	private String v2CodesMockedResult = "{\"resource\":{\"package_id\":\"kvalitetsindikatorer\",\"name\":\"kik-v3-codes\","
			+ "\"hash\":\"kik-v3-codes__\"},\"fields\":[{\"id\":\"id\",\"type\":\"text\"},{\"id\":\"codeId\",\"type\":\"text\"},{\"id\":\"displayName\","
			+ "\"type\":\"text\"},{\"id\":\"codeSystemOID\",\"type\":\"text\"},{\"id\":\"definition\",\"type\":\"text\"},{\"id\":\"active\","
			+ "\"type\":\"bool\"}],\"primary_key\":\"id\",\"always_strings\":[\"codeId\"]}";
	
	private String v3CodesData = "[{\"definition\":\"30 år till 64 år och 364 dagar\",\"displayName\":\"Ålder 30 - 64\",\"codeSystemOID\":"
			+ "\"19bb7abe-7de4-420f-8259-7aca0a461b6f\",\"codeId\":\"30 - 64\",\"active\":true,\"id\":\"74df40e2-e615-4a65-bf6d-bc7b5e72d551\"},"
			+ "{\"definition\":\"Dagar\",\"displayName\":\"Dagar\",\"codeSystemOID\":\"544678f431509a480440ecaf\",\"codeId\":\"Dagar\","
			+ "\"active\":true,\"id\":\"76784d8b-d273-4a98-8ad8-e821004ba314\"}]";
	
	private String v3IncorrectCodesData = "[{\"definition\":false,\"displayName\":\"Ålder 30 - 64\",\"codeSystemOID\":"
			+ "\"19bb7abe-7de4-420f-8259-7aca0a461b6f\",\"codeId\":\"30 - 64\",\"active\":true,\"id\":\"74df40e2-e615-4a65-bf6d-bc7b5e72d551\"},"
			+ "{\"definition\":\"Dagar\",\"displayName\":\"Dagar\",\"codeSystemOID\":\"544678f431509a480440ecaf\",\"codeId\":\"Dagar\","
			+ "\"active\":true,\"id\":\"76784d8b-d273-4a98-8ad8-e821004ba314\"}]";
	
	private String v3CodesMockedResult = "{\"resource\":{\"package_id\":\"kvalitetsindikatorer\",\"name\":\"kik-v3-codes\","
			+ "\"hash\":\"kik-v3-codes__\"},\"fields\":[{\"id\":\"id\",\"type\":\"text\"},{\"id\":\"codeId\",\"type\":\"text\"},{\"id\":\"displayName\","
			+ "\"type\":\"text\"},{\"id\":\"codeSystemOID\",\"type\":\"text\"},{\"id\":\"definition\",\"type\":\"text\"},{\"id\":\"active\","
			+ "\"type\":\"bool\"}],\"primary_key\":\"id\",\"always_strings\":[\"codeId\"]}";

	private String v2CodeSystemsData = "[{\"active\":true,\"source\":\"Nyckelord\",\"OID\":\"54353f603154934aec4f4b79\","
			+ "\"name\":\"Nyckelord\",\"description\":\"Används för att mappa nyckelord till indikatorer\"},{\"active\":true,\"source\":null,"
			+ "\"OID\":\"544678f431509a480440ecaf\",\"name\":\"Mätenheter\",\"description\":\"Här läggs koder för mätenheter\"}]";
	
	private String v2IncorrectCodeSystemsData = "[{\"active\":\"test\",\"source\":\"Nyckelord\",\"OID\":\"54353f603154934aec4f4b79\","
			+ "\"name\":\"Nyckelord\",\"description\":\"Används för att mappa nyckelord till indikatorer\"},{\"active\":true,\"source\":null,"
			+ "\"OID\":\"544678f431509a480440ecaf\",\"name\":\"Mätenheter\",\"description\":\"Här läggs koder för mätenheter\"}]";
	
	private String v2CodeSystemsMockedResult = "{\"resource\":{\"package_id\":\"kvalitetsindikatorer\",\"name\":\"kik-v3-codeSystems\","
			+ "\"hash\":\"kik-v3-codeSystems__\"},\"fields\":[{\"id\":\"OID\",\"type\":\"text\"},{\"id\":\"name\",\"type\":\"text\"},"
			+ "{\"id\":\"source\",\"type\":\"text\"},{\"id\":\"description\",\"type\":\"text\"},{\"id\":\"active\",\"type\":\"bool\"}],\"primary_key\":\"OID\"}";
	
	private String v3CodeSystemsData = "[{\"active\":true,\"source\":\"Nyckelord\",\"OID\":\"54353f603154934aec4f4b79\","
			+ "\"name\":\"Nyckelord\",\"description\":\"Används för att mappa nyckelord till indikatorer\"},{\"active\":true,\"source\":null,"
			+ "\"OID\":\"544678f431509a480440ecaf\",\"name\":\"Mätenheter\",\"description\":\"Här läggs koder för mätenheter\"}]";
	
	private String v3IncorrectCodeSystemsData = "[{\"active\":\"test\",\"source\":\"Nyckelord\",\"OID\":\"54353f603154934aec4f4b79\","
			+ "\"name\":\"Nyckelord\",\"description\":\"Används för att mappa nyckelord till indikatorer\"},{\"active\":true,\"source\":null,"
			+ "\"OID\":\"544678f431509a480440ecaf\",\"name\":\"Mätenheter\",\"description\":\"Här läggs koder för mätenheter\"}]";
	
	private String v3CodeSystemsMockedResult = "{\"resource\":{\"package_id\":\"kvalitetsindikatorer\",\"name\":\"kik-v3-codeSystems\","
			+ "\"hash\":\"kik-v3-codeSystems__\"},\"fields\":[{\"id\":\"OID\",\"type\":\"text\"},{\"id\":\"name\",\"type\":\"text\"},"
			+ "{\"id\":\"source\",\"type\":\"text\"},{\"id\":\"description\",\"type\":\"text\"},{\"id\":\"active\",\"type\":\"bool\"}],\"primary_key\":\"OID\"}";

	private String v2MeasureFormerVersionsData = "[{\"measurePopulationExclusionValueSet\":null,\"measuringFrequency\":[{\"code\":\"547489db315319316c159aee\","
			+ "\"system\":\"547489a8315319316c156a41\"}],\"regionalAffinityCalculation\":null,\"thresholdForViewing\":0,\"validTo\":null,\"descriptionSource\":[\"Nationella diabetesregistret (NDR)\"],"
			+ "\"unit\":null,\"denominatorExclusionValueSet\":null,\"measureVersionNumberMajor\":1,\"coverageCalculation\":null,\"numeratorDefinition\":"
			+ "\"\\u003cp\\u003e\\u003cspan style\\u003d\\\"font-size:10pt\\\"\\u003eAntal patienter med typ 1-diabetes vid medicinkliniker\\u0026nbsp;"
			+ "\\u003c/span\\u003esom har BMI lika med eller högre än 35.\\u0026nbsp;\\u003c/p\\u003e\\n\",\"measurementTypeCode\":{\"code\":\"5444f28731504f493890e7b3\","
			+ "\"system\":\"54479f1e3150584788ceffcd\"},\"serviceContractVersion\":null,\"title\":\"Andel utan svår fetma vid diabetes typ 1\","
			+ "\"measurePopulationDefinition\":null,\"interpretationTextPublic\":null,\"measureTypeCode\":{\"code\":\"54479f933150584788cf003a\","
			+ "\"system\":\"5444ef7331504f493890e7a4\"},\"denominatorDefinition\":\"\\u003cp\\u003e\\u003cspan style\\u003d\\\"font-size:10pt\\\"\\u003eTota"
			+ "lt antal patienter med typ 1-diabetes vid medicinklinik\\u0026nbsp;\\u003c/span\\u003edär uppgift om BMI finns under mätperioden.\\u003c/p\\u003e\\n\","
			+ "\"denominatorInclusionValueSet\":null,\"availableReportingSystemHsaId\":[\"SE2321000131-S000000015190\"],\"organizationLevel\":[{\"code\":\"54763d8631504f144406cbc9\","
			+ "\"system\":\"54748b66315319316c168ef3\"},{\"code\":\"54763e0531504f144406ccf0\",\"system\":\"54748b66315319316c168ef3\"},{\"code\":\"54763e4b31504f144406cd3f\","
			+ "\"system\":\"54748b66315319316c168ef3\"}],\"soughtValue\":{\"code\":\"5474840531504f316c6101d9\",\"system\":\"5474837931504f316c60a079\"},\"measureGroupCode\":[{\"code"
			+ "\":\"543cea333152bd41105a9138\",\"system\":\"54353f603154934aec4f4b79\"},{\"code\":\"32eee07f-57ab-4c8f-90fc-0f25f3678692\",\"system\":\"54353f603154934aec4f4b79\"},"
			+ "{\"code\":\"543cea3d3152bd41105a913b\",\"system\":\"54353f603154934aec4f4b79\"}],\"publiclyAvailableInformation\":false,\"cohortDefinition\":null,\"description\":null,"
			+ "\"measureId\":\"3f1ddf20-bfcf-449a-9c9d-5439c6587093\",\"lastUpdated\":\"2017-01-25T15:59:16\",\"authenticator\":null,\"interpretationTextPersonell\":null,"
			+ "\"numeratorExclusionValueSet\":null,\"cohortInclusionValueSet\":null,\"validFrom\":\"2014-01-01\",\"measurePopulationInclusionValueSet\":null,\"measureDefinition\":"
			+ "\"Andel personer med diabetes typ 1 som har BMI lägre än 35 kg/m2. Medicinklinik.\",\"author\":{\"authorName\":\"Fredrik Westander\",\"authorRoleCode\":null,"
			+ "\"authorOrganization\":null,\"authorContactInformation\":null},\"cohortExclusionValueSet\":null,\"measureFormerVersionId\":\"a5fe3972-8027-483a-9d44-a52ebf4f3495\","
			+ "\"sourcesOfError\":null,\"measureVersionNumberMinor\":10,\"numeratorInclusionValueSet\":null,\"regionalIndicator\":null}]";
	
	private String v2IncorrectMeasureFormerVersionsData = "[{\"measurePopulationExclusionValueSet\":null,\"measuringFrequency\":[{\"code\":\"547489db315319316c159aee\","
			+ "\"system\":\"547489a8315319316c156a41\"}],\"regionalAffinityCalculation\":null,\"thresholdForViewing\":0,\"validTo\":null,\"descriptionSource\":[\"Nationella diabetesregistret (NDR)\"],"
			+ "\"unit\":null,\"denominatorExclusionValueSet\":null,\"measureVersionNumberMajor\":1,\"coverageCalculation\":null,\"numeratorDefinition\":"
			+ "\"\\u003cp\\u003e\\u003cspan style\\u003d\\\"font-size:10pt\\\"\\u003eAntal patienter med typ 1-diabetes vid medicinkliniker\\u0026nbsp;"
			+ "\\u003c/span\\u003esom har BMI lika med eller högre än 35.\\u0026nbsp;\\u003c/p\\u003e\\n\",\"measurementTypeCode\":{\"code\":\"5444f28731504f493890e7b3\","
			+ "\"system\":\"54479f1e3150584788ceffcd\"},\"serviceContractVersion\":null,\"title\":\"Andel utan svår fetma vid diabetes typ 1\","
			+ "\"measurePopulationDefinition\":null,\"interpretationTextPublic\":null,\"measureTypeCode\":{\"code\":\"54479f933150584788cf003a\","
			+ "\"system\":\"5444ef7331504f493890e7a4\"},\"denominatorDefinition\":\"\\u003cp\\u003e\\u003cspan style\\u003d\\\"font-size:10pt\\\"\\u003eTota"
			+ "lt antal patienter med typ 1-diabetes vid medicinklinik\\u0026nbsp;\\u003c/span\\u003edär uppgift om BMI finns under mätperioden.\\u003c/p\\u003e\\n\","
			+ "\"denominatorInclusionValueSet\":null,\"availableReportingSystemHsaId\":[\"SE2321000131-S000000015190\"],\"organizationLevel\":[{\"code\":\"54763d8631504f144406cbc9\","
			+ "\"system\":\"54748b66315319316c168ef3\"},{\"code\":\"54763e0531504f144406ccf0\",\"system\":\"54748b66315319316c168ef3\"},{\"code\":\"54763e4b31504f144406cd3f\","
			+ "\"system\":\"54748b66315319316c168ef3\"}],\"soughtValue\":{\"code\":\"5474840531504f316c6101d9\",\"system\":\"5474837931504f316c60a079\"},\"measureGroupCode\":[{\"code"
			+ "\":\"543cea333152bd41105a9138\",\"system\":\"54353f603154934aec4f4b79\"},{\"code\":\"32eee07f-57ab-4c8f-90fc-0f25f3678692\",\"system\":\"54353f603154934aec4f4b79\"},"
			+ "{\"code\":\"543cea3d3152bd41105a913b\",\"system\":\"54353f603154934aec4f4b79\"}],\"publiclyAvailableInformation\":false,\"cohortDefinition\":null,\"description\":null,"
			+ "\"measureId\":\"3f1ddf20-bfcf-449a-9c9d-5439c6587093\",\"lastUpdated\":\"2017-01-25T15:59:16\",\"authenticator\":null,\"interpretationTextPersonell\":null,"
			+ "\"numeratorExclusionValueSet\":null,\"cohortInclusionValueSet\":null,\"validFrom\":\"2014-01-01\",\"measurePopulationInclusionValueSet\":null,\"measureDefinition\":"
			+ "\"Andel personer med diabetes typ 1 som har BMI lägre än 35 kg/m2. Medicinklinik.\",\"author\":{\"authorName\":\"Fredrik Westander\",\"authorRoleCode\":null,"
			+ "\"authorOrganization\":null,\"authorContactInformation\":null},\"cohortExclusionValueSet\":null,\"measureFormerVersionId\":\"a5fe3972-8027-483a-9d44-a52ebf4f3495\","
			+ "\"sourcesOfError\":null,\"measureVersionNumberMinor\":10,\"numeratorInclusionValueSet\":null,\"regionalIndicator\":false}]";

	private String v2MeasureFormerVersionsMockedResult = "{\"resource\":{\"package_id\":\"kvalitetsindikatorer\",\"name\":\"kik-v2-measureFormerVersions\",\"hash\":\"kik-v2-measureFormerVersions__\"},"
			+ "\"fields\":[{\"id\":\"measureFormerVersionId\",\"type\":\"text\"},{\"id\":\"measureId\",\"type\":\"text\"},{\"id\":\"measureVersionNumberMajor\",\"type\":\"int\"},"
			+ "{\"id\":\"measureVersionNumberMinor\",\"type\":\"int\"},{\"id\":\"lastUpdated\",\"type\":\"timestamp\"},{\"id\":\"validFrom\",\"type\":\"date\"},{\"id\":\"validTo\","
			+ "\"type\":\"date\"},{\"id\":\"publiclyAvailableInformation\",\"type\":\"bool\"},{\"id\":\"regionalIndicator\",\"type\":\"text[]\"},{\"id\":\"title\",\"type\":\"text\"},"
			+ "{\"id\":\"measureDefinition\",\"type\":\"text\"},{\"id\":\"description\",\"type\":\"text\"},{\"id\":\"descriptionSource\",\"type\":\"text[]\"},{\"id\":\"measureTypeCode\","
			+ "\"type\":\"json\"},{\"id\":\"measurementTypeCode\",\"type\":\"json\"},{\"id\":\"measureGroupCode\",\"type\":\"json\"},{\"id\":\"soughtValue\",\"type\":\"json\"},{\"id\":\"sourcesOfError\","
			+ "\"type\":\"text\"},{\"id\":\"coverageCalculation\",\"type\":\"text\"},{\"id\":\"numeratorDefinition\",\"type\":\"text\"},{\"id\":\"numeratorInclusionValueSet\",\"type\":\"json\"},"
			+ "{\"id\":\"numeratorExclusionValueSet\",\"type\":\"json\"},{\"id\":\"denominatorDefinition\",\"type\":\"text\"},{\"id\":\"denominatorInclusionValueSet\",\"type\":\"json\"},"
			+ "{\"id\":\"denominatorExclusionValueSet\",\"type\":\"json\"},{\"id\":\"unit\",\"type\":\"json\"},{\"id\":\"measurePopulationDefinition\",\"type\":\"text\"},{\"id\":\"measurePopulationInclusionValueSet\","
			+ "\"type\":\"json\"},{\"id\":\"measurePopulationExclusionValueSet\",\"type\":\"json\"},{\"id\":\"cohortDefinition\",\"type\":\"text\"},{\"id\":\"cohortInclusionValueSet\",\"type\":\"json\"},"
			+ "{\"id\":\"cohortExclusionValueSet\",\"type\":\"json\"},{\"id\":\"regionalAffinityCalculation\",\"type\":\"json\"},{\"id\":\"thresholdForViewing\",\"type\":\"int\"},{\"id\":\"measuringFrequency\","
			+ "\"type\":\"json\"},{\"id\":\"organizationLevel\",\"type\":\"json\"},{\"id\":\"interpretationTextPersonell\",\"type\":\"text\"},{\"id\":\"interpretationTextPublic\",\"type\":\"text\"},"
			+ "{\"id\":\"availableReportingSystemHsaId\",\"type\":\"text[]\"},{\"id\":\"serviceContractVersion\",\"type\":\"int\"},{\"id\":\"author\",\"type\":\"json\"},{\"id\":\"authenticator\","
			+ "\"type\":\"json\"}],\"primary_key\":\"measureFormerVersionId\",\"always_strings\":[\"measureId\"],\"always_arrays\":[\"regionalIndicator\",\"subCategoryCode\",\"descriptionSource\","
			+ "\"measureGroupCode\",\"denominatorInclusionValueSetId\",\"denominatorExclusionValueSetId\",\"numeratorInclusionValueSetId\",\"numeratorExclusionValueSetId\",\"measurePopulationInclusionValueSetId\","
			+ "\"measurePopulationExclusionValueSetId\",\"measuringFrequency\",\"organizationLevel\",\"availableReportingSystemHsaId\"]}";

	private String v2MeasuresData = "[{\"measureId\":\"02913191-53a2-420a-8028-fe3e1a828941\",\"author\":{\"authorName\":\"Ulrika Elmroth\",\"authorRoleCode\":null,\"authorOrganization\":\"SKL\","
			+ "\"authorContactInformation\":null},\"validTo\":\"2018-03-31\",\"parentMeasureDefinition\":null,\"lastUpdated\":null,\"authenticator\":null,\"validFrom\":\"2017-06-01\","
			+ "\"subCategoryDefinition\":{\"parentMeasureId\":\"dcfcea2e-ea53-4db5-b6bc-024d24b20556\",\"subCategoryCode\":[{\"code\":\"df4f77da-41f8-469f-aa2c-320f55017da4\","
			+ "\"system\":\"19bb7abe-7de4-420f-8259-7aca0a461b6f\"}]},\"publiclyAvailableInformation\":false,\"regionalIndicator\":null}]";
	
	private String v2IncorrectMeasuresData = "[{\"measureId\":\"02913191-53a2-420a-8028-fe3e1a828941\",\"author\":{\"authorName\":\"Ulrika Elmroth\",\"authorRoleCode\":null,"
			+ "\"authorOrganization\":\"SKL\",\"authorContactInformation\":null},\"validTo\":\"2018-03-31\",\"parentMeasureDefinition\":null,\"lastUpdated\":null,\"authenticator\":null,"
			+ "\"validFrom\":\"2017-06-01\",\"subCategoryDefinition\":{\"parentMeasureId\":\"dcfcea2e-ea53-4db5-b6bc-024d24b20556\",\"subCategoryCode\":[{\"code\":\"df4f77da-41f8-469f-aa2c-320f55017da4\","
			+ "\"system\":\"19bb7abe-7de4-420f-8259-7aca0a461b6f\"}]},\"publiclyAvailableInformation\":\"test\",\"regionalIndicator\":null}]";
	
	private String v2MeasuresMockedResult = "{\"resource\":{\"package_id\":\"kvalitetsindikatorer\",\"name\":\"kik-v3-measures\",\"hash\":\"kik-v3-measures__\"},\"fields\":[{\"id\":\"measureId\","
			+ "\"type\":\"text\"},{\"id\":\"lastUpdated\",\"type\":\"timestamp\"},{\"id\":\"validFrom\",\"type\":\"date\"},{\"id\":\"validTo\",\"type\":\"date\"},{\"id\":\"publiclyAvailableInformation\","
			+ "\"type\":\"bool\"},{\"id\":\"regionalIndicator\",\"type\":\"text[]\"},{\"id\":\"subCategoryDefinition\",\"type\":\"json\"},{\"id\":\"parentMeasureDefinition\",\"type\":\"json\"},"
			+ "{\"id\":\"author\",\"type\":\"json\"},{\"id\":\"authenticator\",\"type\":\"json\"}],\"primary_key\":\"measureId\",\"always_strings\":[\"measureId\"],\"always_arrays\":[\"regionalIndicator\","
			+ "\"subCategoryCode\",\"descriptionSource\",\"measureGroupCode\",\"denominatorInclusionValueSetId\",\"denominatorExclusionValueSetId\",\"numeratorInclusionValueSetId\",\"numeratorExclusionValueSetId\","
			+ "\"measurePopulationInclusionValueSetId\",\"measurePopulationExclusionValueSetId\",\"measuringFrequency\",\"organizationLevel\",\"availableReportingSystemHsaId\"]}";
	
	private String v3MeasuresData = "[{\"measureId\":\"02913191-53a2-420a-8028-fe3e1a828941\",\"author\":{\"authorName\":\"Ulrika Elmroth\",\"authorRoleCode\":null,\"authorOrganization\":\"SKL\","
			+ "\"authorContactInformation\":null},\"validTo\":\"2018-03-31\",\"parentMeasureDefinition\":null,\"lastUpdated\":null,\"authenticator\":null,\"validFrom\":\"2017-06-01\","
			+ "\"subCategoryDefinition\":{\"parentMeasureId\":\"dcfcea2e-ea53-4db5-b6bc-024d24b20556\",\"subCategoryCode\":[{\"code\":\"df4f77da-41f8-469f-aa2c-320f55017da4\","
			+ "\"system\":\"19bb7abe-7de4-420f-8259-7aca0a461b6f\"}]},\"publiclyAvailableInformation\":false,\"regionalIndicator\":null}]";
	
	private String v3IncorrectMeasuresData = "[{\"measureId\":\"02913191-53a2-420a-8028-fe3e1a828941\",\"author\":{\"authorName\":\"Ulrika Elmroth\",\"authorRoleCode\":null,"
			+ "\"authorOrganization\":\"SKL\",\"authorContactInformation\":null},\"validTo\":\"2018-03-31\",\"parentMeasureDefinition\":null,\"lastUpdated\":null,\"authenticator\":null,"
			+ "\"validFrom\":\"2017-06-01\",\"subCategoryDefinition\":{\"parentMeasureId\":\"dcfcea2e-ea53-4db5-b6bc-024d24b20556\",\"subCategoryCode\":[{\"code\":\"df4f77da-41f8-469f-aa2c-320f55017da4\","
			+ "\"system\":\"19bb7abe-7de4-420f-8259-7aca0a461b6f\"}]},\"publiclyAvailableInformation\":\"test\",\"regionalIndicator\":null}]";
	
	private String v3MeasuresMockedResult = "{\"resource\":{\"package_id\":\"kvalitetsindikatorer\",\"name\":\"kik-v3-measures\",\"hash\":\"kik-v3-measures__\"},\"fields\":[{\"id\":\"measureId\","
			+ "\"type\":\"text\"},{\"id\":\"lastUpdated\",\"type\":\"timestamp\"},{\"id\":\"validFrom\",\"type\":\"date\"},{\"id\":\"validTo\",\"type\":\"date\"},{\"id\":\"publiclyAvailableInformation\","
			+ "\"type\":\"bool\"},{\"id\":\"regionalIndicator\",\"type\":\"text[]\"},{\"id\":\"subCategoryDefinition\",\"type\":\"json\"},{\"id\":\"parentMeasureDefinition\",\"type\":\"json\"},"
			+ "{\"id\":\"author\",\"type\":\"json\"},{\"id\":\"authenticator\",\"type\":\"json\"}],\"primary_key\":\"measureId\",\"always_strings\":[\"measureId\"],\"always_arrays\":[\"regionalIndicator\","
			+ "\"subCategoryCode\",\"descriptionSource\",\"measureGroupCode\",\"denominatorInclusionValueSetId\",\"denominatorExclusionValueSetId\",\"numeratorInclusionValueSetId\",\"numeratorExclusionValueSetId\","
			+ "\"measurePopulationInclusionValueSetId\",\"measurePopulationExclusionValueSetId\",\"measuringFrequency\",\"organizationLevel\",\"availableReportingSystemHsaId\"]}";

	private String v2PerformingOrganizationsData = "[{\"performingOrganizationHsaId\":\"SE\",\"performingOrganizationId\":\"54490c573150bc438c644337\",\"asOrganizationPartOf\":null,"
			+ "\"performingOrganizationType\":{\"code\":\"5447722f31506222d0b0f02a\",\"system\":\"5447717431505822d0f4f4e8\"},\"performingOrganizationName\":\"Sverige\"}]";

	private String v2IncorrectPerformingOrganizationsData = "[{\"performingOrganizationHsaId\":false,\"performingOrganizationId\":\"54490c573150bc438c644337\",\"asOrganizationPartOf\":null,"
			+ "\"performingOrganizationType\":{\"code\":\"5447722f31506222d0b0f02a\",\"system\":\"5447717431505822d0f4f4e8\"},\"performingOrganizationName\":\"Sverige\"}]";
		
	private String v2PerformingOrganizationsMockedResult = "{\"resource\":{\"package_id\":\"kvalitetsindikatorer\",\"name\":\"kik-v3-performingOrganizations\","
			+ "\"hash\":\"kik-v3-performingOrganizations__\"},\"fields\":[{\"id\":\"performingOrganizationId\",\"type\":\"text\"},{\"id\":\"performingOrganizationHsaId\","
			+ "\"type\":\"text\"},{\"id\":\"performingOrganizationName\",\"type\":\"text\"},{\"id\":\"performingOrganizationType\",\"type\":\"json\"},{\"id\":\"asOrganizationPartOf\","
			+ "\"type\":\"json\"}],\"primary_key\":\"performingOrganizationId\",\"always_strings\":\"performingOrganizationHsaId\",\"always_arrays\":[\"organizationalTarget\"]}";
	
	private String v3PerformingOrganizationsData = "[{\"performingOrganizationHsaId\":\"SE\",\"performingOrganizationId\":\"54490c573150bc438c644337\",\"asOrganizationPartOf\":null,"
			+ "\"performingOrganizationType\":{\"code\":\"5447722f31506222d0b0f02a\",\"system\":\"5447717431505822d0f4f4e8\"},\"performingOrganizationName\":\"Sverige\"}]";

	private String v3IncorrectPerformingOrganizationsData = "[{\"performingOrganizationHsaId\":false,\"performingOrganizationId\":\"54490c573150bc438c644337\",\"asOrganizationPartOf\":null,"
			+ "\"performingOrganizationType\":{\"code\":\"5447722f31506222d0b0f02a\",\"system\":\"5447717431505822d0f4f4e8\"},\"performingOrganizationName\":\"Sverige\"}]";
		
	private String v3PerformingOrganizationsMockedResult = "{\"resource\":{\"package_id\":\"kvalitetsindikatorer\",\"name\":\"kik-v3-performingOrganizations\","
			+ "\"hash\":\"kik-v3-performingOrganizations__\"},\"fields\":[{\"id\":\"performingOrganizationId\",\"type\":\"text\"},{\"id\":\"performingOrganizationHsaId\","
			+ "\"type\":\"text\"},{\"id\":\"performingOrganizationName\",\"type\":\"text\"},{\"id\":\"performingOrganizationType\",\"type\":\"json\"},{\"id\":\"asOrganizationPartOf\","
			+ "\"type\":\"json\"}],\"primary_key\":\"performingOrganizationId\",\"always_strings\":\"performingOrganizationHsaId\",\"always_arrays\":[\"organizationalTarget\"]}";

	private String v2TargetMeasurementsData = "[{\"targetType\":{\"code\":\"a4a84aba-1432-4294-8b74-c2b775830546\",\"system\":\"1.2.826.0.1.3680043.9.4672.8\"},"
			+ "\"authoringOrganization\":\"SWEDEHEART - RIKS-HIA\",\"targetProportionMeasure\":{\"targetRate\":0.9},\"targetOrganizationId\":\"54490c573150bc438c644337\","
			+ "\"targetContinuousMeasure\":null,\"validTo\":null,\"author\":{\"authorName\":\"SKL\",\"authorRoleCode\":null,\"authorOrganization\":null,\"authorContactInformation\":null},"
			+ "\"targetId\":\"3aaf1b08-bb75-4ac6-beb2-0f2a5a251ca2\",\"targetMeasureId\":\"b62f3ad3-c7b8-449e-afbc-3f26d49084eb\",\"targetCohortMeasure\":null,\"validFrom\":\"2013-01-01\","
			+ "\"targetName\":\"I SWEDEHEARTs kvalitetsindex är den högre målnivån satt till 90 procent.\",\"targetDescription\":\"I SWEDEHEARTs kvalitetsindex är den högre målnivån satt till 90 procent.\"}]";
	
	private String v2IncorrectTargetMeasurementsData = "[{\"targetType\":{\"code\":\"a4a84aba-1432-4294-8b74-c2b775830546\",\"system\":\"1.2.826.0.1.3680043.9.4672.8\"},"
			+ "\"authoringOrganization\":\"SWEDEHEART - RIKS-HIA\",\"targetProportionMeasure\":{\"targetRate\":0.9},\"targetOrganizationId\":\"54490c573150bc438c644337\","
			+ "\"targetContinuousMeasure\":null,\"validTo\":null,\"author\":{\"authorName\":\"SKL\",\"authorRoleCode\":null,\"authorOrganization\":null,\"authorContactInformation\":null},"
			+ "\"targetId\":\"3aaf1b08-bb75-4ac6-beb2-0f2a5a251ca2\",\"targetMeasureId\":\"b62f3ad3-c7b8-449e-afbc-3f26d49084eb\",\"targetCohortMeasure\":null,\"validFrom\":\"2013-01-01\","
			+ "\"targetName\":\"I SWEDEHEARTs kvalitetsindex är den högre målnivån satt till 90 procent.\",\"targetDescription\":false}]";
	
	private String v2TargetMeasurementsMockedResult = "{\"resource\":{\"package_id\":\"kvalitetsindikatorer\",\"name\":\"kik-v2-targetMeasurements\",\"hash\":"
			+ "\"kik-v2-targetMeasurements__\"},\"fields\":[{\"id\":\"targetId\",\"type\":\"text\"},{\"id\":\"validFrom\",\"type\":\"date\"},{\"id\":\"validTo\",\"type\":\"date\"},"
			+ "{\"id\":\"targetName\",\"type\":\"text\"},{\"id\":\"targetDescription\",\"type\":\"text\"},{\"id\":\"targetType\",\"type\":\"json\"},{\"id\":\"targetMeasureId\","
			+ "\"type\":\"text\"},{\"id\":\"targetOrganizationId\",\"type\":\"text\"},{\"id\":\"authoringOrganization\",\"type\":\"text\"},{\"id\":\"targetProportionMeasure\","
			+ "\"type\":\"json\"},{\"id\":\"targetContinuousMeasure\",\"type\":\"json\"},{\"id\":\"targetCohortMeasure\",\"type\":\"json\"},{\"id\":\"author\",\"type\":\"json\"}],"
			+ "\"primary_key\":\"targetId\",\"always_strings\":[\"targetMeasureId\"]}";
	
	private String v3TargetMeasurementsData = "[{\"targetType\":{\"code\":\"a4a84aba-1432-4294-8b74-c2b775830546\",\"system\":\"1.2.826.0.1.3680043.9.4672.8\"},"
			+ "\"authoringOrganization\":\"SWEDEHEART - RIKS-HIA\",\"targetProportionMeasure\":{\"targetRate\":0.9},\"targetOrganizationId\":\"54490c573150bc438c644337\","
			+ "\"targetContinuousMeasure\":null,\"validTo\":null,\"author\":{\"authorName\":\"SKL\",\"authorRoleCode\":null,\"authorOrganization\":null,\"authorContactInformation\":null},"
			+ "\"targetId\":\"3aaf1b08-bb75-4ac6-beb2-0f2a5a251ca2\",\"targetMeasureId\":\"b62f3ad3-c7b8-449e-afbc-3f26d49084eb\",\"targetCohortMeasure\":null,\"validFrom\":\"2013-01-01\","
			+ "\"targetName\":\"I SWEDEHEARTs kvalitetsindex är den högre målnivån satt till 90 procent.\",\"targetDescription\":\"I SWEDEHEARTs kvalitetsindex är den högre målnivån satt till 90 procent.\"}]";
	
	private String v3IncorrectTargetMeasurementsData = "[{\"targetType\":{\"code\":\"a4a84aba-1432-4294-8b74-c2b775830546\",\"system\":\"1.2.826.0.1.3680043.9.4672.8\"},"
			+ "\"authoringOrganization\":\"SWEDEHEART - RIKS-HIA\",\"targetProportionMeasure\":{\"targetRate\":0.9},\"targetOrganizationId\":\"54490c573150bc438c644337\","
			+ "\"targetContinuousMeasure\":null,\"validTo\":null,\"author\":{\"authorName\":\"SKL\",\"authorRoleCode\":null,\"authorOrganization\":null,\"authorContactInformation\":null},"
			+ "\"targetId\":\"3aaf1b08-bb75-4ac6-beb2-0f2a5a251ca2\",\"targetMeasureId\":\"b62f3ad3-c7b8-449e-afbc-3f26d49084eb\",\"targetCohortMeasure\":null,\"validFrom\":\"2013-01-01\","
			+ "\"targetName\":\"I SWEDEHEARTs kvalitetsindex är den högre målnivån satt till 90 procent.\",\"targetDescription\":false}]";
	
	private String v3TargetMeasurementsMockedResult = "{\"resource\":{\"package_id\":\"kvalitetsindikatorer\",\"name\":\"kik-v3-targetMeasurements\","
			+ "\"hash\":\"kik-v3-targetMeasurements__\"},\"fields\":[{\"id\":\"targetId\",\"type\":\"text\"},{\"id\":\"validFrom\",\"type\":\"date\"},{\"id\":\"validTo\","
			+ "\"type\":\"date\"},{\"id\":\"targetName\",\"type\":\"text\"},{\"id\":\"targetDescription\",\"type\":\"text\"},{\"id\":\"targetType\",\"type\":\"json\"},"
			+ "{\"id\":\"targetMeasureId\",\"type\":\"text\"},{\"id\":\"targetOrganizationId\",\"type\":\"text\"},{\"id\":\"authoringOrganization\",\"type\":\"text\"},"
			+ "{\"id\":\"targetProportionMeasure\",\"type\":\"json\"},{\"id\":\"targetContinuousMeasure\",\"type\":\"json\"},{\"id\":\"targetCohortMeasure\",\"type\":\"json\"},"
			+ "{\"id\":\"author\",\"type\":\"json\"}],\"primary_key\":\"targetId\",\"always_strings\":[\"targetMeasureId\"]}";

	private String v2ValueSetsData = "[{\"active\":false,\"codeInValueSet\":[{\"code\":\"H664\",\"system\":\"1.2.752.116.1.1.1.1.1\"}],\"valueSetId\":"
			+ "\"05718c0f-e3b8-4b97-a547-7ab0fc404874\",\"name\":\"Varig mellanöreinflammation, ej specificerad som akut eller kronisk\"},{\"active\":false,"
			+ "\"codeInValueSet\":[{\"code\":\"J01XX05\",\"system\":\"2.16.840.1.113883.6.73\"}],\"valueSetId\":\"12dbedc1-c41f-450f-add0-8cb992f04237\",\"name\":\"Metenamin\"}]";

	private String v2IncorrectValueSetsData = "[{\"active\":\"fel input\",\"codeInValueSet\":[{\"code\":\"H664\",\"system\":\"1.2.752.116.1.1.1.1.1\"}],\"valueSetId\":"
			+ "\"05718c0f-e3b8-4b97-a547-7ab0fc404874\",\"name\":\"Varig mellanöreinflammation, ej specificerad som akut eller kronisk\"},{\"active\":false,"
			+ "\"codeInValueSet\":[{\"code\":\"J01XX05\",\"system\":\"2.16.840.1.113883.6.73\"}],\"valueSetId\":\"12dbedc1-c41f-450f-add0-8cb992f04237\",\"name\":\"Metenamin\"}]";

	private String v2ValueSetsMockedResult = "{\"resource\":{\"package_id\":\"kvalitetsindikatorer\",\"name\":\"kik-v2-valueSets\",\"hash\":\"kik-v2-valueSets__\"},\"fields\":"
			+ "[{\"id\":\"valueSetId\",\"type\":\"text\"},{\"id\":\"name\",\"type\":\"text\"},{\"id\":\"active\",\"type\":\"bool\"},{\"id\":\"codeInValueSet\",\"type\":\"json\"}],"
			+ "\"primary_key\":\"valueSetId\",\"always_arrays\":[\"codeInValueSet\"]}";
	
	private String v3ValueSetsData = "[{\"active\":false,\"codeInValueSet\":[{\"code\":\"H664\",\"system\":\"1.2.752.116.1.1.1.1.1\"}],\"valueSetId\":"
			+ "\"05718c0f-e3b8-4b97-a547-7ab0fc404874\",\"name\":\"Varig mellanöreinflammation, ej specificerad som akut eller kronisk\"},{\"active\":false,"
			+ "\"codeInValueSet\":[{\"code\":\"J01XX05\",\"system\":\"2.16.840.1.113883.6.73\"}],\"valueSetId\":\"12dbedc1-c41f-450f-add0-8cb992f04237\",\"name\":\"Metenamin\"}]";

	private String v3IncorrectValueSetsData = "[{\"active\":\"fel input\",\"codeInValueSet\":[{\"code\":\"H664\",\"system\":\"1.2.752.116.1.1.1.1.1\"}],\"valueSetId\":"
			+ "\"05718c0f-e3b8-4b97-a547-7ab0fc404874\",\"name\":\"Varig mellanöreinflammation, ej specificerad som akut eller kronisk\"},{\"active\":false,"
			+ "\"codeInValueSet\":[{\"code\":\"J01XX05\",\"system\":\"2.16.840.1.113883.6.73\"}],\"valueSetId\":\"12dbedc1-c41f-450f-add0-8cb992f04237\",\"name\":\"Metenamin\"}]";	
	
	private String v3ValueSetsMockedResult = "{\"resource\":{\"package_id\":\"kvalitetsindikatorer\",\"name\":\"kik-v3-valueSets\",\"hash\":\"kik-v3-valueSets__\"},\"fields\":"
			+ "[{\"id\":\"valueSetId\",\"type\":\"text\"},{\"id\":\"name\",\"type\":\"text\"},{\"id\":\"active\",\"type\":\"bool\"},{\"id\":\"codeInValueSet\",\"type\":\"json\"}],"
			+ "\"primary_key\":\"valueSetId\",\"always_arrays\":[\"codeInValueSet\"]}";
	
	private String v3ViewGroupsData = "[{\"viewTemplateReferenceId\":null,\"purposeDescription\":\"Presentation av indikatorer i PrimärvårdsKvalitet\",\"displayName\":"
			+ "\"PrimärvårdsKvalitet\",\"id\":\"2da23398-d8e0-4ab5-8f11-4fb9ea8a7621\",\"measureReference\":[{\"measureId\":\"ee3f3e87-db43-42e1-8577-7d66c5611f0d\",\"label\":null}]}]";

	private String v3IncorrectViewGroupsData = "[{\"viewTemplateReferenceId\":false,\"purposeDescription\":\"Presentation av indikatorer i PrimärvårdsKvalitet\",\"displayName\":"
			+ "\"PrimärvårdsKvalitet\",\"id\":\"2da23398-d8e0-4ab5-8f11-4fb9ea8a7621\",\"measureReference\":[{\"measureId\":\"ee3f3e87-db43-42e1-8577-7d66c5611f0d\",\"label\":null}]}]";

	private String v3ViewGroupsMockedResult = "{\"resource\":{\"package_id\":\"kvalitetsindikatorer\",\"name\":\"kik-v3-viewGroups\",\"hash\":\"kik-v3-viewGroups__\"},"
			+ "\"fields\":[{\"id\":\"id\",\"type\":\"text\"},{\"id\":\"displayName\",\"type\":\"text\"},{\"id\":\"purposeDescription\",\"type\":\"text\"},{\"id\":\"measureReference\","
			+ "\"type\":\"json\"},{\"id\":\"viewTemplateReferenceId\",\"type\":\"text\"}],\"primary_key\":\"id\"}";
	
	private String v3ViewTemplatesData = "[{\"description\":\"Ska visa sex enkätsvar tillsammans i rätt ordning från alternativ 6 t o m 1. Från positivt till mer negativa "
			+ "svarsalternativ. Detta för att göra data för olika enheter jämförbart. Positiva svar ska representeras med positiv färg, negativa med negativ färg, neutrala med neutral "
			+ "färg (olika färger för Varken ja eller nej - och Kan/vill ej svara). Enskilda indikatorer kan visas enskilt om grupptillhörigheten är tydlig, samma gäller om data exporteras.\","
			+ "\"availableLabel\":\"{\\\"1\\u003d Nej, inte alls\\\",\\\"2\\u003d Nej, sällan\\\",\\\"3\\u003d Varken ja eller nej\\\",\\\"4 \\u003d Ja, oftast\\\",\\\"5 \\u003d Ja, "
			+ "helt och hållet\\\",\\\"6\\u003d Kan/vill ej svara\\\"}\",\"id\":\"7f9d39e6-0426-4fca-906d-93748c6596d0\",\"name\":\"Sex enkätsvar\"}]";
	
	private String v3IncorrectViewTemplatesData = "[{\"description\":\"Ska visa sex enkätsvar tillsammans i rätt ordning från alternativ 6 t o m 1. Från positivt "
			+ "till mer negativa svarsalternativ. Detta för att göra data för olika enheter jämförbart. Positiva svar ska representeras med positiv färg, negativa med negativ färg, "
			+ "neutrala med neutral färg (olika färger för Varken ja eller nej - och Kan/vill ej svara). Enskilda indikatorer kan visas enskilt om grupptillhörigheten är tydlig, "
			+ "samma gäller om data exporteras.\",\"availableLabel\":\"{\\\"1\\u003d Nej, inte alls\\\",\\\"2\\u003d Nej, sällan\\\",\\\"3\\u003d Varken ja eller nej\\\",\\\"4 \\u003d Ja, oftast\\\","
			+ "\\\"5 \\u003d Ja, helt och hållet\\\",\\\"6\\u003d Kan/vill ej svara\\\"}\",\"id\":\"7f9d39e6-0426-4fca-906d-93748c6596d0\",\"name\":false}]";
		
	private String v3ViewTemplatesMockedResult = "{\"resource\":{\"package_id\":\"kvalitetsindikatorer\",\"name\":\"kik-v3-viewTemplates\",\"hash\":"
			+ "\"kik-v3-viewTemplates__\"},\"fields\":[{\"id\":\"id\",\"type\":\"text\"},{\"id\":\"name\",\"type\":\"text\"},{\"id\":\"description\","
			+ "\"type\":\"text\"},{\"id\":\"availableLabel\",\"type\":\"text\"}],\"primary_key\":\"id\"}";

	
	@SuppressWarnings("unchecked")
	private boolean doValidate(String template, String data) throws IOException
	{
		ODPRequest<Object> req = mapper.readValue(template, ODPRequest.class);

		req.setRecords(mapper.readValue(data, List.class));
		
		return validator.validateResource(req);
	}

	@Test
	public void testValidateCorrectV2ChecksumsForPost() throws JsonParseException, JsonMappingException, IOException
	{
		assertEquals(doValidate(v2ChecksumsMockedResult, v2ChecksumsData), true);
	}

	@Test(expected=ODPException.class)
	public void testValidateIncorrectV2ChecksumsForPost() throws JsonParseException, JsonMappingException, IOException
	{
		doValidate(v2ChecksumsMockedResult, v2IncorrectChecksumsData);		
	}
	
	@Test
	public void testValidateCorrectV3ChecksumsForPost() throws JsonParseException, JsonMappingException, IOException
	{
		assertEquals(doValidate(v3ChecksumsMockedResult, v3ChecksumsData), true);
	}
	
	@Test(expected=ODPException.class)
	public void testValidateIncorrectV3ChecksumsForPost() throws JsonParseException, JsonMappingException, IOException
	{
		doValidate(v3ChecksumsMockedResult, v3IncorrectChecksumsData);		
	}

	@Test
	public void testValidateCorrectV2CodesForPost() throws JsonParseException, JsonMappingException, IOException
	{
		assertEquals(doValidate(v2CodesMockedResult, v2CodesData), true);
	}

	@Test(expected=ODPException.class)
	public void testValidateIncorrectV2CodesForPost() throws JsonParseException, JsonMappingException, IOException
	{
		doValidate(v2CodesMockedResult, v2IncorrectCodesData);		
	}	
	
	@Test
	public void testValidateCorrectV3CodesForPost() throws JsonParseException, JsonMappingException, IOException
	{
		assertEquals(doValidate(v3CodesMockedResult, v3CodesData), true);
	}
	
	@Test(expected=ODPException.class)
	public void testValidateIncorrectV3CodesForPost() throws JsonParseException, JsonMappingException, IOException
	{
		doValidate(v3CodesMockedResult, v3IncorrectCodesData);		
	}
	
	@Test
	public void testValidateCorrectV2CodeSystemsForPost() throws JsonParseException, JsonMappingException, IOException
	{
		assertEquals(doValidate(v2CodeSystemsMockedResult, v2CodeSystemsData), true);
	}
	
	@Test(expected=ODPException.class)
	public void testValidateIncorrectV2CodeSystemsForPost() throws JsonParseException, JsonMappingException, IOException
	{
		doValidate(v2CodeSystemsMockedResult, v2IncorrectCodeSystemsData);		
	}
	
	@Test
	public void testValidateCorrectV3CodeSystemsForPost() throws JsonParseException, JsonMappingException, IOException
	{
		assertEquals(doValidate(v3CodeSystemsMockedResult, v3CodeSystemsData), true);
	}
	
	@Test(expected=ODPException.class)
	public void testValidateIncorrectV3CodeSystemsForPost() throws JsonParseException, JsonMappingException, IOException
	{
		doValidate(v3CodeSystemsMockedResult, v3IncorrectCodeSystemsData);		
	}
	
	@Test
	public void testValidateCorrectV2MeasureFormerVersionsForPost() throws JsonParseException, JsonMappingException, IOException
	{
		assertEquals(doValidate(v2MeasureFormerVersionsMockedResult, v2MeasureFormerVersionsData), true);
	}
	
	@Test(expected=ODPException.class)
	public void testValidateIncorrectV2MeasureFormerVersionsForPost() throws JsonParseException, JsonMappingException, IOException
	{
		doValidate(v2MeasureFormerVersionsMockedResult, v2IncorrectMeasureFormerVersionsData);		
	}

	@Test
	public void testValidateCorrectV2MeasuresForPost() throws JsonParseException, JsonMappingException, IOException
	{
		assertEquals(doValidate(v2MeasuresMockedResult, v2MeasuresData), true);
	}

	@Test(expected=ODPException.class)
	public void testValidateIncorrectV2MeasuresForPost() throws JsonParseException, JsonMappingException, IOException
	{
		doValidate(v2MeasuresMockedResult, v2IncorrectMeasuresData);		
	}
	
	@Test
	public void testValidateCorrectV3MeasuresForPost() throws JsonParseException, JsonMappingException, IOException
	{
		assertEquals(doValidate(v3MeasuresMockedResult, v3MeasuresData), true);
	}
	
	@Test(expected=ODPException.class)
	public void testValidateIncorrectV3MeasuresForPost() throws JsonParseException, JsonMappingException, IOException
	{
		doValidate(v3MeasuresMockedResult, v3IncorrectMeasuresData);		
	}
	
	@Test
	public void testValidateCorrectV2PerformingOrganizationsForPost() throws JsonParseException, JsonMappingException, IOException
	{
		assertEquals(doValidate(v2PerformingOrganizationsMockedResult, v2PerformingOrganizationsData), true);
	}
		
	@Test(expected=ODPException.class)
	public void testValidateIncorrectV2PerformingOrganizationsForPost() throws JsonParseException, JsonMappingException, IOException
	{
		doValidate(v2PerformingOrganizationsMockedResult, v2IncorrectPerformingOrganizationsData);		
	}
	
	@Test
	public void testValidateCorrectV3PerformingOrganizationsForPost() throws JsonParseException, JsonMappingException, IOException
	{
		assertEquals(doValidate(v3PerformingOrganizationsMockedResult, v3PerformingOrganizationsData), true);
	}
	
	@Test(expected=ODPException.class)
	public void testValidateIncorrectV3PerformingOrganizationsForPost() throws JsonParseException, JsonMappingException, IOException
	{
		doValidate(v3PerformingOrganizationsMockedResult, v3IncorrectPerformingOrganizationsData);		
	}
	
	@Test
	public void testValidateCorrectV2TargetMeasurementsForPost() throws JsonParseException, JsonMappingException, IOException
	{
		assertEquals(doValidate(v2TargetMeasurementsMockedResult, v2TargetMeasurementsData), true);
	}
	
	@Test(expected=ODPException.class)
	public void testValidateIncorrectV2TargetMeasurementsForPost() throws JsonParseException, JsonMappingException, IOException
	{
		doValidate(v2TargetMeasurementsMockedResult, v2IncorrectTargetMeasurementsData);		
	}
	
	@Test
	public void testValidateCorrectV3TargetMeasurementsForPost() throws JsonParseException, JsonMappingException, IOException
	{
		assertEquals(doValidate(v3TargetMeasurementsMockedResult, v3TargetMeasurementsData), true);
	}
	
	@Test(expected=ODPException.class)
	public void testValidateIncorrectV3TargetMeasurementsForPost() throws JsonParseException, JsonMappingException, IOException
	{
		doValidate(v3TargetMeasurementsMockedResult, v3IncorrectTargetMeasurementsData);		
	}

	@Test
	public void testValidateCorrectV2ValueSetsForPost() throws JsonParseException, JsonMappingException, IOException
	{
		assertEquals(doValidate(v2ValueSetsMockedResult, v2ValueSetsData), true);
	}
	
	@Test(expected=ODPException.class)
	public void testValidateIncorrectV2ValueSetsForPost() throws JsonParseException, JsonMappingException, IOException
	{
		doValidate(v2ValueSetsMockedResult, v2IncorrectValueSetsData);		
	}
	
	@Test
	public void testValidateCorrectV3ValueSetsForPost() throws JsonParseException, JsonMappingException, IOException
	{
		assertEquals(doValidate(v3ValueSetsMockedResult, v3ValueSetsData), true);
	}
	
	@Test(expected=ODPException.class)
	public void testValidateIncorrectV3ValueSetsForPost() throws JsonParseException, JsonMappingException, IOException
	{
		doValidate(v3ValueSetsMockedResult, v3IncorrectValueSetsData);		
	}
	
	@Test
	public void testValidateCorrectV3ViewGroupsForPost() throws JsonParseException, JsonMappingException, IOException
	{
		assertEquals(doValidate(v3ViewGroupsMockedResult, v3ViewGroupsData), true);
	}
	
	@Test(expected=ODPException.class)
	public void testValidateIncorrectV3ViewGroupsForPost() throws JsonParseException, JsonMappingException, IOException
	{
		doValidate(v3ViewGroupsMockedResult, v3IncorrectViewGroupsData);		
	}
	
	@Test
	public void testValidateCorrectV3ViewTemplatesForPost() throws JsonParseException, JsonMappingException, IOException
	{
		assertEquals(doValidate(v3ViewTemplatesMockedResult, v3ViewTemplatesData), true);
	}
	
	@Test(expected=ODPException.class)
	public void testValidateIncorrectV3ViewTemplatesForPost() throws JsonParseException, JsonMappingException, IOException
	{
		doValidate(v3ViewTemplatesMockedResult, v3IncorrectViewTemplatesData);		
	}
	
}