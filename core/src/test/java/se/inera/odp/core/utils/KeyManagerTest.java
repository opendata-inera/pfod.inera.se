package se.inera.odp.core.utils;

import org.junit.Test;

import org.junit.Assert;
import se.inera.odp.core.utils.KeyManager;

public class KeyManagerTest {

    @Test
    public void failIfCanNotRead() {
    	KeyManager keyManager = new KeyManager("read", "write", "");
    	Assert.assertTrue(keyManager.canRead("read"));
    	Assert.assertTrue(keyManager.canRead("write"));
    	Assert.assertTrue(!keyManager.canRead("wrong"));
    	Assert.assertTrue(keyManager.canRead(null));
    }
    
    @Test
    public void failIfCanNotWrite() {
    	KeyManager keyManager = new KeyManager("read", "write", "");
    	Assert.assertTrue(!keyManager.canWrite("read"));
    	Assert.assertTrue(keyManager.canWrite("write"));
    	Assert.assertTrue(!keyManager.canWrite("wrong"));
    	Assert.assertTrue(!keyManager.canWrite(null));
    }
    
    @Test
    public void testGetDatasetKey() {
    	KeyManager keyManager = new KeyManager("read", "write", "key");
    	Assert.assertTrue(keyManager.getDatasetKeyForRead("read").equals("key"));
    	Assert.assertTrue(keyManager.getDatasetKeyForRead("write").equals("key"));
    	Assert.assertTrue(keyManager.getDatasetKeyForWrite("write").equals("key"));
    	try {
    		keyManager.getDatasetKeyForWrite("write1").equals("key");
    		Assert.assertFalse(true);
    	} catch(Exception e) {}
    }
    
    
}
