package se.inera.odp.core.config;

import java.nio.charset.Charset;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import org.springframework.web.reactive.function.client.WebClient;
import se.inera.odp.core.utils.KeyManager;
import se.inera.odp.core.utils.ResourceValidator;

@Configuration
public class CommonConfig {

	@Bean
    public RestTemplate getRestTemplate() {
		
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
	    return restTemplate;
	}

	@Bean
	public WebClient webClient() {
		WebClient webClient = WebClient.builder()
			.defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
			.defaultHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
			.build();
		return webClient;
	}
  
	@Value("${app.security.readkey:}")
	private String readKey;
	@Value("${app.security.writekey:}")
	private String writeKey;
	@Value("${app.dataset.key:}")
	private String datasetKey;
	
	@Bean
	KeyManager keyManager() {
		KeyManager keyManager = new KeyManager(readKey, writeKey, datasetKey);
		return keyManager;
	}
	
	@Bean
	ResourceValidator resourceValidator() {
		ResourceValidator resourceValidator = new ResourceValidator();
		return resourceValidator;
	}
}