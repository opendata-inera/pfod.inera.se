package se.inera.odp.core.response;

public class ResponseMessage {

    private int status;
    private String message;
    private String endpoint;
    private int resultSize;
    
    public ResponseMessage() {}
    
	public ResponseMessage(int status, String message, String endpoint, int resultSize) {
		super();
		this.status = status;
		this.message = message;
		this.endpoint = endpoint;
		this.resultSize = resultSize;
	}

	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getEndpoint() {
		return endpoint;
	}
	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}
	public int getResultSize() {
		return resultSize;
	}
	public void setResultSize(int size) {
		this.resultSize = size;
	}

}
