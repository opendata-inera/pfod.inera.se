package se.inera.odp.core.controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.HttpMediaTypeException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.util.WebUtils;

import se.inera.odp.core.exception.*;
import se.inera.odp.core.utils.ResponseLoggerMapper;
import springfox.documentation.annotations.ApiIgnore;

@ApiIgnore
@RestControllerAdvice
@RestController
public class RestResponseEntityExceptionHandler implements ErrorController {

	Logger logger = LoggerFactory.getLogger(RestResponseEntityExceptionHandler.class);

    private static final String ERROR_MAPPING = "/error";

    @RequestMapping(value = ERROR_MAPPING)
    public ResponseEntity<String> error() {
        return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
    }

    @Override
    public String getErrorPath() {
        return ERROR_MAPPING;
    }

    @RequestMapping(value = "/csrf")
    public ResponseEntity<String> csrf() {
        return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
    }

	@Autowired
	ResponseLoggerMapper responseMapper;

	// Handles a feature i swagger2
	@GetMapping(value= "/")
	public ResponseEntity<String> noAction() {
		return new ResponseEntity<String>(HttpStatus.OK);		
	}
	
    @ExceptionHandler(value = { ODPAuthorizationException.class })
    protected ResponseEntity<Object> handleODPAuthorizationException(RuntimeException ex, WebRequest request) {
        Map<String, Object> bodyOfResponse = responseMapper.errorMessage(HttpStatus.FORBIDDEN, ex.getMessage(), responseMapper.getUri(request));
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.FORBIDDEN, request);
    }

    @ExceptionHandler(value = { ODPException.class })
    protected ResponseEntity<Object> handleODPException(ODPException ex, WebRequest request) {
    	Map<String, Object>  bodyOfResponse = responseMapper.errorMessage(ex.getStatus(), ex.getMessage(), responseMapper.getUri(request), ex.getErrCode());
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), ex.getStatus(), request);
    }

    @ExceptionHandler(value = { HttpRequestMethodNotSupportedException.class })
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException ex, WebRequest request) {
    	Map<String, Object>  bodyOfResponse = responseMapper.errorMessage(HttpStatus.BAD_REQUEST, ex.getMessage(), responseMapper.getUri(request));
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }
   
    @ExceptionHandler(value = { HttpStatusCodeException.class, HttpServerErrorException.class, HttpClientErrorException.class})
    protected ResponseEntity<Object> handleRestClientException(HttpStatusCodeException ex, WebRequest request) {
    	
    	if(ex.getStatusCode().equals(HttpStatus.FORBIDDEN) || ex.getStatusCode().equals(HttpStatus.UNAUTHORIZED))
    		return handleODPAuthorizationException(ex, request);
    	else {
	        Map<String, Object> bodyOfResponse = responseMapper.errorMessage(ex.getStatusCode(), "CKAN response error", responseMapper.getUri(request));
	        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), ex.getStatusCode(), request);
    	}
    }

    @ExceptionHandler(value = { RestClientException.class})
    protected ResponseEntity<Object> handleRestClientException(RestClientException ex, WebRequest request) {
    	Map<String, Object>  bodyOfResponse = responseMapper.errorMessage(HttpStatus.INTERNAL_SERVER_ERROR, " Internal server error", responseMapper.getUri(request));
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }
 
    @ExceptionHandler(value = { NoHandlerFoundException.class })
    protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, WebRequest request) {
    	Map<String, Object>  bodyOfResponse = responseMapper.errorMessage(HttpStatus.BAD_REQUEST, ex.getMessage(), responseMapper.getUri(request));
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }
    @ExceptionHandler(value = { HttpMediaTypeException.class })
    protected ResponseEntity<Object> handleHttpMediaTypeException(HttpMediaTypeException ex, WebRequest request) {
    	Map<String, Object>  bodyOfResponse = responseMapper.errorMessage(HttpStatus.BAD_REQUEST, ex.getMessage(), responseMapper.getUri(request));
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }
    @ExceptionHandler(value = { HttpMediaTypeNotAcceptableException.class })
    protected ResponseEntity<Object> handleHttpMediaTypeNotAcceptableException(HttpMediaTypeNotAcceptableException ex, WebRequest request) {
    	Map<String, Object>  bodyOfResponse = responseMapper.errorMessage(HttpStatus.BAD_REQUEST, ex.getMessage(), responseMapper.getUri(request));
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }
           
    // Will occur when illegal enum value is used
    @ExceptionHandler(value = { MethodArgumentTypeMismatchException.class })
    protected ResponseEntity<Object> handleMethodArgumentTypeMismatchException(Exception ex, WebRequest request) {
        Map<String, Object> bodyOfResponse = responseMapper.errorMessage(HttpStatus.BAD_REQUEST, "Request not supported", request.getDescription(false));
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }
     
    @ExceptionHandler(value = { IllegalArgumentException.class })
    protected ResponseEntity<Object> handleIllegalArgumentException(Exception ex, WebRequest request) {
        Map<String, Object> bodyOfResponse = responseMapper.errorMessage(HttpStatus.BAD_REQUEST, "Illegal argument", request.getDescription(false));
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }
     
    @ExceptionHandler(value = { Exception.class, RuntimeException.class })
    protected ResponseEntity<Object> handleException(Exception ex, WebRequest request) {
        Map<String, Object> bodyOfResponse = responseMapper.errorMessage(HttpStatus.INTERNAL_SERVER_ERROR, " Unhandled exception: " + ex.getMessage(), request.getDescription(false));
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }
 
    
	private ResponseEntity<Object> handleExceptionInternal(
			Exception ex, @Nullable Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {

		if (HttpStatus.INTERNAL_SERVER_ERROR.equals(status)) {
			request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, ex, WebRequest.SCOPE_REQUEST);
		}
		return new ResponseEntity<>(body, headers, status);
	}
	
	private ResponseEntity<Object> handleExceptionInternal(
			Exception ex, @Nullable Object body, HttpHeaders headers, HttpStatus status) {

		return new ResponseEntity<>(body, headers, status);
	}
	
}
