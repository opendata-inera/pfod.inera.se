package se.inera.odp.core.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import se.inera.odp.core.response.ODPResponse;

public interface ODPServiceInterface {

	void setMapper(ObjectMapper mapper);

	/**
	 * PFÖD 2.0 style
	 * @deprecated
	 * @param dataset_id
	 * @param resource_name
	 * @param params
	 * @param auth
	 * @param url
	 * @return
	 */
	String getResourceById(String dataset_id, String resource_name, Map<String, String> params, String auth,
			String url);

	List getResourceAsList(String dataset_id, String resource_name, Map<String, String> params, String auth,
			String url);

	ODPResponse getResourceAsObject(String dataset_id, String resource_name, Map<String, String> params, String auth,
			String url);

	String createResource(String auth, String data);

	String deleteResource(String auth, String dataset_id, String resource_name) throws IOException;

	String deleteResource(String auth, String dataset_id, String resource_name, String field, String row_id)
			throws IOException;

	String updateResource(String auth, String dataset_id, String resource_name, String data)
			throws JsonParseException, JsonMappingException, IOException;

}