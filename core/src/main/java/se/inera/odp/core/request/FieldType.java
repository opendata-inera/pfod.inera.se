package se.inera.odp.core.request;

public class FieldType {

	private String type;
	private String id;
	
	
	public String getType() {
		return type;
	}
	public FieldType setType(String type) {
		this.type = type;
		return this;
	}
	public String getId() {
		return id;
	}
	public FieldType setId(String id) {
		this.id = id;
		return this;
	}
	
	
}
