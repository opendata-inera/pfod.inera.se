package se.inera.odp.core.utils;

import static net.logstash.logback.argument.StructuredArguments.keyValue;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import se.inera.odp.core.response.ResponseMessage;

@Component
public class ResponseLoggerMapper {

	Logger logger = LoggerFactory.getLogger(ResponseLoggerMapper.class);

    public Map<String, Object> errorMessage(HttpStatus status, String msg, String url ) {
    	return errorMessage(status, msg, url, null);
    }
    
    public Map<String, Object> errorMessage(HttpStatus status, String msg, String uri, String errCode ) {
    	LocalDateTime localDate = LocalDateTime.now();

        Map<String, Object> response = new HashMap<>();
        response.put("timestamp", localDate.toString());
        response.put("status", status.value());
        response.put("error", status.getReasonPhrase());
        if(errCode != null)
        	response.put("error_code", errCode);
        response.put("message", msg);
        response.put("endpoint", uri);

		logger.info("exception", keyValue("exception", response));

    	return response;
    }

    public ResponseMessage responseMessage(HttpStatus status, String msg ) {
        return responseMessage(status, msg, -1);
    }

    public ResponseMessage responseMessage(HttpStatus status, String msg, int resultSize ) {
    	return responseMessage(status, msg, 
    			ServletUriComponentsBuilder.fromCurrentRequestUri()
    			.host(null)
    			.scheme(null)
    			.toUriString(),
            resultSize);
    }

    public ResponseMessage responseMessage(HttpStatus status, String msg, String uri) {
        return responseMessage(status, msg, uri, -1);
    }

    public ResponseMessage responseMessage(HttpStatus status, String msg, String uri, int resultSize ) {
    	ResponseMessage response = new ResponseMessage(status.value(), msg, uri, resultSize);

		logger.info("response", keyValue("response", response));

    	return response;
    }

    public String getUri(WebRequest request) {
    	return ((ServletWebRequest)request).getRequest().getRequestURI();
    }
}
