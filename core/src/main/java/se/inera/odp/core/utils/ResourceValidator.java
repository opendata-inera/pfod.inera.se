package se.inera.odp.core.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.http.HttpStatus;

import se.inera.odp.core.exception.ODPException;
import se.inera.odp.core.request.FieldType;
import se.inera.odp.core.request.ODPRequest;

public class ResourceValidator {
	
	private String timeStampFormat = "yyyy-MM-dd'T'HH:mm:ss";
	private String dateFormat = "yyyy-MM-dd";
	
	public boolean validateResource(ODPRequest<Object> req, Map data) {
		List<Object> list = new ArrayList<>();
		list.add(data);
		req.setRecords(list);
		return validator(req);
	}

	public boolean validateResource(ODPRequest<Object> req) {
		boolean result = validator(req);
		if(!result)
			throw new ODPException(HttpStatus.BAD_REQUEST, "Felaktig data angiven");
		return result;
	}
	
	public boolean validator(ODPRequest<Object> req) {
		HashMap<String, String> resourceFieldsAndTypes = new HashMap();
		Set<FieldType> list = req.getFields();
		for (FieldType me : list) {			
			resourceFieldsAndTypes.put(me.getId(), me.getType());
		}
		
		Set<String> setOfKeys = new HashSet<>();
		String primaryKey = req.getPrimaryKey();
		
		for (Object obj : req.getRecords())	{
			LinkedHashMap<String, Object> input = (LinkedHashMap<String, Object>) obj;			
			for (Map.Entry<String, Object> me : input.entrySet()) {
				String fieldName = me.getKey();
				if (!setOfKeys.contains(fieldName))
					setOfKeys.add(fieldName);
				else 
					return false;
			}
			if (!(setOfKeys.size() == resourceFieldsAndTypes.size()))
				return false;
			for (Map.Entry<String, Object> me : input.entrySet()) {				
				String fieldName = me.getKey();
				Object fieldValue = me.getValue();
				String fieldType = resourceFieldsAndTypes.get(fieldName);
				if (fieldName.equals(primaryKey) && fieldValue == null)
					return false;
				boolean validated = true;
				if (fieldValue != null) {
					switch (fieldType) {
						case "text":
							validated = isString(fieldValue);
							break;
						case "bool":
							validated = isBoolean(fieldValue);
							break;
						case "int":
							validated = isInt(fieldValue);
							break;
						case "text[]":
							validated = isStringArray(fieldValue);
							break;
						case "timestamp":
							validated = isTimeStamp(fieldValue);
							break;
						case "date":
							validated = isDate(fieldValue);
							break;
						case "json":
							validated = isJsonString(fieldValue);
							break;
					}
				}
				if (!validated)
					return false;
			}
			if (input.get(primaryKey) == null) 
				return false;
			setOfKeys.clear();
		}
		return true;		
	}
	
	
	private boolean isTimeStamp(Object obj) { 
		String inputString = (String) obj;
	    SimpleDateFormat format = new java.text.SimpleDateFormat(timeStampFormat);
	    try{
	       format.parse(inputString);
	       return true;
	    }
	    catch(ParseException e)
	    {
	        return false;
	    }
	}
	
	private boolean isBoolean(Object obj) {
		if (obj instanceof Boolean)
			return true;
		return false;
	}
	
	private boolean isDate(Object obj) {
		String inputString = (String) obj;
	    SimpleDateFormat format = new java.text.SimpleDateFormat(dateFormat);
	    try{
	       format.parse(inputString);
	       return true;
	    }
	    catch(ParseException e)
	    {
	        return false;
	    }
	}
	
	private boolean isJsonString(Object obj) {
		   if (obj instanceof Map) 
			   return true;
		   else if (obj instanceof List) {
			   List<Map<String, Object>> listOfMaps = (List<Map<String, Object>>)obj;
			   for (Map<String, Object> me : listOfMaps)
			   {
				   if (!(me instanceof Map))
					   return false;
			   }
			   return true;
		   }
		   return false;
	}
	
	private boolean isStringArray(Object obj) {
		if (obj instanceof ArrayList) 
		{
			ArrayList<String> al = (ArrayList<String>)obj;
			for (String str : al)
			{
				if (!(str instanceof String)) {
					if (str != null)
						return false;
				}
			}
			return true;
		}
		return false;
	}
	
	private boolean isInt(Object obj) {
		if (obj instanceof Integer)
			return true;
		return false;
	}
	
	private boolean isString(Object obj) {
		if (obj instanceof String)
			return true;
		return false;
	}

}
