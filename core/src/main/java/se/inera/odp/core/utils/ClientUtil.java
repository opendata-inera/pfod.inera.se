package se.inera.odp.core.utils;

import java.net.URI;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import static org.springframework.http.HttpHeaders.*;

public class ClientUtil {
	
	public static MultiValueMap<String, String> createHeaders(String auth) {
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
		if(auth != null && auth.length() > 0)
			headers.add(AUTHORIZATION, auth);
		headers.add("Content-Type", "application/json");
		headers.add("Accept", "application/json");
		return headers;
	}

	public static HttpHeaders createHeaders(String auth, String key, String value) {
		HttpHeaders headers = new HttpHeaders();
		if(auth != null && auth.length() > 0)
			headers.add(AUTHORIZATION, auth);
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Accept", "application/json");
		if(key != null)
			headers.add(key, value);
		return headers;
	}

	public static URI createUriWithParams(String url, Map<String, String> params) {

		UriComponentsBuilder _uri = UriComponentsBuilder.fromUriString(url);
		if(params != null) {
			for(String k : params.keySet())	{
				_uri.queryParam(k, params.get(k));
			}
		}
		URI uri = _uri.build().toUri();
		return uri;
	}
}
