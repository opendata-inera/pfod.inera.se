package se.inera.odp.core.utils;

import se.inera.odp.core.exception.ODPAuthorizationException;

public class KeyManager {
	
	private String readKey;
	private String writeKey;
	private String datasetKey;
	
	// public KeyManager() {}
	
	public KeyManager(String readKey, String writeKey, String datasetKey) {
		super();
		this.readKey = readKey;
		this.writeKey = writeKey;
		this.datasetKey = datasetKey;
	}

	public String getDatasetKeyForRead(String key) {
		if(canRead(key))
			return datasetKey;
		else
			throw new ODPAuthorizationException();
	}
	
	public String getDatasetKeyForWrite(String key) {
		if(canWrite(key))
			return datasetKey;
		else
			throw new ODPAuthorizationException();
	}
	
	public boolean canRead(String key) {
		if(key == null || key.length() == 0)
			return true; // We assume resource i public
		else if(key.equals(readKey) || key.contentEquals(writeKey))
			return true;
		else
			return false;
	}
	
	public boolean canWrite(String key) {
		if(key == null || key.length() == 0)
			return false;
		else if(key.contentEquals(writeKey))
			return true;
		else
			return false;
	}
	
	/* ********************************************************
	 * For db-auth controller
	 * *********************************************************/
	
	public boolean checkIfDbAuthIsDatasetKey(String dbAuth) {

		return dbAuth != null && dbAuth.equals(datasetKey);
	}

	public void validateDatasetKey(String dbAuth) {

		if( dbAuth == null || !dbAuth.equals(datasetKey))
			throw new ODPAuthorizationException();
	}

}
