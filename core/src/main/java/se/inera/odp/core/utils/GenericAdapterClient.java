package se.inera.odp.core.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import se.inera.odp.core.exception.ODPException;
import se.inera.odp.core.response.ODPResponse;

@Service
public class GenericAdapterClient {

    private static final String X_ODP_ADAPTER_GET_URL = "x-odp-adapter-get-url";

    Logger logger = LoggerFactory.getLogger(GenericAdapterClient.class);

    // URL for creating a new datastore in CKAN
    @Value("${app.server.datastore.url:not set}")
    private String SERVER_DATASTORE_URL;

    @Value("${app.dataset:not set}")
    private String DATASET;

    @Autowired
    WebClient webClient;

    @Autowired
    ObjectMapper mapper;

    // Send data to ckan
    public <T> void createResource(String auth, T data, Class clazz) {
        this.webClient.post()
            .uri(SERVER_DATASTORE_URL + "/save")
            .header("authorization", auth)
            .body(BodyInserters.fromObject(data))
            .exchange()
            .flatMap(clientResponse -> clientResponse.toEntity(String.class))
            .doOnError(e -> logger.error(e.getMessage()))
            .doOnSuccess(GenericAdapterClient::catchErrors)
            .block();
    }

    public String deleteResource(String auth, String resource_name, String field, String row_id) {

        // @DeleteMapping("/delete/{dataset_id}/{resource_id}/{row_id}")
        String url = SERVER_DATASTORE_URL + "/delete/" + DATASET + "/" + resource_name + "/" + field + "/" + row_id;
        return this.webClient.delete()
            .uri(url)
            .header("authorization", auth)
            .exchange()
            .flatMap(clientResponse -> clientResponse.toEntity(String.class))
            .doOnError(e -> logger.error(e.getMessage()))
            .doOnSuccess(GenericAdapterClient::catchErrors)
            .block()
            .getBody();
    }

    public <T> String updateResource(String auth, String resource_name, T data) {

        String url = SERVER_DATASTORE_URL + "/update/" + DATASET + "/" + resource_name;
        return this.webClient.put()
            .uri(url)
            .header("authorization", auth)
            .body(BodyInserters.fromObject(data))
            .exchange()
            .flatMap(clientResponse -> clientResponse.toEntity(String.class))
            .doOnError(e -> logger.error(e.getMessage()))
            .doOnSuccess(GenericAdapterClient::catchErrors)
            .block()
            .getBody();
    }

    public <T> T getData(String auth, String id, Map<String, String> params, Class<T> clazz) {
        if (id == null) {
            return null;
        }

        String type;
        if (List.class.isAssignableFrom(clazz)) {
            type = "/getlist/";
        } else if (ODPResponse.class.isAssignableFrom(clazz)) {
            type = "/getobject/";
        } else {
            type = "/get/";
        }

        String url = SERVER_DATASTORE_URL + type + DATASET + "/" + id;

        return this.webClient.get()
            .uri(ClientUtil.createUriWithParams(url, params))
            .header("authorization", auth)
            .header(X_ODP_ADAPTER_GET_URL, ServletUriComponentsBuilder.fromCurrentRequestUri().toUriString())
            .exchange()
            .flatMap(clientResponse -> clientResponse.toEntity(clazz))
            .doOnError(e -> logger.error(e.getMessage()))
            .doOnSuccess(GenericAdapterClient::catchErrors)
            .block()
            .getBody();
    }

    private static <T> void catchErrors(ResponseEntity<T> clientResponse) {
        HttpStatus httpStatus = clientResponse.getStatusCode();
        if (!httpStatus.is2xxSuccessful()) {
            throw new ODPException(httpStatus, "Error encountered");
        }
    }
}
