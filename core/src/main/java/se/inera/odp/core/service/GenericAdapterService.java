package se.inera.odp.core.service;

import java.io.IOException;
import java.io.InputStream;

import com.fasterxml.jackson.databind.ObjectMapper;

import se.inera.odp.core.exception.ODPException;

public abstract class GenericAdapterService {

	ObjectMapper mapper;
	
	public GenericAdapterService(ObjectMapper mapper) {
		super();
		this.mapper = mapper;
	}

	public <T> T readValue(InputStream src, Class<T> valueType) {
		try {
			return mapper.readValue(src, valueType);
		} catch (IOException e) {
			throw new ODPException(e.getClass().getName() + ":" + e.getMessage());
		}		
	}
	
	public <T> T readValue(String src, Class<T> valueType) {
		try {
			return mapper.readValue(src, valueType);
		} catch (IOException e) {
			throw new ODPException(e.getClass().getName() + ":" + e.getMessage());
		}		
	}	
}
