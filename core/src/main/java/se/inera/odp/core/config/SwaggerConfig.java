package se.inera.odp.core.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnResource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

import se.inera.odp.core.utils.YamlPropertySourceFactory;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@Profile("!production")
@PropertySource(value={"classpath:swagger.yml"}, factory = YamlPropertySourceFactory.class)
@ConditionalOnResource(resources = {"classpath:swagger.yml"})
public class SwaggerConfig {
	
	@Value("${app.swagger.email:}")
	private String email;

	@Value("${app.swagger.title:}")
	private String title;

	@Value("${app.swagger.description:Rest Api för dataset}")
	private String description;

	@Value("${app.swagger.intro:}")
	private String introtext;
	
	@Value("${app.swagger.basepackage:any}")
	private String basepackage;
	  
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(basepackage.equals("any")
                		? RequestHandlerSelectors.any()
                		: RequestHandlerSelectors.basePackage(basepackage))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiEndPointsInfo());
    }
    
    private ApiInfo apiEndPointsInfo() {
        return new ApiInfoBuilder().title(title)
            .description("## " + description + "\n\n" + introtext)
            .contact(new Contact("Inera AB", "www.inera.se", email))
            //.license("Apache 2.0")
            //.licenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html")
            .version("1.0.0")
            .build();
    } 
    
}