package se.inera.odp.hsa.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.Assert;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import se.inera.odp.core.exception.ODPException;
import se.inera.odp.hsa.resource.HsaObject;
import se.inera.odp.hsa.service.HsaXmlToJsonMapper;

@RunWith(SpringRunner.class)
public class ResourceTest {
	
	ObjectMapper mapper = new ObjectMapper();

	@Test
	public void shouldMapSingleHsaObjectInJson() {
		
		try {
			InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("hsa-v1-single-post-json-example.json");
			HsaObject req = mapper.readValue(is, HsaObject.class);
			Assert.assertTrue("75320".contentEquals(req.getPostalCode()));
		} catch (IOException e) {
			throw new ODPException(e.getClass().getName() + ":" + e.getMessage());
		}
	}
	
	@Test
	public void shouldMapSingleHsaObjectInXml() {
		
		try {
			InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("hsa-v1-single-post-xml-example.xml");
			XmlMapper x = new XmlMapper();
			
			se.inera.odp.hsa.resource.xml.HsaObject xmlObj = x.readValue(is, se.inera.odp.hsa.resource.xml.HsaObject.class);

			HsaXmlToJsonMapper map = new HsaXmlToJsonMapper();
			HsaObject h = map.mapHsaObject(xmlObj);
						
			System.out.println(mapper.writeValueAsString(h));
			
			Assert.assertTrue("17:00".contentEquals(h.getSurgeryHours().get(0).getToTime2()));
		} catch (IOException e) {
			throw new ODPException(e.getClass().getName() + ":" + e.getMessage());
		}
	}

	@Test
	public void shouldMapHsaFile() {
		
		try {
			InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("psiPublicUnits.xml");
			
			HsaXmlToJsonMapper map = new HsaXmlToJsonMapper();
			List<HsaObject> h = map.map(is);
						
			// System.out.println(mapper.writeValueAsString(h));

			Assert.assertTrue(h.size() == 428);
		} catch (IOException e) {
			throw new ODPException(e.getClass().getName() + ":" + e.getMessage());
		}
	}

}
