package se.inera.odp.hsa.client;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import se.inera.odp.hsa.service.HsaAdapterService;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@TestPropertySource("classpath:application-test.properties")
public class ClientTest {

    @Autowired
    private HsaAdapterClient hsaAdapterClient;

    @Autowired
    private HsaAdapterService hsaAdapterService;

	@Configuration
	@ComponentScan({"se.inera.odp.hsa.*", "se.inera.odp.hsa", "se.inera.odp.core.*"})
	public static class SpringConfig {		  
	}

	// TODO: Add mocked tests.


	@Test
	@Ignore
	/**
	 * Test to download file from hsa. Remove @Ignore to try.
	 */
	public void clientDownloadTest() {
		hsaAdapterClient.download().blockFirst();
	}

	@Test
	@Ignore
	/**
	 * Test to download file from hsa and send to ckan. Remove @Ignore to try.
	 */
	public void hsaAdapterDownloadTest() {
		hsaAdapterService.schedule();
	}

}
