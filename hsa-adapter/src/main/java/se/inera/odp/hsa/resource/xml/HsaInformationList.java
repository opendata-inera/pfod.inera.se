package se.inera.odp.hsa.resource.xml;

import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class HsaInformationList {

	@JacksonXmlProperty(localName="StartDate")	
	private String startDate;
	@JacksonXmlProperty(localName="HsaObjects")	
	private List<HsaObject> hsaObjects;
	@JacksonXmlProperty(localName="EndDate")	
	private String endDate;
	@JacksonXmlProperty(localName="TotalHsaObjects")	
	private Integer totalHsaObjects;
	
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public List<HsaObject> getHsaObjects() {
		return hsaObjects;
	}
	public void setHsaObjects(List<HsaObject> hsaObjects) {
		this.hsaObjects = hsaObjects;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public Integer getTotalHsaObjects() {
		return totalHsaObjects;
	}
	public void setTotalHsaObjects(Integer totalHsaObjects) {
		this.totalHsaObjects = totalHsaObjects;
	}
	
	
}
