package se.inera.odp.hsa.client;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.function.Consumer;
import javax.net.ssl.SSLContext;
import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ClientHttpConnector;
import org.springframework.http.client.reactive.JettyClientHttpConnector;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RequestCallback;
import org.springframework.web.client.ResponseExtractor;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import se.inera.odp.core.exception.ODPException;
import se.inera.odp.core.utils.ssl.SSLContextCreator;

@Service
public class HsaAdapterClient {

	@Value("${app.hsafileservice.url}")
    private String HSA_FILE_URL;

	@Value("${app.certificates.path}")
    private String PEM_FILE;

    SSLContext sslContext;

    @Autowired
    private WebClient webClient;

    public String downloadFile() {
        // Optional Accept header
        RequestCallback requestCallback = request -> request.getHeaders()
            .setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM, MediaType.ALL));

        // Streams the response instead of loading it all in memory
        ResponseExtractor<Void> responseExtractor = response -> {
            // Here I write the response to a file but do what you like
            Path path = Paths.get("target/slask.txt");
            Files.delete(path);
            Files.copy(response.getBody(), path);
            return null;
        };

        Consumer<? super byte[]> fun = data -> {
            Path path = Paths.get("target/slask.txt");
            try {
                Files.delete(path);
                Files.write(path, data);
            } catch (IOException e) {
                e.printStackTrace();
            }

        };
        getWebClient().get().uri(HSA_FILE_URL).accept(MediaType.APPLICATION_OCTET_STREAM, MediaType.ALL).retrieve().bodyToFlux(byte[].class)
            .subscribe(fun);

        return "target/slask.txt";
    }


    public Flux<byte[]> download() {
        return getWebClient().get()
            .uri(HSA_FILE_URL)
            .header(HttpHeaders.ACCEPT_ENCODING, "gzip")
            .retrieve()
            .bodyToFlux(byte[].class);
    }

    private WebClient getWebClient() {
        WebClient webclient = null;
        try {
            sslContext = SSLContextCreator.createSSLContext(getFilePath(PEM_FILE), true);
        } catch (Exception e) {
            throw new ODPException(HttpStatus.INTERNAL_SERVER_ERROR, "SSL Context can not be created: " + e.getMessage());
        }

        SslContextFactory sslContextFactory = new SslContextFactory();
        sslContextFactory.setSslContext(sslContext);
        HttpClient httpClient = new HttpClient(sslContextFactory);
        ClientHttpConnector clientConnector = new JettyClientHttpConnector(httpClient);

        webclient = WebClient.builder().clientConnector(clientConnector).build();
        return webclient;
    }

    private String getFilePath(String filename){
        if(filename.startsWith("classpath:")) {
            String filePath = HsaAdapterClient.class.getClassLoader()
                .getResource(filename.substring(10)).getPath();
            if(filePath.startsWith("/")) {
                filePath = filePath.substring(1);
            }
            return filePath;
        } else
            return filename;
    }
}
