package se.inera.odp.hsa.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import se.inera.odp.core.exception.ODPException;
import se.inera.odp.core.request.ODPRequest;
import se.inera.odp.core.service.GenericAdapterService;
import se.inera.odp.core.utils.GenericAdapterClient;
import se.inera.odp.hsa.client.HsaAdapterClient;
import se.inera.odp.core.response.ODPResponse;
import se.inera.odp.hsa.resource.HsaObject;

import static se.inera.odp.core.exception.ErrorCodes.*;


@Service
public class HsaAdapterService extends GenericAdapterService {

	Logger logger = LoggerFactory.getLogger(HsaAdapterService.class);

	@Autowired
	GenericAdapterClient adapterClient;

	@Autowired
	HsaAdapterClient hsaClient;

	@Value("${app.dataset.key}")
	private String authentication_key;
	
	@Value("${app.hsa.schedule.enabled:false}")
	private boolean enabled;
	
	private static final String HSA_CODES_DEFINITION = "hsa/definition_hsa_v1_hsaObjects.json";

	@Autowired
	public HsaAdapterService(ObjectMapper mapper) {
		super(mapper);
	}

	@Scheduled(cron = "${app.hsa.schedule.cron}" )
	public void schedule() {
		if(!enabled) return;
		download(authentication_key);
	}
	
	public int execDownload(String auth) {
		
		return download(auth);
	}
	
	private int download(String auth) {
		int size = -1;
				
		logger.info("Starting download of hsa file ...");

		PipedOutputStream os = new PipedOutputStream();
		PipedInputStream in = null;
		try {
			in = new PipedInputStream(os);
		} catch (IOException e) {
			throw new ODPException(HttpStatus.INTERNAL_SERVER_ERROR, e.getClass().getName() + ":" + e.getMessage());
		}

		hsaClient.download()
			.subscribe( data -> {
			try {
				os.write(data);
			} catch (IOException e) {
				throw new ODPException(HttpStatus.INTERNAL_SERVER_ERROR, e.getClass().getName() + ":" + e.getMessage());
			}
		});

		try(ZipInputStream zin = new ZipInputStream(in)) {
			
			zin.getNextEntry();
			logger.info("Download of hsa file done. Start saving to datastore ...");

			HsaXmlToJsonMapper map = new HsaXmlToJsonMapper();
			List<HsaObject> records = map.map(zin);
			size = records == null ? -1 : records.size();

			InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(HSA_CODES_DEFINITION);
			@SuppressWarnings("unchecked")
			ODPRequest<HsaObject> req = readValue(is, ODPRequest.class);
			req.setRecords(records);
			adapterClient.createResource(authentication_key, req, ODPRequest.class);
			
		} catch (IOException e) {
			throw new ODPException(HttpStatus.INTERNAL_SERVER_ERROR, e.getClass().getName() + ":" + e.getMessage());
		}
		logger.info("Hsa file is downloaded and saved to CKAN");
		return size;
	}

	// hsa-v1-hsaObjects
	private String createResourceName(String version, String resource_id) {
		String resource_name = "hsa-" + version + "-" + resource_id;
		return resource_name;
	}
	
	public ODPResponse getResourceAsObject(String version, String resource_id, String id, Map<String, String> params, String auth) {
		return adapterClient.getData(auth, createResourceName(version, resource_id), params, ODPResponse.class);
	}
	
	@SuppressWarnings("unchecked")
	public ODPResponse getParentResourceAsObject(String version, String resource_id, String id, Map<String, String> params, String auth) {
		try {
			Map<String, String> params2 = new HashMap<>();
			params2.put("hsaIdentity", id);
			String data = adapterClient.getData(auth, createResourceName(version, resource_id), params2, String.class);
			List<Map<String, Object>> map = null;
			map = readValue(data, List.class);
			String dn = (String) map.get(0).get("parentDn");
			params.put("dn", URLEncoder.encode(URLDecoder.decode(dn, "UTF-8"), "UTF-8"));
			return adapterClient.getData(auth, createResourceName(version, resource_id), params, ODPResponse.class);
		} catch (IOException e) {
			throw new ODPException(HttpStatus.INTERNAL_SERVER_ERROR, e.getClass().getName() + ":" + e.getMessage(), ERROR_CODE_URL_PARSING);
		} 
	}
	
	@SuppressWarnings("unchecked")
	public ODPResponse getChildResourceAsObject(String version, String resource_id, String id, Map<String, String> params, String auth) {
		try {
			Map<String, String> params2 = new HashMap<>();
			params2.put("hsaIdentity", id);
			String data = adapterClient.getData(auth, createResourceName(version, resource_id), params2, String.class);
			List<Map<String, Object>> map = null;
			map = readValue(data, List.class);
			String dn = (String) map.get(0).get("dn");
			params.put("parentDn", URLEncoder.encode(URLDecoder.decode(dn, "UTF-8"), "UTF-8"));
			return adapterClient.getData(auth, createResourceName(version, resource_id), params, ODPResponse.class);
		} catch (IOException e) {
			throw new ODPException(HttpStatus.INTERNAL_SERVER_ERROR, e.getClass().getName() + ":" + e.getMessage(), ERROR_CODE_URL_PARSING);
		}
	}

	public List<?> getResourceAsList(String version, String resource_id, String id, Map<String, String> params,	String auth) {
		return adapterClient.getData(auth, createResourceName(version, resource_id), params, List.class);
	}
	
	public List<?> getParentResourceAsList(String version, String resource_id, String id, Map<String, String> params, String auth) {
		try {
			Map<String, String> params2 = new HashMap<>();
			params2.put("hsaIdentity", id);
			String data = adapterClient.getData(auth, createResourceName(version, resource_id), params2, String.class);
			List<Map<String, Object>> map = null;
			map = readValue(data, List.class);
			String dn = (String) map.get(0).get("parentDn");
			params.put("dn", URLEncoder.encode(URLDecoder.decode(dn, "UTF-8"), "UTF-8"));
			return adapterClient.getData(auth, createResourceName(version, resource_id), params, List.class);
		} catch (IOException e) {
			throw new ODPException(HttpStatus.INTERNAL_SERVER_ERROR, e.getClass().getName() + ":" + e.getMessage(), ERROR_CODE_URL_PARSING);
		} 
	}

	public List<?> getChildResourceAsList(String version, String resource_id, String id, Map<String, String> params, String auth) {
		try {
			Map<String, String> params2 = new HashMap<>();
			params2.put("hsaIdentity", id);
			String data = adapterClient.getData(auth, createResourceName(version, resource_id), params2, String.class);
			List<Map<String, Object>> map = null;
			map = readValue(data, List.class);
			String dn = (String) map.get(0).get("dn");
			params.put("parentDn", URLEncoder.encode(URLDecoder.decode(dn, "UTF-8"), "UTF-8"));
			return adapterClient.getData(auth, createResourceName(version, resource_id), params, List.class);
		} catch (IOException e) {
			throw new ODPException(HttpStatus.INTERNAL_SERVER_ERROR, e.getClass().getName() + ":" + e.getMessage(), ERROR_CODE_URL_PARSING);
		}
	}




}
