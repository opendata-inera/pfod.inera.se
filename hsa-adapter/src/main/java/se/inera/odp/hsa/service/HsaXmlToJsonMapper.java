package se.inera.odp.hsa.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import se.inera.odp.hsa.resource.HsaObject;
import se.inera.odp.hsa.resource.xml.Attribute;
import se.inera.odp.hsa.resource.xml.HsaInformationList;

public class HsaXmlToJsonMapper {
	
	public List<HsaObject> map(InputStream is) throws IOException {
		XmlMapper x = new XmlMapper();
		
		HsaInformationList hsaInformationList = x.readValue(is, HsaInformationList.class);
		
		List<HsaObject> hsaObjects = new ArrayList<>();
		hsaInformationList.getHsaObjects().forEach(h -> {
			hsaObjects.add(mapHsaObject(h));
		});
		return hsaObjects;
	}
	
	public HsaObject mapHsaObject(se.inera.odp.hsa.resource.xml.HsaObject xmlObj) {
		HsaObject h = new HsaObject();
		h.setDn(xmlObj.getDn());
		
		// Create parentDn. Remove first element
		int first = xmlObj.getDn().indexOf(',') + 1;
		String parentDn = first > 0 ? xmlObj.getDn().substring(first).trim() : null;
		h.setParentDn(parentDn);
		
		for (Attribute k : xmlObj.getAttributes()) {
			switch (k.getName()) {
			case "hsaIdentity":
				h.setHsaIdentity(k.getS());
				break;
			case "cn":
				h.setCn(k.getS());
				break;
			case "c":
				h.setC(k.getS());
				break;
			case "hsaVpwWebPage":
				h.setHsaVpwWebPage(k.getS());
				break;
			case "hsaAltText":
				h.setHsaAltText(k.getS());
				break;
			case "ouShort":
				h.setOuShort(k.getS());
				break;
			case "description":
				h.setDescription(k.getS());
				break;
			case "street":
				h.setStreet(k.getS());
				break;
			case "hsaVisitingRules":
				h.setHsaVisitingRules(k.getS());
				break;
			case "visitingHours":
				h.addVisitingHours(k.getTimeSpan());
				break;
			case "jpegPhoto":
				h.setJpegPhoto(k.getS());
				break;
			case "telephoneNumber":
				h.addTelephoneNumber(k.getS());
				break;
			case "hsaBusinessType":
				h.setHsaBusinessType(k.getS()); // Borde vara List<String>
				break;
			case "facsimileTelephoneNumber":
				h.addFacsimileTelephoneNumber(k.getS());
				break;
			case "route":
				h.setRoute(k.getS());
				break;
			case "l":
				h.setL(k.getS());
				break;
			case "geographicalCoordinates":
				h.addGeographicalCoordinates(k.getCoordinate());
				break;
			case "geographicalCoordinatesSweref99TM":
				h.setGeographicalCoordinatesSweref99TM(k.getS());
				break;
			case "hsaDirectoryContact":
				h.setHsaDirectoryContact(k.getS());
				break;
			case "municipalityCode":
				h.setMunicipalityCode(k.getS());
				break;
			case "municipalityName":
				h.setMunicipalityName(k.getS());
				break;
			case "hsaJpegLogotype":
				h.setHsaJpegLogotype(k.getS());
				break;
			case "countyCode":
				h.setCountyCode(k.getS());
				break;
			case "countyName":
				h.setCountyName(k.getS());
				break;
			case "hsaHealthCareArea":
				h.setHsaHealthCareArea(k.getS());
				break;
			case "orgNo":
				h.setOrgNo(k.getS());
				break;
			case "postalAddress":
				h.setPostalAddress(k.getAddress().getAddressLine());
				break;
			case "postalCode":
				h.setPostalCode(k.getS());
				break;
			case "hsaVisitingRuleReferral":
				h.setHsaVisitingRuleReferral(k.getS());
				break;
			case "modifyTimestamp":
				h.setModifyTimestamp(k.getS());
				break;
			case "createTimestamp":
				h.setCreateTimestamp(k.getS());
				break;
			case "endDate":
				h.setEndDate(k.getDate());
				break;
			case "startDate":
				h.setStartDate(k.getDate());
				break;
			case "telephoneHours":
				h.addTelephoneHours(k.getTimeSpan());
				break;
			case "hsaTextTelephoneNumber":
				h.addHsaTextTelephoneNumber(k.getS());
				break;
			case "dropInHours":
				h.addDropInHours(k.getTimeSpan());
				break;
			case "businessClassificationType":
				h.addBusinessClassificationType(k.getBusinessClassificationType());
				break;
			case "careType":
				h.addCareType(k.getS());
				break;
			case "hsaSwitchboardNumber":
				h.setHsaSwitchboardNumber(k.getS());
				break;
			case "labeledURI":
				h.setLabeledURI(k.getS());
				break;
			case "hsaVisitingRuleAge":
				h.addHsaVisitingRuleAge(k.getAgeSpan());
				break;
			case "management":
				h.setManagement(k.getS());
				break;
			case "surgeryHours":
				h.addSurgeryHours(k.getTimeSpan());
				break;
			case "mobile":
				h.addMobile(k.getS());
				break;
			case "publicName":
				h.setPublicName(k.getS());
				break;
			}
		}
		return h;
	}
}
