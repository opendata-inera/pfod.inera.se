package se.inera.odp.hsa.resource.xml;

import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

public class Address {

	@JacksonXmlElementWrapper(useWrapping = false)
	private List<String> addressLine;

	public List<String> getAddressLine() {
		if(addressLine == null || addressLine.size() == 0)
			return null;
		else
			return addressLine;
	}

	public void setAddressLine(List<String> addressLine) {
		this.addressLine = addressLine;
	}
	
	
}
