package se.inera.odp.hsa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages= {"se.inera.odp.hsa", "se.inera.odp.hsa.*", "se.inera.odp.core.*"})
public class ODPHsaApplication {

 
    public static void main(String[] args) {
        SpringApplication.run(ODPHsaApplication.class, args);
    }
    
}
