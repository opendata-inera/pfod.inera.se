package se.inera.odp.hsa.resource;

public class BusinessClassificationType {

	private String businessClassificationName;
	private String businessClassificationCode;
	
	public String getBusinessClassificationName() {
		return businessClassificationName;
	}
	public void setBusinessClassificationName(String businessClassificationName) {
		this.businessClassificationName = businessClassificationName;
	}
	public String getBusinessClassificationCode() {
		return businessClassificationCode;
	}
	public void setBusinessClassificationCode(String businessClassificationCode) {
		this.businessClassificationCode = businessClassificationCode;
	}
	
	
}
