package se.inera.odp.hsa.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import se.inera.odp.core.response.ODPResponse;
import se.inera.odp.core.response.ResponseMessage;
import se.inera.odp.core.utils.KeyManager;
import se.inera.odp.core.utils.ResponseLoggerMapper;
import se.inera.odp.hsa.service.HsaAdapterService;
import springfox.documentation.annotations.ApiIgnore;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@RestController
@RequestMapping({""})
@Api(value = "hsa", tags = {"HSA Public Units"}, produces = "application/json", consumes = "application/json")
public class HsaAdapterController {

    @Autowired
    HsaAdapterService hsaAdapterService;

    @Autowired
    private ResponseLoggerMapper responseMapper;

    @Autowired
    private KeyManager keyManager;

    /*
     *         /hsa/v1/hsaObjects
        Get all HsaObjects.

        GET
        /hsa/v1/hsaObjects/{ID}
        Get a HsaObject based on its hsaIdentity

        GET
        /hsa/v1/hsaObjects/{ID}/children
        Get the children HsaObjects of the HsaObject with the specified hsaIdentity.

        GET
        /hsa/v1/hsaObjects/{ID}/parent
        Get the parent hsaObject of the HsaObject with the specified hsaIdentity

     */
    @ApiOperation(value = "Returnerar ett objekt med hsaobjects av typen 'resource_id'", response = ResponseMessage.class)
    @GetMapping(value = {"/v1/hsaObjects", "/v1/hsaObjects/{id}"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getResourceAsObject(
        @RequestHeader(value = AUTHORIZATION, required = false) String auth,
        @PathVariable(required = false) String id,
        @RequestParam(required = false) Integer limit,
        @RequestParam(required = false) Integer offset,
        @RequestParam(required = false) String fields,
        @RequestParam(required = false) String sort,
        @RequestParam(required = false) Map<String, String> params) {

		if (id != null) {
			params.put("hsaIdentity", id);
		}
        String version = "v1";
        String resource_id = "hsaObjects";
        ODPResponse result = hsaAdapterService.getResourceAsObject(version, resource_id, id, params, keyManager.getDatasetKeyForRead(auth));
        responseMapper.responseMessage(HttpStatus.OK, "OK", result.getTotal());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @ApiOperation(value = "Returnerar en lista med hsaobjects av typen 'resource_id'", response = ResponseMessage.class)
    @GetMapping(value = {"/v1/hsaObjects/list", "/v1/hsaObjects/{id}/list"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List> getResourceAsList(
        @RequestHeader(value = AUTHORIZATION, required = false) String auth,
        @PathVariable(required = false) String id,
        @RequestParam(required = false) Integer limit,
        @RequestParam(required = false) Integer offset,
        @RequestParam(required = false) String fields,
        @RequestParam(required = false) String sort,
        @RequestParam(required = false) Map<String, String> params) {

		if (id != null) {
			params.put("hsaIdentity", id);
		}
        String version = "v1";
        String resource_id = "hsaObjects";
        List<?> result = hsaAdapterService.getResourceAsList(version, resource_id, id, params, keyManager.getDatasetKeyForRead(auth));
        responseMapper.responseMessage(HttpStatus.OK, "OK", result.size());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @ApiOperation(value = "Returnerar ett objekt med hsaobjects av typen 'resource_id'", response = ResponseMessage.class)
    @GetMapping(value = {"/v1/hsaObjects/{id}/parent"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getParentResourceAsObject(
        @RequestHeader(value = AUTHORIZATION, required = false) String auth,
        @PathVariable String id,
        @RequestParam(required = false) Integer limit,
        @RequestParam(required = false) Integer offset,
        @RequestParam(required = false) String fields,
        @RequestParam(required = false) String sort,
        @RequestParam(required = false) Map<String, String> params) {

        String version = "v1";
        String resource_id = "hsaObjects";
        ODPResponse result = hsaAdapterService
            .getParentResourceAsObject(version, resource_id, id, params, keyManager.getDatasetKeyForRead(auth));
        responseMapper.responseMessage(HttpStatus.OK, "OK", result.getTotal());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @ApiOperation(value = "Returnerar en lista med hsaobjects av typen 'resource_id'", response = ResponseMessage.class)
    @GetMapping(value = {"/v1/hsaObjects/{id}/parent/list"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List> getParentResourceAsList(
        @RequestHeader(value = AUTHORIZATION, required = false) String auth,
        @PathVariable String id,
        @RequestParam(required = false) Integer limit,
        @RequestParam(required = false) Integer offset,
        @RequestParam(required = false) String fields,
        @RequestParam(required = false) String sort,
        @RequestParam(required = false) Map<String, String> params) {

        String version = "v1";
        String resource_id = "hsaObjects";
        List<?> result = hsaAdapterService.getParentResourceAsList(version, resource_id, id, params, keyManager.getDatasetKeyForRead(auth));
        responseMapper.responseMessage(HttpStatus.OK, "OK", result.size());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @ApiOperation(value = "Returnerar ett objekt med hsaobjects av typen 'resource_id'", response = ResponseMessage.class)
    @GetMapping(value = {"/v1/hsaObjects/{id}/children"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getChildrenResourceAsObject(
        @RequestHeader(value = AUTHORIZATION, required = false) String auth,
        @PathVariable String id,
        @RequestParam(required = false) Integer limit,
        @RequestParam(required = false) Integer offset,
        @RequestParam(required = false) String fields,
        @RequestParam(required = false) String sort,
        @RequestParam(required = false) Map<String, String> params) {

        String version = "v1";
        String resource_id = "hsaObjects";
        ODPResponse result = hsaAdapterService
            .getChildResourceAsObject(version, resource_id, id, params, keyManager.getDatasetKeyForRead(auth));
        responseMapper.responseMessage(HttpStatus.OK, "OK", result.getTotal());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @ApiOperation(value = "Returnerar en lista med hsaobjects av typen 'resource_id'", response = ResponseMessage.class)
    @GetMapping(value = {"/v1/hsaObjects/{id}/children/list"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List> getChildrenResourceAsList(
        @RequestHeader(value = AUTHORIZATION, required = false) String auth,
        @PathVariable String id,
        @RequestParam(required = false) Integer limit,
        @RequestParam(required = false) Integer offset,
        @RequestParam(required = false) String fields,
        @RequestParam(required = false) String sort,
        @RequestParam(required = false) Map<String, String> params) {

        String version = "v1";
        String resource_id = "hsaObjects";
        List<?> result = hsaAdapterService.getChildResourceAsList(version, resource_id, id, params, keyManager.getDatasetKeyForRead(auth));
        responseMapper.responseMessage(HttpStatus.OK, "OK", result.size());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * Instructs the adapter to download new hsa data. This method should not be visible in swagger.
     */
    @ApiIgnore
    @PostMapping(value = {"/v1/hsaObjects"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> save(
        @RequestHeader(value = AUTHORIZATION, required = false) String auth,
        @RequestParam(required = false) Map<String, String> params) {

        auth = auth == null ? params.get(AUTHORIZATION) : auth;

        auth = keyManager.getDatasetKeyForWrite(auth);

        int size = hsaAdapterService.execDownload(auth);
        return new ResponseEntity<>(responseMapper.responseMessage(HttpStatus.CREATED, "OK", size), HttpStatus.CREATED);
    }
}