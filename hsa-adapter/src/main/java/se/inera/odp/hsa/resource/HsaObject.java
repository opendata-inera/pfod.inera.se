package se.inera.odp.hsa.resource;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HsaObject {
	private String hsaIdentity ;
	@JsonProperty(required=false)
	private String dn;
	@JsonProperty(required=false)
	private String parentDn;
	@JsonProperty(required=false)
	private String cn;
	@JsonProperty(required=false)
	private String c;
	@JsonProperty(required=false)
	private String hsaVpwWebPage;
	@JsonProperty(required=false)
	private String hsaAltText;
	@JsonProperty(required=false)
	private String ouShort;
	@JsonProperty(required=false)
	private String description;
	@JsonProperty(required=false)
	private String street;
	@JsonProperty(required=false)
	private String hsaVisitingRules;
	private List<TimeSpan> visitingHours;
	@JsonProperty(required=false)
	private String jpegPhoto;
	@JsonProperty(required=false)
	private List<String> telephoneNumber;
	@JsonProperty(required=false)
	private String hsaBusinessType; // Borde vara List<String>
	@JsonProperty(required=false)
	private List<String> facsimileTelephoneNumber;
	@JsonProperty(required=false)
	private String route;
	@JsonProperty(required=false)
	private String l;
	@JsonProperty(required=false)
	private List<Coordinate> geographicalCoordinates;
	@JsonProperty(required=false)
	private String geographicalCoordinatesSweref99TM;
	@JsonProperty(required=false)
	private String hsaDirectoryContact;
	@JsonProperty(required=false)
	private String municipalityCode;
	@JsonProperty(required=false)
	private String municipalityName;
	@JsonProperty(required=false)
	private String hsaJpegLogotype;
	@JsonProperty(required=false)
	private String countyCode;
	@JsonProperty(required=false)
	private String countyName;
	@JsonProperty(required=false)
	private String hsaHealthCareArea;
	@JsonProperty(required=false)
	private String orgNo;
	@JsonProperty(required=false)
	private List<String> postalAddress;
	@JsonProperty(required=false)
	private String postalCode;
	@JsonProperty(required=false)
	private String hsaVisitingRuleReferral;
	@JsonProperty(required=false)
	private String modifyTimestamp;
	@JsonProperty(required=false)
	private String createTimestamp;
	@JsonProperty(required=false)
	private String endDate;
	@JsonProperty(required=false)
	private String startDate;
	@JsonProperty(required=false)
	private List<TimeSpan> telephoneHours;
	@JsonProperty(required=false)
	private List<String> hsaTextTelephoneNumber;
	@JsonProperty(required=false)
	private List<TimeSpan> dropInHours;
	@JsonProperty(required=false)
	private List<BusinessClassificationType> businessClassificationType;
	@JsonProperty(required=false)
	private List<String> careType;
	@JsonProperty(required=false)
	private String hsaSwitchboardNumber;
	@JsonProperty(required=false)
	private String labeledURI;
	@JsonProperty(required=false)
	private List<AgeSpan> hsaVisitingRuleAge;
	@JsonProperty(required=false)
	private String management;
	@JsonProperty(required=false)
	private List<TimeSpan> surgeryHours;
	@JsonProperty(required=false)
	private List<String> mobile;
	@JsonProperty(required=false)
	private String publicName;
	@JsonProperty(required=false)
	private String financingOrganization;
	@JsonProperty(required=false)
	private String o;
	@JsonProperty(required=false)
	private String ou;

	
	public String getHsaIdentity() {
		return hsaIdentity;
	}
	public void setHsaIdentity(String hsaIdentity) {
		this.hsaIdentity = hsaIdentity;
	}
	public String getDn() {
		return dn;
	}
	public void setDn(String dn) {
		this.dn = dn;
	}
	public String getParentDn() {
		return parentDn;
	}
	public void setParentDn(String parentDn) {
		this.parentDn = parentDn;
	}
	public String getCn() {
		return cn;
	}
	public void setCn(String cn) {
		this.cn = cn;
	}
	public String getC() {
		return c;
	}
	public void setC(String c) {
		this.c = c;
	}
	public String getHsaVpwWebPage() {
		return hsaVpwWebPage;
	}
	public void setHsaVpwWebPage(String hsaVpwWebPage) {
		this.hsaVpwWebPage = hsaVpwWebPage;
	}
	public String getHsaAltText() {
		return hsaAltText;
	}
	public void setHsaAltText(String hsaAltText) {
		this.hsaAltText = hsaAltText;
	}
	public String getOuShort() {
		return ouShort;
	}
	public void setOuShort(String ouShort) {
		this.ouShort = ouShort;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getHsaVisitingRules() {
		return hsaVisitingRules;
	}
	public void setHsaVisitingRules(String hsaVisitingRules) {
		this.hsaVisitingRules = hsaVisitingRules;
	}
	public List<TimeSpan> getVisitingHours() {
		return visitingHours;
	}
	public void setVisitingHours(List<TimeSpan> visitingHours) {
		this.visitingHours = visitingHours;
	}
	public void addVisitingHours(TimeSpan visitingHours) {
		if(this.visitingHours == null)
			this.visitingHours = new ArrayList<>();
		this.visitingHours.add(visitingHours);
	}
	public String getJpegPhoto() {
		return jpegPhoto;
	}
	public void setJpegPhoto(String jpegPhoto) {
		this.jpegPhoto = jpegPhoto;
	}
	public List<String> getTelephoneNumber() {
		return telephoneNumber;
	}
	public void setTelephoneNumber(List<String> telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}
	public void addTelephoneNumber(String telephoneNumber) {
		if(this.telephoneNumber == null)
			this.telephoneNumber = new ArrayList<String>();
		this.telephoneNumber.add(telephoneNumber);
	}
	public String getHsaBusinessType() {                           
		return hsaBusinessType;
	}
	public void setHsaBusinessType(String hsaBusinessType) {
		this.hsaBusinessType = hsaBusinessType;
	}

	public List<String> getFacsimileTelephoneNumber() {
		return facsimileTelephoneNumber;
	}
	public void setFacsimileTelephoneNumber(List<String> facsimileTelephoneNumber) {
		this.facsimileTelephoneNumber = facsimileTelephoneNumber;
	}
	public void addFacsimileTelephoneNumber(String facsimileTelephoneNumber) {
		if(this.facsimileTelephoneNumber == null)
			this.facsimileTelephoneNumber = new ArrayList<String>();
		this.facsimileTelephoneNumber.add(facsimileTelephoneNumber);
	}
	public String getRoute() {
		return route;
	}
	public void setRoute(String route) {
		this.route = route;
	}
	public String getL() {
		return l;
	}
	public void setL(String l) {
		this.l = l;
	}
	public List<Coordinate> getGeographicalCoordinates() {
		return geographicalCoordinates;
	}
	public void setGeographicalCoordinates(List<Coordinate> geographicalCoordinates) {
		this.geographicalCoordinates = geographicalCoordinates;
	}
	public void addGeographicalCoordinates(Coordinate geographicalCoordinates) {
		if(this.geographicalCoordinates == null)
			this.geographicalCoordinates = new ArrayList<>();
		this.geographicalCoordinates.add(geographicalCoordinates);
	}
	public String getGeographicalCoordinatesSweref99TM() {
		return geographicalCoordinatesSweref99TM;
	}
	public void setGeographicalCoordinatesSweref99TM(String geographicalCoordinatesSweref99TM) {
		this.geographicalCoordinatesSweref99TM = geographicalCoordinatesSweref99TM;
	}
	public String getHsaDirectoryContact() {
		return hsaDirectoryContact;
	}
	public void setHsaDirectoryContact(String hsaDirectoryContact) {
		this.hsaDirectoryContact = hsaDirectoryContact;
	}
	public String getMunicipalityCode() {
		return municipalityCode;
	}
	public void setMunicipalityCode(String municipalityCode) {
		this.municipalityCode = municipalityCode;
	}
	public String getMunicipalityName() {
		return municipalityName;
	}
	public void setMunicipalityName(String municipalityName) {
		this.municipalityName = municipalityName;
	}
	public String getHsaJpegLogotype() {
		return hsaJpegLogotype;
	}
	public void setHsaJpegLogotype(String hsaJpegLogotype) {
		this.hsaJpegLogotype = hsaJpegLogotype;
	}
	public String getCountyCode() {
		return countyCode;
	}
	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}
	public String getCountyName() {
		return countyName;
	}
	public void setCountyName(String countyName) {
		this.countyName = countyName;
	}
	public String getHsaHealthCareArea() {
		return hsaHealthCareArea;
	}
	public void setHsaHealthCareArea(String hsaHealthCareArea) {
		this.hsaHealthCareArea = hsaHealthCareArea;
	}
	public String getOrgNo() {
		return orgNo;
	}
	public void setOrgNo(String orgNo) {
		this.orgNo = orgNo;
	}
	public List<String> getPostalAddress() {
		return postalAddress;
	}
	public void setPostalAddress(List<String> postalAddress) {
		this.postalAddress = postalAddress;
	}
	public void addPostalAddress(String postalAddress) {
		if(this.postalAddress == null)
			this.postalAddress = new ArrayList<>();
		this.postalAddress.add(postalAddress);
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getHsaVisitingRuleReferral() {
		return hsaVisitingRuleReferral;
	}
	public void setHsaVisitingRuleReferral(String hsaVisitingRuleReferral) {
		this.hsaVisitingRuleReferral = hsaVisitingRuleReferral;
	}
	public String getModifyTimestamp() {
		return modifyTimestamp;
	}
	public void setModifyTimestamp(String modifyTimestamp) {
		this.modifyTimestamp = modifyTimestamp;
	}
	public String getCreateTimestamp() {
		return createTimestamp;
	}
	public void setCreateTimestamp(String createTimestamp) {
		this.createTimestamp = createTimestamp;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public List<TimeSpan> getTelephoneHours() {
		return telephoneHours;
	}
	public void setTelephoneHours(List<TimeSpan> telephoneHours) {
		this.telephoneHours = telephoneHours;
	}
	public void addTelephoneHours(TimeSpan telephoneHours) {
		if(this.telephoneHours == null)
			this.telephoneHours = new ArrayList<>();
		this.telephoneHours.add(telephoneHours);
	}
	public List<String> getHsaTextTelephoneNumber() {
		return hsaTextTelephoneNumber;
	}
	public void setHsaTextTelephoneNumber(List<String> hsaTextTelephoneNumber) {
		this.hsaTextTelephoneNumber = hsaTextTelephoneNumber;
	}
	public void addHsaTextTelephoneNumber(String hsaTextTelephoneNumber) {
		if(this.hsaTextTelephoneNumber == null)
			this.hsaTextTelephoneNumber = new ArrayList<String>();
		this.hsaTextTelephoneNumber.add(hsaTextTelephoneNumber);
	}
	public List<TimeSpan> getDropInHours() {
		return dropInHours;
	}
	public void setDropInHours(List<TimeSpan> dropInHours) {
		this.dropInHours = dropInHours;
	}
	public void addDropInHours(TimeSpan dropInHours) {
		if(this.dropInHours == null)
			this.dropInHours = new ArrayList<>();
		this.dropInHours.add(dropInHours);
	}

	public List<BusinessClassificationType> getBusinessClassificationType() {
		return businessClassificationType;
	}
	public void setBusinessClassificationType(List<BusinessClassificationType> businessClassificationType) {
		this.businessClassificationType = businessClassificationType;
	}
	public void addBusinessClassificationType(BusinessClassificationType businessClassificationType) {
		if(this.businessClassificationType == null)
			this.businessClassificationType = new ArrayList<>();
		this.businessClassificationType.add(businessClassificationType);
	}
	public List<String> getCareType() {
		return careType;
	}
	public void setCareType(List<String> careType) {
		this.careType = careType;
	}
	public void addCareType(String careType) {
		if(this.careType == null)
			this.careType = new ArrayList<String>();
		this.careType.add(careType);
	}
	public String getHsaSwitchboardNumber() {
		return hsaSwitchboardNumber;
	}
	public void setHsaSwitchboardNumber(String hsaSwitchboardNumber) {
		this.hsaSwitchboardNumber = hsaSwitchboardNumber;
	}
	public String getLabeledURI() {
		return labeledURI;
	}
	public void setLabeledURI(String labeledURI) {
		this.labeledURI = labeledURI;
	}
	public List<AgeSpan> getHsaVisitingRuleAge() {
		return hsaVisitingRuleAge;
	}
	public void setHsaVisitingRuleAge(List<AgeSpan> hsaVisitingRuleAge) {
		this.hsaVisitingRuleAge = hsaVisitingRuleAge;
	}
	public void addHsaVisitingRuleAge(AgeSpan hsaVisitingRuleAge) {
		if(this.hsaVisitingRuleAge == null)
			this.hsaVisitingRuleAge = new ArrayList<>();
		this.hsaVisitingRuleAge.add(hsaVisitingRuleAge);
	}
	public String getManagement() {
		return management;
	}
	public void setManagement(String management) {
		this.management = management;
	}
	public List<TimeSpan> getSurgeryHours() {
		return surgeryHours;
	}
	public void setSurgeryHours(List<TimeSpan> surgeryHours) {
		this.surgeryHours = surgeryHours;
	}
	public void addSurgeryHours(TimeSpan surgeryHours) {
		if(this.surgeryHours == null)
			this.surgeryHours = new ArrayList<>();
		this.surgeryHours.add(surgeryHours);
	}

	public List<String> getMobile() {
		return mobile;
	}
	public void setMobile(List<String> mobile) {
		this.mobile = mobile;
	}
	public void addMobile(String mobile) {
		if(this.mobile == null)
			this.mobile = new ArrayList<String>();
		this.mobile.add(mobile);
	}
	public String getPublicName() {
		return publicName;
	}
	public void setPublicName(String publicName) {
		this.publicName = publicName;
	}
	public String getFinancingOrganization() {
		return financingOrganization;
	}
	public void setFinancingOrganization(String financingOrganization) {
		this.financingOrganization = financingOrganization;
	}
	public String getO() {
		return o;
	}
	public void setO(String o) {
		this.o = o;
	}
	public String getOu() {
		return ou;
	}
	public void setOu(String ou) {
		this.ou = ou;
	}

}
