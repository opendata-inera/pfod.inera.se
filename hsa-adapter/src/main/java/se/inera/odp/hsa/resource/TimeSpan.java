package se.inera.odp.hsa.resource;

public class TimeSpan {

    private Integer fromDay; //1
    private String fromTime2; // "00:00",
    private Integer toDay;
    private String toTime2; // "24:00"
    private String comment;
    private String fromDate;
    private String toDate;
    
	public Integer getFromDay() {
		return fromDay;
	}
	public void setFromDay(Integer fromDay) {
		this.fromDay = fromDay;
	}
	public String getFromTime2() {
		return fromTime2;
	}
	public void setFromTime2(String fromTime2) {
		this.fromTime2 = fromTime2;
	}
	public Integer getToDay() {
		return toDay;
	}
	public void setToDay(Integer toDay) {
		this.toDay = toDay;
	}
	public String getToTime2() {
		return toTime2;
	}
	public void setToTime2(String toTime2) {
		this.toTime2 = toTime2;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
  
}
