package se.inera.odp.hsa.resource.xml;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import se.inera.odp.hsa.resource.AgeSpan;
import se.inera.odp.hsa.resource.BusinessClassificationType;
import se.inera.odp.hsa.resource.Coordinate;
import se.inera.odp.hsa.resource.TimeSpan;

public class Attribute {
	
	private String name;
	
	@JacksonXmlProperty(localName="S")	
	private String S;
	
	@JacksonXmlProperty(localName="Base64")	
	private String base64;
	
	@JacksonXmlProperty(localName="Address")
	private Address address;
	
	@JacksonXmlProperty(localName="Coordinate")
	private Coordinate coordinate;
	
	@JacksonXmlProperty(localName="TimeSpan")
	private TimeSpan timeSpan;
	
	@JacksonXmlProperty(localName="AgeSpan")
	private AgeSpan ageSpan;

	@JacksonXmlProperty(localName="BusinessClassificationType")
	BusinessClassificationType businessClassificationType;
	
	@JacksonXmlProperty(localName="Date")
	private String date;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getS() {
		return S;
	}
	public void setS(String s) {
		S = s;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public Coordinate getCoordinate() {
		return coordinate;
	}
	public void setCoordinate(Coordinate coordinate) {
		this.coordinate = coordinate;
	}
	public TimeSpan getTimeSpan() {
		return timeSpan;
	}
	public void setTimeSpan(TimeSpan timeSpan) {
		this.timeSpan = timeSpan;
	}

	public AgeSpan getAgeSpan() {
		return ageSpan;
	}
	public void setAgeSpan(AgeSpan ageSpan) {
		this.ageSpan = ageSpan;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	public BusinessClassificationType getBusinessClassificationType() {
		return businessClassificationType;
	}
	public void setBusinessClassificationType(BusinessClassificationType businessClassificationType) {
		this.businessClassificationType = businessClassificationType;
	}
	public String getText() {
		return S;
	}
	public String getBase64() {
		return base64;
	}
	public void setBase64(String base64) {
		this.base64 = base64;
	}
	
}
