package se.inera.odp.hsa.resource.xml;

import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class HsaObject {

	@JacksonXmlProperty(localName="DN")
	String dn;
	@JacksonXmlProperty(localName="Attribute")
	@JacksonXmlElementWrapper(useWrapping = false)
	List<Attribute> attributes;
	
	
	public String getDn() {
		return dn;
	}
	public void setDn(String dn) {
		this.dn = dn;
	}
	public List<Attribute> getAttributes() {
		return attributes;
	}
	public void setAttributes(List<Attribute> attribute) {
		this.attributes = attribute;
	}
	
	
}
