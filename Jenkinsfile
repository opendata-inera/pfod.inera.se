#!groovy

pipeline {
    agent { label 'maven' }

    environment {
        BRANCH_NAME = "${params.SOURCE_BRANCH}"
    }

    options {
        timeout(time: 1, unit: 'HOURS')
    }

    stages {
        stage('Print input') {
            steps {
                sh('echo "SOURCE_BRANCH: ' + env.BRANCH_NAME + '"')
            }
        }

        stage('Calculate version') {
            steps {
                script {
                    def commit = sh(returnStdout: true, script: "git log -n 1 --pretty=format:'%h'").trim()
                    def timestamp = sh(returnStdout: true, script: "date +%Y%m%dT%H%M%S").trim()
                    currentBuild.displayName = "${BRANCH_NAME}" + '-' + timestamp + '-' + commit;
                }
                sh('mvn versions:set -DnewVersion=' + currentBuild.displayName)
            }
        }

        stage('Build') {
            steps {
                sh 'mvn -B -DskipTests clean install'
            }
        }

        stage('Test') {
            steps {
                sh 'mvn test -Dspring.profiles.active=jenkins'
            }
            post {
                always {
                    junit '**/target/surefire-reports/*.xml'
                }
            }
        }

        stage('Build proxy docker image') {
            steps {
                sh('oc patch bc/proxy --patch \'{"spec":{"output":{"to":{"name":"docker.drift.inera.se/odp/proxy:' + currentBuild.displayName + '"}}}}\'')
                sh('oc start-build proxy --from-file=proxy/target/odp-proxy-' + currentBuild.displayName + '.jar --follow')
                sh('oc patch bc/proxy --patch \'{"spec":{"output":{"to":{"name":"docker.drift.inera.se/odp/proxy:latest"}}}}\'')
            }
        }

        stage('Build odp-application docker image') {
            steps {
                sh('oc patch bc/odp-application --patch \'{"spec":{"output":{"to":{"name":"docker.drift.inera.se/odp/application:' + currentBuild.displayName + '"}}}}\'')
                sh('oc start-build odp-application --from-file=application/target/odp-application-' + currentBuild.displayName + '.jar --follow')
                sh('oc patch bc/odp-application --patch \'{"spec":{"output":{"to":{"name":"docker.drift.inera.se/odp/application:latest"}}}}\'')
            }
        }

        stage('Build odp-db-api docker image') {
            steps {
                sh('oc patch bc/odp-db-api --patch \'{"spec":{"output":{"to":{"name":"docker.drift.inera.se/odp/db-api:' + currentBuild.displayName + '"}}}}\'')
                sh('oc start-build odp-db-api --from-file=db-api/target/odp-db-api-' + currentBuild.displayName + '.jar --follow')
                sh('oc patch bc/odp-db-api --patch \'{"spec":{"output":{"to":{"name":"docker.drift.inera.se/odp/db-api:latest"}}}}\'')
            }
        }

        stage('Build odp-kik-adapter docker image') {
            steps {
                sh('oc patch bc/odp-kik-adapter --patch \'{"spec":{"output":{"to":{"name":"docker.drift.inera.se/odp/kik-adapter:' + currentBuild.displayName + '"}}}}\'')
                sh('oc start-build odp-kik-adapter --from-file=kik-adapter/target/odp-kik-adapter-' + currentBuild.displayName + '.jar --follow')
                sh('oc patch bc/odp-kik-adapter --patch \'{"spec":{"output":{"to":{"name":"docker.drift.inera.se/odp/kik-adapter:latest"}}}}\'')
            }
        }

        stage('Build odp-nkk-adapter docker image') {
            steps {
                sh('oc patch bc/odp-nkk-adapter --patch \'{"spec":{"output":{"to":{"name":"docker.drift.inera.se/odp/nkk-adapter:' + currentBuild.displayName + '"}}}}\'')
                sh('oc start-build odp-nkk-adapter --from-file=nkk-adapter/target/odp-nkk-adapter-' + currentBuild.displayName + '.jar --follow')
                sh('oc patch bc/odp-nkk-adapter --patch \'{"spec":{"output":{"to":{"name":"docker.drift.inera.se/odp/nkk-adapter:latest"}}}}\'')
            }
        }

        stage('Build odp-hsa-adapter docker image') {
            steps {
                sh('oc patch bc/odp-hsa-adapter --patch \'{"spec":{"output":{"to":{"name":"docker.drift.inera.se/odp/hsa-adapter:' + currentBuild.displayName + '"}}}}\'')
                sh('oc start-build odp-hsa-adapter --from-file=hsa-adapter/target/odp-hsa-adapter-' + currentBuild.displayName + '.jar --follow')
                sh('oc patch bc/odp-hsa-adapter --patch \'{"spec":{"output":{"to":{"name":"docker.drift.inera.se/odp/hsa-adapter:latest"}}}}\'')
            }
        }

        stage('Deploy to development environment') {
            when { expression { return env.BRANCH_NAME == 'develop' } }
            steps {
                build job: 'ODP Deploy to ' + env.OPENSHIFT_NAMESPACE, parameters: [string(name: 'ODP_IMAGE_VERSION', value: currentBuild.displayName), string(name: 'INFRASTRUCTURE_BRANCH', value: 'master')]
            }
        }
    }
    post {
        changed {
            script {
                emailext subject: '$DEFAULT_SUBJECT',
                        body: '$DEFAULT_CONTENT',
                        recipientProviders: [
                                [$class: 'CulpritsRecipientProvider'],
                                [$class: 'DevelopersRecipientProvider'],
                                [$class: 'RequesterRecipientProvider']
                        ],
                        replyTo: '$DEFAULT_REPLYTO',
                        to: '$DEFAULT_RECIPIENTS'
            }
        }
    }
}
