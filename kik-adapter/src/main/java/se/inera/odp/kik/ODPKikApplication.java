package se.inera.odp.kik;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages= {"se.inera.odp.kik", "se.inera.odp.kik.*", "se.inera.odp.core.*"})
public class ODPKikApplication {

 
    public static void main(String[] args) {
        SpringApplication.run(ODPKikApplication.class, args);
    }
    
}
