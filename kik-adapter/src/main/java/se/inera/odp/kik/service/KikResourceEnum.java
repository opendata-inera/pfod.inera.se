package se.inera.odp.kik.service;

public enum KikResourceEnum {
	checksums("id"),
	codes("id"), 
	codeSystems("id"),
	measureFormerVersions("measureFormerVersionId"),
	measures("measureId"),
	performingOrganizations("performingOrganizationId"),
	publiclyAvailableMeasures(""),
	targetMeasurements("targetId"),
	valueSets("valueSetId"),
	viewGroups("id"),
	viewTemplates("id");
	
	private String pk;
	
	private KikResourceEnum(String pk) {
		this.pk = pk;
	}

	public String getPk() {
		return pk;
	}
}
