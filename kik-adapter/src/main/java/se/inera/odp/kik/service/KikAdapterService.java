package se.inera.odp.kik.service;

import java.io.InputStream;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import se.inera.odp.core.exception.ODPException;
import se.inera.odp.core.request.ODPRequest;
import se.inera.odp.core.response.ODPResponse;
import se.inera.odp.core.service.GenericAdapterService;
import se.inera.odp.core.utils.ResourceValidator;
import se.inera.odp.kik.client.KikAdapterClient;

import static se.inera.odp.core.exception.ErrorCodes.*;

@Service
public class KikAdapterService extends GenericAdapterService {

	Logger logger = LoggerFactory.getLogger(KikAdapterService.class);

	@Autowired
	KikAdapterClient adapterClient;

	@Autowired
	private ResourceValidator validator;

	/*
	private static final String DEFINITION_KIK_V1_CHECKSUMS = "kik/definition_kik_v1_checksums.json";
	private static final String DEFINITION_KIK_V1_CODES = "kik/definition_kik_v1_codes.json";
	private static final String DEFINITION_KIK_V1_CODESYSTEMS = "kik/definition_kik_v1_codeSystems.json";
	private static final String DEFINITION_KIK_V1_MEASURES = "kik/definition_kik_v1_measures.json";
	private static final String DEFINITION_KIK_V1_MEASURESFORMERVERSIONS = "kik/definition_kik_v1_measuresFormerVersions.json";
	private static final String DEFINITION_KIK_V1_PERFORMINGORGANIZATIONS = "kik/definition_kik_v1_performingOrganizations.json";
	private static final String DEFINITION_KIK_V1_TARGETMEASUREMENTS = "kik/definition_kik_v1_targetMeasurements.json";
	private static final String DEFINITION_KIK_V1_VALUESETS = "kik/definition_kik_v1_valueSets.json";
	private static final String DEFINITION_KIK_V3_CHECKSUMS = "kik/definition_kik_v3_checksums.json";
	private static final String DEFINITION_KIK_V3_CODES = "kik/definition_kik_v3_codes.json";
	private static final String DEFINITION_KIK_V3_CODESYSTEMS = "kik/definition_kik_v3_codeSystems.json";
	private static final String DEFINITION_KIK_V3_MEASURES = "kik/definition_kik_v3_measures.json";
	private static final String DEFINITION_KIK_V3_PERFORMINGORGANIZATIONS = "kik/definition_kik_v3_performingOrganizations.json";
	private static final String DEFINITION_KIK_V3_TARGETMEASUREMENTS = "kik/definition_kik_v3_targetMeasurements.json";
	private static final String DEFINITION_KIK_V3_VALUESETS = "kik/definition_kik_v3_valueSets.json";
	private static final String DEFINITION_KIK_V3_VIEWGROUPS = "kik/definition_kik_v3_viewGroups.json";
	private static final String DEFINITION_KIK_V3_VIEWTEMPLATES = "kik/definition_kik_v3_viewTemplates.json";
	*/

	@Autowired
	public KikAdapterService(ObjectMapper mapper) {
		super(mapper);
	}

	public int saveResource(String auth, String version, KikResourceEnum resource_id, List<Object> data) {
		int size = data == null ? -1 : data.size();
		ODPRequest<Object> req = createResourceDefinition(version, resource_id);
		req.setRecords(data);

		validator.validateResource(req);

		adapterClient.createResource(auth, req, ODPRequest.class);
		
		return size;
	}

	public int updateResource(String auth, String version, KikResourceEnum resource_id, List<Object> data) {

		int size = data == null ? -1 : data.size();

		// Validate
		ODPRequest<Object> req = createResourceDefinition(version, resource_id);
		req.setRecords(data);
		validator.validateResource(req);
		
		// Update
		adapterClient.updateResource(auth, createResourceName(version, resource_id), data);
		
		return size;
	}
	
	public void updateResource(String auth, String version, KikResourceEnum resource_id, String row_id, String data) {

		if(data == null || data.trim().length() == 0)
			throw new ODPException(HttpStatus.BAD_REQUEST, "Data saknas", ERROR_CODE_DATA_ERROR);
				
		Map<String, String> datamap = null;

		data = data.trim();
		
		if(data.startsWith("[")) {
			throw new ODPException(HttpStatus.BAD_REQUEST, "Endast en post kan uppdateras", ERROR_CODE_DATA_ERROR);
		} else if(!data.startsWith("{")) {
			throw new ODPException(HttpStatus.BAD_REQUEST, "Posten måste börja med {", ERROR_CODE_DATA_ERROR);
		}
			
		datamap = readValue(data, Map.class);
		
		ODPRequest<Object> req = createResourceDefinition(version, resource_id);		
		validator.validateResource(req, datamap);

		String value = (String)datamap.get(resource_id.getPk());

		if(value == null)
			datamap.put(resource_id.getPk(), row_id);
		else if(!value.equals(row_id))
			throw new ODPException(HttpStatus.BAD_REQUEST, "Id i urlen är inte samma som i data.", ERROR_CODE_URL_PARSING);

		adapterClient.updateResource(auth, createResourceName(version, resource_id), data);

	}
	
	public String deleteResource(String auth, String version, KikResourceEnum resource_id, String row_id) {	
		adapterClient.deleteResource(auth, createResourceName(version, resource_id), resource_id.getPk(), row_id);
		return null;
	}

	private String createResourceName(String version, KikResourceEnum resource_id) {
		String resource_name = "kik-" + version + "-" + resource_id;
		return resource_name;
	}

	private ODPRequest<Object> createResourceDefinition(String version, KikResourceEnum resource_id) {
		String definition = "kik/definition_kik_" + version + "_" + resource_id + ".json";
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(definition);
		ODPRequest<Object> req = readValue(is, ODPRequest.class);
		return req;
	}
	
	public String getResourceById(String version, KikResourceEnum resource_id, String id, Map<String, String> params, String auth) {
		if(id != null)
			params.put(resource_id.getPk(), id);
		return adapterClient.getData(auth, createResourceName(version, resource_id), params, String.class);
	}
	public List<?> getResourceAsList(String version, KikResourceEnum resource_id, String id, Map<String, String> params, String auth) {
		if(id != null)
			params.put(resource_id.getPk(), id);
		return adapterClient.getData(auth, createResourceName(version, resource_id), params, List.class);
	}
	public ODPResponse getResourceAsObject(String version, KikResourceEnum resource_id, String id, Map<String, String> params, String auth) {
		if(id != null)
			params.put(resource_id.getPk(), id);
		return adapterClient.getData(auth, createResourceName(version, resource_id), params, ODPResponse.class);
	}
}
