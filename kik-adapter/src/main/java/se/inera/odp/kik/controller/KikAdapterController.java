package se.inera.odp.kik.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import se.inera.odp.core.exception.ODPException;
import se.inera.odp.core.response.ODPResponse;
import se.inera.odp.core.response.ResponseMessage;
import se.inera.odp.core.utils.KeyManager;
import se.inera.odp.core.utils.ResourceValidator;
import se.inera.odp.core.utils.ResponseLoggerMapper;
import se.inera.odp.kik.service.KikAdapterService;
import se.inera.odp.kik.service.KikResourceEnum;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@RestController
@RequestMapping({""})
@Api(value = "kik", tags = {"Kvalitetsindikatorer"}, produces = "application/json", consumes = "application/json")
public class KikAdapterController {

    @Autowired
    KikAdapterService kikAdapterService;

    @Autowired
    private ResponseLoggerMapper responseMapper;

    @Autowired
    private KeyManager keyManager;

    @Autowired
    private ResourceValidator validator;

    @ApiOperation(value = "Returnerar ett objekt med kvalitetsindikatorer av typen 'resource_id'", response = ODPResponse.class)
    @GetMapping(value = {"/{version:v1|v2|v3}/{resource_id}",
        "/{version:v1|v2|v3}/{resource_id}/{id}"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getResourceById(
        @RequestHeader(value = AUTHORIZATION, required = false) String auth,
        @PathVariable String version,
        @PathVariable KikResourceEnum resource_id,
        @PathVariable(required = false) String id,
        @RequestParam(required = false) Integer limit,
        @RequestParam(required = false) Integer offset,
        @RequestParam(required = false) String fields,
        @RequestParam(required = false) String sort,
        @RequestParam(required = false) Map<String, String> params) {

        ODPResponse result = kikAdapterService.getResourceAsObject(version, resource_id, id, params, keyManager.getDatasetKeyForRead(auth));
        responseMapper.responseMessage(HttpStatus.OK, "OK", result.getTotal());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @ApiOperation(value = "Returnerar en lista med kvalitetsindikatorer av typen 'resource_id'", response = List.class)
    @GetMapping(value = {"/{version:v1|v2|v3}/{resource_id}/list",
        "/{version:v1|v2|v3}/{resource_id}/{id}/list"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List> getResourceAsList(
        @RequestHeader(value = AUTHORIZATION, required = false) String auth,
        @PathVariable String version,
        @PathVariable KikResourceEnum resource_id,
        @PathVariable(required = false) String id,
        @RequestParam(required = false) Integer limit,
        @RequestParam(required = false) Integer offset,
        @RequestParam(required = false) String fields,
        @RequestParam(required = false) String sort,
        @RequestParam(required = false) Map<String, String> params) {

        List<?> result = kikAdapterService.getResourceAsList(version, resource_id, id, params, keyManager.getDatasetKeyForRead(auth));
        responseMapper.responseMessage(HttpStatus.OK, "OK", result.size());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }


    /**
     * Example POST /api/kik/v1/codes (delete all -> insert)
     */
    @ApiOperation(value = "Skapar eller uppdaterar en resurs av typen 'resource_id'", response = ResponseMessage.class)
    @PostMapping(value = "/{version:v1|v2|v3}/{resource_id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseMessage> createData(@RequestHeader(value = "Authorization", required = false) String auth,
        @PathVariable String version, @PathVariable KikResourceEnum resource_id,
        @RequestBody List<Object> data, @RequestHeader HttpHeaders headers) {

        int size = kikAdapterService.saveResource(keyManager.getDatasetKeyForWrite(auth), version, resource_id, data);
        return new ResponseEntity<>(responseMapper.responseMessage(HttpStatus.CREATED, "OK", size), HttpStatus.CREATED);
    }

    /**
     * Example DELETE /api/kik/v1/codes/{id}
     */
    @DeleteMapping(value = "/{version:v1|v2|v3}/{resource_id}/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Tar bort en post med nyckel 'id' från resurs av typen 'resource_id'", response = ResponseMessage.class)
    public ResponseEntity<?> deleteResource(
        @RequestHeader(value = AUTHORIZATION) String auth,
        @PathVariable String version, @PathVariable KikResourceEnum resource_id,
        @PathVariable String id) throws IOException {

        kikAdapterService.deleteResource(keyManager.getDatasetKeyForWrite(auth), version, resource_id, id);
        return new ResponseEntity<>(responseMapper.responseMessage(HttpStatus.OK, "OK"), HttpStatus.OK);
    }

    @PutMapping(value = "/{version:v1|v2|v3}/{resource_id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Uppdaterar 'resource_id' från en lista med poster", response = ResponseMessage.class)
    public ResponseEntity<?> updateResource(
        @RequestHeader(value = AUTHORIZATION) String auth,
        @PathVariable String version, @PathVariable KikResourceEnum resource_id,
        @RequestBody List<Object> data) throws IOException {

        int size = kikAdapterService.updateResource(keyManager.getDatasetKeyForWrite(auth), version, resource_id, data);
        return new ResponseEntity<>(responseMapper.responseMessage(HttpStatus.OK, "OK", size), HttpStatus.OK);
    }

    /**
     * Example POST /api/kik/v1/codes/{id} (update(if exists) else insert)
     */
    @RequestMapping(value = "/{version:v1|v2|v3}/{resource_id}/{id}", method = {RequestMethod.POST,
        RequestMethod.PUT}, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Uppdaterar en post med nyckel 'id' i resursen 'resource_id'", response = ResponseMessage.class)
    public ResponseEntity<?> updateResource(
        @RequestHeader(value = AUTHORIZATION) String auth,
        @PathVariable String version, @PathVariable KikResourceEnum resource_id, @PathVariable String id,
        @RequestBody String data) throws IOException {

        kikAdapterService.updateResource(keyManager.getDatasetKeyForWrite(auth), version, resource_id, id, data);
        return new ResponseEntity<>(responseMapper.responseMessage(HttpStatus.OK, "OK", 1), HttpStatus.OK);
    }
}
