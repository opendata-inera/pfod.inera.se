# odp-application

odp-application tillhandahåller ett api mot CKAN för öppen data. 


## Metoder

Följande metoder finns:


### GET /api/get/{dataset\_id}/{resource_id}
Hämta en resurs från CKAN. Som default krävs ingen autentisering.  
Stöd finns för frågeparametrarna
* limit 
* offset 
* fields
* sort". 

Parametrarna "resource\_id", "filters"," q", "distinct", "plain", "language", "include\_total", "records\_format" är ej tillåtna. Andra parametrar betraktas som fältnnamn och kommer att användas för filtrering ("filters"). Någon kontroll om namnet finns görs dock inte.

Exempel: http://someurl.se/api/get/dataset/resurs?limit=5&something=avalue


### POST /api/save
Uppdatera eller skapa en ny resurs. Kräver autentisering.


### DELETE /api/delete/{dataset\_id}/{resource_id}
Ta bort en resurs. Kräver autentisering.


### DELETE /api/delete/{dataset\_id}/{resource\_id}/{row_id}
Ta bort en specifik post från en resurs. Kräver autentisering.


### PUT /api/update/{dataset\_id}/{resource_id}
Uppdatera en specifik resurs. Kräver autentisering.


## Autentisering


