package se.inera.odp.dbapi.service;

import static org.assertj.core.api.Assertions.assertThat;
import static se.inera.odp.dbapi.common.TestUtils.*;

import java.util.HashMap;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import se.inera.odp.core.response.ODPResponse;
import se.inera.odp.dbapi.repository.ResourceRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ODPDbApiServiceTest {

    @Autowired
    private ResourceRepository resourceRepository;

    @Autowired
    private ODPDbApiService service;

    @Before
    public void setup(){
        service.setResourceRepository(resourceRepository);
    }

    @Test
    public void getResourceWithLimit(){
        insertOdpResources(resourceRepository);

        Map<String, String> params = new HashMap<String, String>();
        params.put("limit","2");
        ODPResponse resourceAsObject1 = service.getResourceAsObject(TEST_DATASET1, TEST_RESOURCE1, params, "auth", "url");

        params.put("offset","2");
        ODPResponse resourceAsObject2 = service.getResourceAsObject(TEST_DATASET1, TEST_RESOURCE1, params, "auth", "url");

        params.put("offset","4");
        ODPResponse resourceAsObject3 = service.getResourceAsObject(TEST_DATASET1, TEST_RESOURCE1, params, "auth", "url");

        params.put("offset","6");
        ODPResponse resourceAsObject4 = service.getResourceAsObject(TEST_DATASET1, TEST_RESOURCE1, params, "auth", "url");

        assertThat(resourceAsObject1.getRecords().size()).isEqualTo(2);
        assertThat(resourceAsObject1.getLimit()).isEqualTo(2);
        assertThat(resourceAsObject1.getOffset()).isEqualTo(0);
        Map<String, String> record = (Map<String, String>)resourceAsObject1.getRecords().get(0);
        assertThat(record.get("id")).isEqualTo("1");
        record = (Map<String, String>)resourceAsObject1.getRecords().get(1);
        assertThat(record.get("id")).isEqualTo("2");

        assertThat(resourceAsObject2.getRecords().size()).isEqualTo(2);
        assertThat(resourceAsObject2.getLimit()).isEqualTo(2);
        assertThat(resourceAsObject2.getOffset()).isEqualTo(2);
        record = (Map<String, String>)resourceAsObject2.getRecords().get(0);
        assertThat(record.get("id")).isEqualTo("3");
        record = (Map<String, String>)resourceAsObject2.getRecords().get(1);
        assertThat(record.get("id")).isEqualTo("4");

        assertThat(resourceAsObject3.getRecords().size()).isEqualTo(2);
        assertThat(resourceAsObject3.getLimit()).isEqualTo(2);
        assertThat(resourceAsObject3.getOffset()).isEqualTo(4);
        record = (Map<String, String>)resourceAsObject3.getRecords().get(0);
        assertThat(record.get("id")).isEqualTo("5");
        record = (Map<String, String>)resourceAsObject3.getRecords().get(1);
        assertThat(record.get("id")).isEqualTo("6");

        assertThat(resourceAsObject4.getRecords().size()).isEqualTo(2);
        assertThat(resourceAsObject4.getLimit()).isEqualTo(2);
        assertThat(resourceAsObject4.getOffset()).isEqualTo(6);
        record = (Map<String, String>)resourceAsObject4.getRecords().get(0);
        assertThat(record.get("id")).isEqualTo("7");
        record = (Map<String, String>)resourceAsObject4.getRecords().get(1);
        assertThat(record.get("id")).isEqualTo("8");

    }

    @Test
    public void getResourceWithLimitAndSort(){
        insertOdpResources(resourceRepository);

        Map<String, String> params = new HashMap<String, String>();
        params.put("limit","2");
        params.put("sort","id desc");
        ODPResponse resourceAsObject1 = service.getResourceAsObject(TEST_DATASET1, TEST_RESOURCE1, params, "auth", "url");

        params.put("offset","2");
        ODPResponse resourceAsObject2 = service.getResourceAsObject(TEST_DATASET1, TEST_RESOURCE1, params, "auth", "url");

        params.put("offset","4");
        ODPResponse resourceAsObject3 = service.getResourceAsObject(TEST_DATASET1, TEST_RESOURCE1, params, "auth", "url");

        params.put("offset","6");
        ODPResponse resourceAsObject4 = service.getResourceAsObject(TEST_DATASET1, TEST_RESOURCE1, params, "auth", "url");

        assertThat(resourceAsObject1.getRecords().size()).isEqualTo(2);
        assertThat(resourceAsObject1.getLimit()).isEqualTo(2);
        assertThat(resourceAsObject1.getOffset()).isEqualTo(0);
        Map<String, String> record = (Map<String, String>)resourceAsObject1.getRecords().get(0);
        assertThat(record.get("id")).isEqualTo("9");
        record = (Map<String, String>)resourceAsObject1.getRecords().get(1);
        assertThat(record.get("id")).isEqualTo("8");

        assertThat(resourceAsObject2.getRecords().size()).isEqualTo(2);
        assertThat(resourceAsObject2.getLimit()).isEqualTo(2);
        assertThat(resourceAsObject2.getOffset()).isEqualTo(2);
        record = (Map<String, String>)resourceAsObject2.getRecords().get(0);
        assertThat(record.get("id")).isEqualTo("7");
        record = (Map<String, String>)resourceAsObject2.getRecords().get(1);
        assertThat(record.get("id")).isEqualTo("6");

        assertThat(resourceAsObject3.getRecords().size()).isEqualTo(2);
        assertThat(resourceAsObject3.getLimit()).isEqualTo(2);
        assertThat(resourceAsObject3.getOffset()).isEqualTo(4);
        record = (Map<String, String>)resourceAsObject3.getRecords().get(0);
        assertThat(record.get("id")).isEqualTo("5");
        record = (Map<String, String>)resourceAsObject3.getRecords().get(1);
        assertThat(record.get("id")).isEqualTo("4");

        assertThat(resourceAsObject4.getRecords().size()).isEqualTo(2);
        assertThat(resourceAsObject4.getLimit()).isEqualTo(2);
        assertThat(resourceAsObject4.getOffset()).isEqualTo(6);
        record = (Map<String, String>)resourceAsObject4.getRecords().get(0);
        assertThat(record.get("id")).isEqualTo("3");
        record = (Map<String, String>)resourceAsObject4.getRecords().get(1);
        assertThat(record.get("id")).isEqualTo("2");

    }

    @Test
    public void getResourceWithFilterAndLimit(){
        insertOdpResources(resourceRepository);

        Map<String, String> params = new HashMap<String, String>();
        params.put("limit","2");
        params.put(KEY1,"456");
        ODPResponse resourceAsObject1 = service.getResourceAsObject(TEST_DATASET1, TEST_RESOURCE1, params, "auth", "url");

        params.put("offset","2");
        params.put(KEY1,"456");
        ODPResponse resourceAsObject2 = service.getResourceAsObject(TEST_DATASET1, TEST_RESOURCE1, params, "auth", "url");

        params.put("offset","4");
        params.put(KEY1,"456");
        ODPResponse resourceAsObject3 = service.getResourceAsObject(TEST_DATASET1, TEST_RESOURCE1, params, "auth", "url");

        assertThat(resourceAsObject1.getRecords().size()).isEqualTo(2);
        assertThat(resourceAsObject1.getLimit()).isEqualTo(2);
        assertThat(resourceAsObject1.getOffset()).isEqualTo(0);
        Map<String, String> record = (Map<String, String>)resourceAsObject1.getRecords().get(0);
        assertThat(record.get("id")).isEqualTo("2");
        assertThat(record.get(KEY1)).isEqualTo("456");
        record = (Map<String, String>)resourceAsObject1.getRecords().get(1);
        assertThat(record.get("id")).isEqualTo("5");
        assertThat(record.get(KEY1)).isEqualTo("456");

        assertThat(resourceAsObject2.getRecords().size()).isEqualTo(2);
        assertThat(resourceAsObject2.getLimit()).isEqualTo(2);
        assertThat(resourceAsObject2.getOffset()).isEqualTo(2);
        record = (Map<String, String>)resourceAsObject2.getRecords().get(0);
        assertThat(record.get("id")).isEqualTo("6");
        assertThat(record.get(KEY1)).isEqualTo("456");
        record = (Map<String, String>)resourceAsObject2.getRecords().get(1);
        assertThat(record.get("id")).isEqualTo("7");
        assertThat(record.get(KEY1)).isEqualTo("456");

        assertThat(resourceAsObject3.getRecords().size()).isEqualTo(2);
        assertThat(resourceAsObject3.getLimit()).isEqualTo(2);
        assertThat(resourceAsObject3.getOffset()).isEqualTo(4);
        record = (Map<String, String>)resourceAsObject3.getRecords().get(0);
        assertThat(record.get("id")).isEqualTo("8");
        assertThat(record.get(KEY1)).isEqualTo("456");
        record = (Map<String, String>)resourceAsObject3.getRecords().get(1);
        assertThat(record.get("id")).isEqualTo("9");
        assertThat(record.get(KEY1)).isEqualTo("456");
    }

    @Test
    public void getResourceWithFilterLimitAndSort(){
        insertOdpResources(resourceRepository);

        Map<String, String> params = new HashMap<String, String>();
        params.put("limit","2");
        params.put(KEY1,"456");
        params.put("sort","id desc");
        ODPResponse resourceAsObject1 = service.getResourceAsObject(TEST_DATASET1, TEST_RESOURCE1, params, "auth", "url");

        params.put("offset","2");
        params.put(KEY1,"456");
        ODPResponse resourceAsObject2 = service.getResourceAsObject(TEST_DATASET1, TEST_RESOURCE1, params, "auth", "url");

        params.put("offset","4");
        params.put(KEY1,"456");
        ODPResponse resourceAsObject3 = service.getResourceAsObject(TEST_DATASET1, TEST_RESOURCE1, params, "auth", "url");

        assertThat(resourceAsObject1.getRecords().size()).isEqualTo(2);
        assertThat(resourceAsObject1.getLimit()).isEqualTo(2);
        assertThat(resourceAsObject1.getOffset()).isEqualTo(0);
        Map<String, String> record = (Map<String, String>)resourceAsObject1.getRecords().get(0);
        assertThat(record.get("id")).isEqualTo("9");
        assertThat(record.get(KEY1)).isEqualTo("456");
        record = (Map<String, String>)resourceAsObject1.getRecords().get(1);
        assertThat(record.get("id")).isEqualTo("8");
        assertThat(record.get(KEY1)).isEqualTo("456");

        assertThat(resourceAsObject2.getRecords().size()).isEqualTo(2);
        assertThat(resourceAsObject2.getLimit()).isEqualTo(2);
        assertThat(resourceAsObject2.getOffset()).isEqualTo(2);
        record = (Map<String, String>)resourceAsObject2.getRecords().get(0);
        assertThat(record.get("id")).isEqualTo("7");
        assertThat(record.get(KEY1)).isEqualTo("456");
        record = (Map<String, String>)resourceAsObject2.getRecords().get(1);
        assertThat(record.get("id")).isEqualTo("6");
        assertThat(record.get(KEY1)).isEqualTo("456");

        assertThat(resourceAsObject3.getRecords().size()).isEqualTo(2);
        assertThat(resourceAsObject3.getLimit()).isEqualTo(2);
        assertThat(resourceAsObject3.getOffset()).isEqualTo(4);
        record = (Map<String, String>)resourceAsObject3.getRecords().get(0);
        assertThat(record.get("id")).isEqualTo("5");
        assertThat(record.get(KEY1)).isEqualTo("456");
        record = (Map<String, String>)resourceAsObject3.getRecords().get(1);
        assertThat(record.get("id")).isEqualTo("2");
        assertThat(record.get(KEY1)).isEqualTo("456");
    }

    @Test
    public void getResourceFields(){
        insertOdpResources(resourceRepository);

        Map<String, String> params = new HashMap<String, String>();
        params.put("fields","id,key1,displayName");

        ODPResponse resourceAsObject1 = service.getResourceAsObject(TEST_DATASET1, TEST_RESOURCE1, params, "auth", "url");

        assertThat(resourceAsObject1.getRecords().get(0).get("id")).isNotNull();
        assertThat(resourceAsObject1.getRecords().get(0).get("key1")).isNotNull();
        assertThat(resourceAsObject1.getRecords().get(0).get("displayName")).isNotNull();
        assertThat(resourceAsObject1.getRecords().get(0).get("active")).isNull();
        assertThat(resourceAsObject1.getRecords().get(0).get("key2")).isNull();
        assertThat(resourceAsObject1.getRecords().get(0).get("key3")).isNull();
    }
}
