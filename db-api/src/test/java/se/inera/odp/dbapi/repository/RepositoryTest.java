package se.inera.odp.dbapi.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static se.inera.odp.dbapi.common.TestUtils.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import se.inera.odp.dbapi.model.OdpResource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RepositoryTest {

    @Autowired
    protected ResourceRepository repository;

    @Autowired
    private ObjectMapper mapper;

    @Test
    public void findByResourceName(){
        insertOdpResources(repository);
        Optional<OdpResource> resource = repository.findByName(TEST_RESOURCE1);
        assertThat(resource.isPresent()).isTrue();
        assertThat(resource.get().getName()).isEqualTo(TEST_RESOURCE1);
    }

    @Test
    public void findByFilter_OneKey() throws IOException {
        insertOdpResources(repository);

        Optional<OdpResource> resource = repository.getFiltered(TEST_RESOURCE1, KEY1, "123");

        assertThat(resource.isPresent()).isTrue();
        assertThat(resource.get().getName()).isEqualTo(TEST_RESOURCE1);

        String data = resource.get().getData();

        Map<String, ?> map = mapper.readValue(data, Map.class);
        List<Map<String, ?>> records = (List<Map<String, ?>>) map.get("records");
        assertThat(records.size()).isEqualTo(2);
        assertThat(records.stream().allMatch( r -> r.get(KEY1).equals("123"))).isTrue();
    }

    @Test
    public void findByFilter_TwoKeys() throws IOException {
        insertOdpResources(repository);

        Optional<OdpResource> resource = repository.getFiltered(TEST_RESOURCE1, KEY1, "456", KEY2, "4567");

        assertThat(resource.isPresent()).isTrue();
        assertThat(resource.get().getName()).isEqualTo(TEST_RESOURCE1);

        String data = resource.get().getData();

        Map<String, ?> map = mapper.readValue(data, Map.class);
        List<Map<String, ?>> records = (List<Map<String, ?>>) map.get("records");
        assertThat(records.size()).isEqualTo(4);
        assertThat(records.stream().allMatch( r -> r.get(KEY1).equals("456"))).isTrue();
        assertThat(records.stream().allMatch( r -> r.get(KEY2).equals("4567"))).isTrue();
    }

    @Test
    public void findByFilter_ThreeKeys() throws IOException {
        insertOdpResources(repository);

        Optional<OdpResource> resource = repository.getFiltered(TEST_RESOURCE1, KEY1, "456", KEY2, "4567", KEY3, "45678");

        assertThat(resource.isPresent()).isTrue();
        assertThat(resource.get().getName()).isEqualTo(TEST_RESOURCE1);

        String data = resource.get().getData();

        Map<String, ?> map = mapper.readValue(data, Map.class);
        List<Map<String, ?>> records = (List<Map<String, ?>>) map.get("records");
        assertThat(records.size()).isEqualTo(2);
        assertThat(records.stream().allMatch( r -> r.get(KEY1).equals("456"))).isTrue();
        assertThat(records.stream().allMatch( r -> r.get(KEY2).equals("4567"))).isTrue();
        assertThat(records.stream().allMatch( r -> r.get(KEY3).equals("45678"))).isTrue();
    }

    @Test
    public void recordExists(){
        insertOdpResources(repository);

        assertThat(repository.recordExists(TEST_RESOURCE1, "id", "1")).isTrue();
        assertThat(repository.recordExists(TEST_RESOURCE1, "id", "100")).isFalse();
    }

    @Test
    public void deleteRecord() throws IOException {
        insertOdpResources(repository);

        repository.deleteByField(TEST_RESOURCE1, "id", "10");

        Optional<OdpResource> resource = repository.findByName(TEST_RESOURCE1);

        assertThat(resource.isPresent()).isTrue();
        assertThat(resource.get().getName()).isEqualTo(TEST_RESOURCE1);

        String data = resource.get().getData();
        Map<String, ?> map = mapper.readValue(data, Map.class);
        List<Map<String, ?>> records = (List<Map<String, ?>>) map.get("records");

        assertThat(records.size()).isEqualTo(9);
    }

    @Test
    public void updateRecord() throws IOException {
        insertOdpResources(repository);

        String recordToUpdate = "{\n"
            + "            \"id\": \"5\",\n"
            + "            \"active\": true,\n"
            + "            \"key1\": \"999\",\n"
            + "            \"key3\": \"999\",\n"
            + "            \"displayName\": \"TestString6\",\n"
            + "            \"key2\": \"999\"}";

        repository.updateByField(TEST_RESOURCE1, "id", "5", recordToUpdate);

        Optional<OdpResource> resource = repository.getFiltered(TEST_RESOURCE1, "id", "5");

        assertThat(resource.isPresent()).isTrue();
        assertThat(resource.get().getName()).isEqualTo(TEST_RESOURCE1);

        String data = resource.get().getData();
        Map<String, ?> map = mapper.readValue(data, Map.class);
        List<Map<String, ?>> records = (List<Map<String, ?>>) map.get("records");

        assertThat(records.size()).isEqualTo(1);
        assertThat(records.get(0).get(KEY1)).isEqualTo("999");
    }

    @Test
    public void insertRecord() throws IOException {
        insertOdpResources(repository);
        String recordToInsert = "{\n"
            + "            \"id\": \"11\",\n"
            + "            \"active\": true,\n"
            + "            \"key1\": \"999\",\n"
            + "            \"key3\": \"999\",\n"
            + "            \"displayName\": \"TestString6\",\n"
            + "            \"key2\": \"999\"}";

        repository.insertRecord(TEST_RESOURCE1, recordToInsert);
        repository.flush();

        Optional<OdpResource> resource = repository.findByName(TEST_RESOURCE1);

        assertThat(resource.isPresent()).isTrue();
        assertThat(resource.get().getName()).isEqualTo(TEST_RESOURCE1);

        String data = resource.get().getData();
        Map<String, ?> map = mapper.readValue(data, Map.class);
        List<?> records = (List<?>) map.get("records");

        assertThat(records.size()).isEqualTo(11);
    }

    @Test
    public void getPrimaryKey(){
        insertOdpResources(repository);

        String pk1 = repository.getPrimaryKeyForResource(TEST_RESOURCE1);
        String pk2 = repository.getPrimaryKeyForResource(TEST_RESOURCE2);

        assertThat(pk1).isEqualTo("id");
        assertThat(pk2).isEqualTo("id");
    }
}