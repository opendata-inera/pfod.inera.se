CREATE TABLE public.odp_resource
(
  id integer NOT NULL,
  dataset character varying(100) NOT NULL,
  name character varying(32) NOT NULL,
  CONSTRAINT odp_resource_pkey PRIMARY KEY (id),
  data jsonb
)
WITH (
  OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.odp_resource
  OWNER to odpadmin;


CREATE SEQUENCE public.hibernate_sequence
    INCREMENT 1
    START 2
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.hibernate_sequence
    OWNER TO odpadmin;