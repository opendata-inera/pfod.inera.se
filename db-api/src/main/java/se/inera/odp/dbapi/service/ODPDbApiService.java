package se.inera.odp.dbapi.service;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import se.inera.odp.core.exception.ODPException;
import se.inera.odp.core.request.CKANError;
import se.inera.odp.core.request.LinkType;
import se.inera.odp.core.request.ODPRequest;
import se.inera.odp.core.response.ODPResponse;
import se.inera.odp.core.service.ODPServiceInterface;
import se.inera.odp.dbapi.model.OdpResource;
import se.inera.odp.dbapi.repository.ResourceRepository;

import static se.inera.odp.core.exception.ErrorCodes.*;

@Service
public class ODPDbApiService implements ODPServiceInterface {

    // Supported ckan query parameters
    private static final ArrayList<String> qparams = new ArrayList<>(
        Arrays.asList(
            "limit",
            "offset",
            "fields",
            "sort"
        ));

    // Not supported ckan query parameters
    private static final ArrayList<String> uparams = new ArrayList<>(
        Arrays.asList(
            "resource_id",
            "filters",
            "q",
            "distinct",
            "plain",
            "language",
            "include_total",
            "records_format"
        ));


    private ResourceRepository resourceRepository;

    @Autowired
    public void setResourceRepository(ResourceRepository resourceRepository) {
        this.resourceRepository = resourceRepository;
    }


    private ObjectMapper mapper;

    @Autowired
    public void setMapper(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    /**
     * PFÖD 2.0 style
     *
     * @deprecated
     */
    public String getResourceById(String dataset_id, String resource_name, Map<String, String> params, String auth, String url) {
        ODPResponse response = getResourceInternal(dataset_id, resource_name, params);
        try {
            if (response.getTotal() == response.getRecords().size()) {
                return mapper.writeValueAsString(response.getRecords());
            } else {
                return mapper.writeValueAsString(response);
            }
        } catch (IOException e) {
            throw new ODPException(HttpStatus.INTERNAL_SERVER_ERROR, "IOException : " + e.getMessage(), ERROR_CODE_JSON_WRITE);
        }
    }

    public List<?> getResourceAsList(String dataset_id, String resource_name, Map<String, String> params, String auth, String url) {
        ODPResponse response = getResourceInternal(dataset_id, resource_name, params);
        if (response == null) {
            return new ArrayList<String>();
        } else {
            return response.getRecords();
        }
    }

    public ODPResponse getResourceAsObject(String dataset_id, String resource_name, Map<String, String> params, String auth, String url) {
        return getResourceInternal(dataset_id, resource_name, params);
    }

    @SuppressWarnings("unchecked")
    private ODPResponse getResourceInternal(String dataset_id, String resource_name, Map<String, String> params) {
        if (!resourceRepository.existsByName(resource_name)) {
            throw new ODPException(HttpStatus.INTERNAL_SERVER_ERROR, "Resource not found!", ERROR_CODE_RESOURCE_NOT_FOUND);
        }

        List<Entry<String, String>> filters = new ArrayList<>();
        computeQuery(params, filters);

        Optional<OdpResource> req = null;
        if (!filters.isEmpty()) {
            req = getFilteredResult(resource_name, filters);
        } else {
            req = resourceRepository.findByName(resource_name);
        }

        if (!req.isPresent()) {
            throw new ODPException(HttpStatus.INTERNAL_SERVER_ERROR, "Unable to obtain resource!", ERROR_CODE_RESOURCE_UNAVAILABLE);
        }

        ODPRequest<Map<String, ?>> req1;
        try {
            req1 = mapper.readValue(req.get().getData(), ODPRequest.class);
        } catch (IOException e) {
            throw new ODPException(HttpStatus.INTERNAL_SERVER_ERROR, "IOException : " + e.getMessage(), ERROR_CODE_JSON_PARSE);
        }

        List<Map<String, ?>> result = req1.getRecords();

        if (params.get("sort") != null) {
            sorter(params.get("sort"), result);
        }

        boolean doLimit = false;
        int offset = 0;
        if (params.get("offset") != null) {
            offset = Integer.parseInt(params.get("offset"));
            if (offset < 0) {
                offset = 0;
            }
            doLimit = true;
        }

        int limit = result.size();
        int queriedLimit = limit;
        if (params.get("limit") != null) {
            queriedLimit = Integer.parseInt(params.get("limit"));
            limit = queriedLimit + offset;
            if (limit > result.size()) {
                limit = result.size();
            }
            doLimit = true;
        }

        if (doLimit) {
            result = result.subList(offset, limit);
        }

        if (params.get("fields") != null) {

            List<String> fields = Arrays
                .stream(params.get("fields").split(","))
                .collect(Collectors.toList());

            for (Map<String, ?> record : result) {

                // r.entrySet().removeIf(m -> !f.contains(m)); verkar inte funka så ...
                Iterator<String> iter = record.keySet().iterator();
                while (iter.hasNext()) {
                    if (!fields.contains(iter.next())) {
                        iter.remove();
                    }
                }
            }
        }

        ODPResponse odpResponse = new ODPResponse();
        odpResponse.setRecords(result);
        odpResponse.setOffset(offset);
        odpResponse.setLimit(queriedLimit);
        odpResponse.setTotal(req1.getRecords().size());

        return odpResponse;
    }

    private Optional<OdpResource> getFilteredResult(String resource_name, List<Entry<String, String>> filters) {
        Optional<OdpResource> req;
        if (filters.size() == 1) {
            req = resourceRepository.getFiltered(resource_name, filters.get(0).getKey(), filters.get(0).getValue());
        } else if (filters.size() == 2) {
            req = resourceRepository.getFiltered(resource_name, filters.get(0).getKey(), filters.get(0).getValue(),
                filters.get(1).getKey(), filters.get(1).getValue());
        } else if (filters.size() == 3) {
            req = resourceRepository.getFiltered(resource_name, filters.get(0).getKey(), filters.get(0).getValue(),
                filters.get(1).getKey(), filters.get(1).getValue(), filters.get(2).getKey(), filters.get(2).getValue());
        } else {
            throw new ODPException(HttpStatus.BAD_REQUEST, "Cannot filter on more than three fields. Check your query string.",
                ERROR_CODE_TO_MANY_FILTERS);
        }
        return req;
    }

    @SuppressWarnings("unchecked")
    private void sorter(String sfields, List list) {

        ArrayList<SortArray> fields = new ArrayList<>();

        for (String s : sfields.split(",")) {
            SortArray f = new SortArray(s);
            fields.add(f);

        }

        final Comparator<Map<String, String>> mapComparator = new Comparator<Map<String, String>>() {
            public int compare(Map<String, String> m1, Map<String, String> m2) {
                int value = 0;
                for (SortArray s : fields) {
                    if (s.desc) {
                        value = m2.get(s.value).compareTo(m1.get(s.value));
                    } else {
                        value = m1.get(s.value).compareTo(m2.get(s.value));
                    }
                    if (value != 0) {
                        break;
                    }
                }
                return value;

            }
        };
        Collections.sort(list, mapComparator);
    }

    static class SortArray {

        String value;
        boolean desc;

        SortArray(String s) {
            String[] s1 = s.split(" ");
            value = s1[0];
            desc = s1.length > 1 && "desc".equalsIgnoreCase(s1[1]);
        }

    }

    @SuppressWarnings("unchecked")
    public String createResource(String auth, String data) {

        ODPRequest<String> req;
        try {
            req = mapper.readValue(data, ODPRequest.class);
        } catch (IOException e) {
            throw new ODPException(HttpStatus.INTERNAL_SERVER_ERROR, "IOException : " + e.getMessage(), ERROR_CODE_JSON_PARSE);
        }

        String resourceName = req.getResource().getName();
        String datasetName = req.getResource().getPackageId();

        Optional<OdpResource> r = resourceRepository.findByName(resourceName);
        OdpResource odpResource;

        if (r.isPresent()) {
            odpResource = r.get();
        } else {
            odpResource = new OdpResource();
            odpResource.setName(resourceName);
            odpResource.setDataset(datasetName);
        }
        odpResource.setData(data);

        resourceRepository.saveAndFlush(odpResource);

        return "{\"success\": true}";

    }

    @Transactional
    public String deleteResource(String auth, String dataset_id, String resource_name) {
        resourceRepository.deleteByName(resource_name);
        return "{\"success\": true}";
    }

    @Transactional
    public String deleteResource(String auth, String dataset_id, String resource_name, String field, String row_id) {
        if (!resourceRepository.recordExists(resource_name, field, row_id)) {
            throw new ODPException(HttpStatus.INTERNAL_SERVER_ERROR, "Record to be deleted does not exists! " + field + ":" + row_id,
                ERROR_CODE_RECORD_NOT_FOUND);
        }
        resourceRepository.deleteByField(resource_name, field, row_id);
        return "{\"success\": true}";
    }

    @Transactional
    public String updateResource(String auth, String dataset_id, String resource_name, String data) {

        if (!resourceRepository.existsByName(resource_name)) {
            throw new ODPException(HttpStatus.INTERNAL_SERVER_ERROR, "Resource not found!", ERROR_CODE_RESOURCE_NOT_FOUND);
        }

        try {
            int recordsIndex = data.indexOf("\"records\"");
            if (recordsIndex == -1) { //Assume single post
                updateSingle(resource_name, data);
            } else { //Assume multiple posts
                updateAll(resource_name, data);
            }
        } catch (IOException e) {
            throw new ODPException(HttpStatus.INTERNAL_SERVER_ERROR, "IOException : " + e.getMessage(), ERROR_CODE_JSON_PARSE);
        }

        return "{\"success\": true}";
    }

    @SuppressWarnings("unchecked")
    private void updateSingle(String resource_name, String data) throws IOException {
        Map<String, ?> map = mapper.readValue(data, Map.class);
        String pk = resourceRepository.getPrimaryKeyForResource(resource_name);
        String key = (String) map.get(pk);

        if (resourceRepository.recordExists(resource_name, pk, key)) {
            resourceRepository.updateByField(resource_name, pk, key, data);
        } else {
            resourceRepository.insertRecord(resource_name, data);
        }
    }

    @SuppressWarnings("unchecked")
    private void updateAll(String resource_name, String data) throws IOException {
        Map<String, ?> dataMap = mapper.readValue(data, Map.class);

        //Get list of records from data
        List<Map<String, ?>> dataRecords = (List<Map<String, ?>>) dataMap.get("records");

        //Get list of records from db
        Optional<OdpResource> optReq = resourceRepository.findByName(resource_name);

        if (!optReq.isPresent()) {
            throw new ODPException(HttpStatus.INTERNAL_SERVER_ERROR, "Unable to obtain resource!", ERROR_CODE_RESOURCE_UNAVAILABLE);
        }

        OdpResource req = optReq.get();
        String dbData = req.getData();

        String pk = resourceRepository.getPrimaryKeyForResource(resource_name);

        Map<String, List<Map<String, ?>>> dbDataMap = mapper.readValue(dbData, Map.class);
        List<Map<String, ?>> dbRecords = dbDataMap.get("records");
        List<String> existingIds = dbRecords.stream().map(e -> (String) e.get(pk)).collect(Collectors.toList());

        //Find and update existing
        for (Map<String, ?> record : dataRecords) {
            String id = (String) record.get(pk);
            if (existingIds.contains(id)) {
                Map<String, ?> obj = dbRecords.stream().filter(e -> e.get(pk).equals(id)).findFirst().get();
                int idx = dbRecords.indexOf(obj);
                dbRecords.set(idx, record);
            } else {
                dbRecords.add(record);
            }
        }

        dbDataMap.replace("records", dbRecords);
        String str = mapper.writeValueAsString(dbDataMap);
        req.setData(str);
        resourceRepository.saveAndFlush(req);
    }

    /*
     * Private methods
     */
    private void computeQuery(Map<String, String> params, List<Entry<String, String>> filters) {
        try {
            Iterator<Map.Entry<String, String>> iter = params.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry<String, String> entry = iter.next();
                if (uparams.contains(entry.getKey())) {
                    iter.remove();
                } else if (!qparams.contains(entry.getKey())) {
                    entry.setValue(URLDecoder.decode(entry.getValue(), "UTF-8"));
                    filters.add(entry);
                    iter.remove();
                }
            }

        } catch (IOException e) {
            throw new ODPException(HttpStatus.BAD_REQUEST, "IOException : " + e.getMessage(), ERROR_CODE_URL_PARSING);
        }
    }
}
