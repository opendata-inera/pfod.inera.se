package se.inera.odp.dbapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages= {"se.inera.odp.dbapi", "se.inera.odp.dbapi.*", "se.inera.odp.core.*"})
@EnableJpaRepositories("se.inera.odp.dbapi.repository")
public class OdpDbApplication {

    public static void main(String[] args) {
        SpringApplication.run(OdpDbApplication.class, args);
    }
    
}
