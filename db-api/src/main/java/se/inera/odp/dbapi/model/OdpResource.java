package se.inera.odp.dbapi.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;

	@Entity
	@Table(name = "odp_resource", schema= "public")
	@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
	public class OdpResource implements Serializable {

		private static final long serialVersionUID = 4487281770344621102L;

		/*
		 *     id oid NOT NULL,
    name character varying(100) NOT NULL,
    dataset character varying(100) NOT NULL,
    data jsonb
		 */
	    @Id
	    @GeneratedValue(strategy = GenerationType.SEQUENCE)
	    private Integer id;

	    @Column(length = 32, nullable = false)
	    private String name;

	    @Column(length = 100, nullable = false)
	    private String dataset;

	    @Type(type = "jsonb")
	    @Column(columnDefinition = "jsonb")
	    private String data;

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getDataset() {
			return dataset;
		}

		public void setDataset(String dataset) {
			this.dataset = dataset;
		}

		public String getData() {
			return data;
		}

		public void setData(String data) {
			this.data = data;
		}

	}

