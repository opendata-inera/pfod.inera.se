package se.inera.odp.dbapi.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import org.springframework.transaction.annotation.Transactional;
import se.inera.odp.dbapi.model.OdpResource;

public interface ResourceRepository extends JpaRepository<OdpResource, Integer> {

    public Optional<OdpResource> findByName(String name);

    public Long deleteByName(String name);

    @Query(value = "SELECT data->>'records' FROM public.odp_resource r WHERE r.name = :name", nativeQuery = true)
    public List<Object> findByName2(@Param("name") String name);

    public boolean existsByName(String name);

    @Query(value = "SELECT id, name, dataset, jsonb_build_object('records',jsonb_agg(records.value)) as data\n"
        + "\tFROM public.odp_resource r, jsonb_array_elements(r.data->'records') as records\n"
        + "\twhere r.name = :name AND records->>:key = :value\n"
        + "\tgroup by id, name, dataset", nativeQuery = true)
    Optional<OdpResource> getFiltered(@Param("name") String name, @Param("key") String key, @Param("value") String value);

    @Query(value = "SELECT id, name, dataset, jsonb_build_object('records',jsonb_agg(records.value)) as data\n"
        + "\tFROM public.odp_resource r, jsonb_array_elements(r.data->'records') as records\n"
        + "\twhere r.name = :name AND records->>:key = :value AND records->>:key2 = :value2\n"
        + "\tgroup by id, name, dataset", nativeQuery = true)
    Optional<OdpResource> getFiltered(@Param("name") String name, @Param("key") String key, @Param("value") String value,
        @Param("key2") String key2, @Param("value2") String value2);

    @Query(value = "SELECT id, name, dataset, jsonb_build_object('records',jsonb_agg(records.value)) as data\n"
        + "\tFROM public.odp_resource r, jsonb_array_elements(r.data->'records') as records\n"
        + "\twhere r.name = :name AND records->>:key = :value AND records->>:key2 = :value2 AND records->>:key3 = :value3\n"
        + "\tgroup by id, name, dataset", nativeQuery = true)
    Optional<OdpResource> getFiltered(@Param("name") String name, @Param("key") String key, @Param("value") String value,
        @Param("key2") String key2, @Param("value2") String value2, @Param("key3") String key3, @Param("value3") String value3);

    @Transactional
    @Modifying
    @Query(value = "WITH q as (\n"
        + "SELECT id, jsonb_agg(records) as records\n"
        + "FROM public.odp_resource r, jsonb_array_elements(r.data->'records') as records\n"
        + "WHERE r.name = :name AND records->>:field <> :row_id\n"
        + "GROUP BY id, dataset, name\n"
        + ")\n"
        + "UPDATE public.odp_resource\n"
        + "SET data = jsonb_set(public.odp_resource.data, '{records}', q.records)\n"
        + "FROM q\n"
        + "WHERE public.odp_resource.id = q.id", nativeQuery = true)
    Integer deleteByField(@Param("name") String resource_name, @Param("field") String field, @Param("row_id") String row_id);

    @Transactional
    @Modifying
    @Query(value = "WITH q as (\n"
        + "SELECT r.id, CAST(('{records,'||index-1||'}') as text[]) as path\n"
        + "FROM public.odp_resource r, jsonb_array_elements(r.data->'records') with ordinality arr(record, index)\n"
        + "WHERE r.name = :name AND record->>:field = :row_id )\n"
        + "UPDATE public.odp_resource\n"
        + "SET data = jsonb_set(data, q.path, CAST(:data as jsonb))\n"
        + "FROM q\n"
        + "WHERE public.odp_resource.id = q.id", nativeQuery = true)
    Integer updateByField(@Param("name") String resource_name, @Param("field") String field, @Param("row_id") String row_id,
        @Param("data") String data);

    @Query(value="SELECT data->>'primary_key' as key\n"
        + "FROM public.odp_resource\n"
        + "WHERE name = :name", nativeQuery = true)
    String getPrimaryKeyForResource(@Param("name") String resource_name);

    @Transactional
    @Modifying
    @Query(value = "UPDATE odp_resource\n"
        + "SET data = jsonb_insert(data, '{records,0}', cast(:data as jsonb))\n"
        + "WHERE name = :name", nativeQuery = true)
    Integer insertRecord(@Param("name") String resource_name, @Param("data") String data);

    @Query(value = "SELECT EXISTS (\n"
        + "SELECT records.value\n"
        + "FROM odp_resource r, jsonb_array_elements(r.data->'records') as records\n"
        + "WHERE r.name = :name AND records.value->>:field = :row_id)", nativeQuery = true)
    boolean recordExists(@Param("name") String resource_name, @Param("field") String pk, @Param("row_id") String key);
}