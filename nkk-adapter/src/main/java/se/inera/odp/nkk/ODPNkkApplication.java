package se.inera.odp.nkk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages= {"se.inera.odp.nkk", "se.inera.odp.nkk.*", "se.inera.odp.core.*"})
public class ODPNkkApplication {

 
    public static void main(String[] args) {
        SpringApplication.run(ODPNkkApplication.class, args);
    }
    
}
