package se.inera.odp.nkk.service;

public enum NkkResourceEnum {
	documents("id"),
	publications_documents("documentId"),
	publications_organizations("root");

	private String pk;
	
	
	private NkkResourceEnum(String pk) {
		this.pk = pk;
	}


	public String getPk() {
		return pk;
	}

	public static NkkResourceEnum get(NkkPublicationsEnum resource_id) {
		if(resource_id == null)
			return NkkResourceEnum.documents;
		else
			return NkkResourceEnum.valueOf("publications_" + resource_id.toString());
	}
}
