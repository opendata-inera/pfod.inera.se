package se.inera.odp.nkk.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import se.inera.odp.core.response.ODPResponse;
import se.inera.odp.core.response.ResponseMessage;
import se.inera.odp.core.utils.KeyManager;
import se.inera.odp.core.utils.ResourceValidator;
import se.inera.odp.core.utils.ResponseLoggerMapper;
import se.inera.odp.nkk.service.NkkAdapterService;
import se.inera.odp.nkk.service.NkkPublicationsEnum;
import se.inera.odp.nkk.service.NkkResourceEnum;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@RestController
@RequestMapping({""})
@Api(value = "nkk", tags = {"Nationellt kliniskt kunskapsstöd"}, produces = "application/json", consumes = "application/json")
public class NkkAdapterController {

    @Autowired
    NkkAdapterService nkkAdapterService;

    @Autowired
    private ResponseLoggerMapper responseMapper;

    @Autowired
    private KeyManager keyManager;

    @Autowired
    private ResourceValidator validator;

    @GetMapping(value = {"/{version:v1|v2|v3}/publications/{resource_id}"}, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Returnerar ett objekt med publikationer av typen 'resource_id'", response = ODPResponse.class)
    public ResponseEntity<String> getResourceById(
        @RequestHeader(value = AUTHORIZATION, required = false) String auth,
        @PathVariable String version,
        @PathVariable NkkPublicationsEnum resource_id,
        @RequestParam(required = false) Integer limit,
        @RequestParam(required = false) Integer offset,
        @RequestParam(required = false) String fields,
        @RequestParam(required = false) String sort,
        @RequestParam(required = false) Map<String, String> params) {

        String result = nkkAdapterService
            .getResourceById(version, NkkResourceEnum.get(resource_id), params, keyManager.getDatasetKeyForRead(auth));
        responseMapper.responseMessage(HttpStatus.OK, "OK", 1);
        return new ResponseEntity<String>(result, HttpStatus.OK);
    }

	/*
	@ApiOperation(value="Returnerar ett objekt med dokument av typen 'resource_id'", response=ODPResponse.class)
	@GetMapping(value= {"/{version:v1|v2|v3}/documents", "/{version:v1|v2|v3}/documents/{id}"})
	public ResponseEntity<String> getResourceById(
			@RequestHeader(value=AUTHORIZATION, required=false ) String auth, 
			@PathVariable String version, 
			@PathVariable(required=false) String id,
			@RequestParam(required=false) Integer limit,
			@RequestParam(required=false) Integer offset,
			@RequestParam(required=false) String fields,
			@RequestParam(required=false) String sort,
			@RequestParam(required=false) Map<String,String> params) {
					
		String result = nkkAdapterService.getResourceById(version, NkkResourceEnum.get(null), id, params, keyManager.getDatasetKeyForRead(auth));
		responseMapper.responseMessage(HttpStatus.OK, "OK");
		return new ResponseEntity<String>(result, HttpStatus.OK);
	}
	*/

    @ApiOperation(value = "Returnerar ett objekt med dokument av typen 'resource_id'", response = ODPResponse.class)
    @GetMapping(value = {"/{version:v1|v2|v3}/documents",
        "/{version:v1|v2|v3}/documents/{id}"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getResourceById(
        @RequestHeader(value = AUTHORIZATION, required = false) String auth,
        @PathVariable String version,
        @PathVariable(required = false) String id,
        @RequestParam(required = false) Integer limit,
        @RequestParam(required = false) Integer offset,
        @RequestParam(required = false) String fields,
        @RequestParam(required = false) String sort,
        @RequestParam(required = false) Map<String, String> params) {

        ODPResponse result = nkkAdapterService
            .getResourceAsObject(version, NkkResourceEnum.get(null), id, params, keyManager.getDatasetKeyForRead(auth));
        responseMapper.responseMessage(HttpStatus.OK, "OK", result.getTotal());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @ApiOperation(value = "Returnerar en lista med dokument av typen 'resource_id'", response = List.class)
    @GetMapping(value = {"/{version:v1|v2|v3}/documents/list",
        "/{version:v1|v2|v3}/documents/{id}/list"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List> getResourceAsList(
        @RequestHeader(value = AUTHORIZATION, required = false) String auth,
        @PathVariable String version,
        @PathVariable(required = false) String id,
        @RequestParam(required = false) Integer limit,
        @RequestParam(required = false) Integer offset,
        @RequestParam(required = false) String fields,
        @RequestParam(required = false) String sort,
        @RequestParam(required = false) Map<String, String> params) {

        List<?> result = nkkAdapterService
            .getResourceAsList(version, NkkResourceEnum.get(null), id, params, keyManager.getDatasetKeyForRead(auth));
        responseMapper.responseMessage(HttpStatus.OK, "OK", result.size());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * Example POST /api/kik/v1/codes (delete all -> insert)
     */
    @ApiOperation(value = "Skapar eller uppdaterar en resurs av typen 'resource_id'", response = ResponseMessage.class)
    @PostMapping(value = {"/{version:v1|v2|v3}/documents",
        "/{version:v1|v2|v3}/publications/{resource_id}"}, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseMessage> createData(@RequestHeader(value = "Authorization", required = false) String auth,
        @PathVariable String version,
        @PathVariable(required = false) NkkPublicationsEnum resource_id,
        @RequestBody List<Object> data,
        @RequestHeader HttpHeaders headers) {

        int size = nkkAdapterService.saveResource(keyManager.getDatasetKeyForWrite(auth), version, NkkResourceEnum.get(resource_id), data);
        return new ResponseEntity<>(responseMapper.responseMessage(HttpStatus.CREATED, "OK", size), HttpStatus.CREATED);
    }

    /**
     * Example DELETE /api/kik/v1/codes/{id}
     */
    @ApiOperation(value = "Tar bort en post med nyckel 'id' från resurs av typen 'resource_id'", response = ResponseMessage.class)
    @DeleteMapping(value = {"/{version:v1|v2|v3}/documents/{id}",
        "/{version:v1|v2|v3}/publications/{resource_id}/{id}"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseMessage> deleteResource(
        @RequestHeader(value = AUTHORIZATION) String auth,
        @PathVariable String version, @PathVariable NkkPublicationsEnum resource_id, @PathVariable String id) throws IOException {

        nkkAdapterService.deleteResource(keyManager.getDatasetKeyForWrite(auth), version, NkkResourceEnum.get(resource_id), id);
        return new ResponseEntity<>(responseMapper.responseMessage(HttpStatus.OK, "OK"), HttpStatus.OK);
    }

    @ApiOperation(value = "Uppdaterar 'resource_id' från en lista med poster", response = ResponseMessage.class)
    @PutMapping(value = {"/{version:v1|v2|v3}/documents", "/{version:v1|v2|v3}/publications/{resource_id}"})
    public ResponseEntity<ResponseMessage> updateResource(
        @RequestHeader(value = AUTHORIZATION) String auth,
        @PathVariable String version, @PathVariable NkkPublicationsEnum resource_id,
        @RequestBody List<Object> data) throws IOException {

        int size = nkkAdapterService
            .updateResource(keyManager.getDatasetKeyForWrite(auth), version, NkkResourceEnum.get(resource_id), data);
        return new ResponseEntity<>(responseMapper.responseMessage(HttpStatus.OK, "OK", size), HttpStatus.OK);
    }

    /**
     * Example POST /api/kik/v1/codes/{id} (update(if exists) else insert)
     */
    @ApiOperation(value = "Uppdaterar en post med nyckel 'id' från resurs av typen 'resource_id'", response = ResponseMessage.class)
    @RequestMapping(value = {"/{version:v1|v2|v3}/documents/{id}", "/{version:v1|v2|v3}/publications/{resource_id}/{id}"}, method = {
        RequestMethod.POST, RequestMethod.PUT}, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseMessage> updateResource(
        @RequestHeader(value = AUTHORIZATION) String auth,
        @PathVariable String version, @PathVariable NkkPublicationsEnum resource_id, @PathVariable String id,
        @RequestBody String data) throws IOException {

        nkkAdapterService.updateResource(keyManager.getDatasetKeyForWrite(auth), version, NkkResourceEnum.get(resource_id), id, data);
        return new ResponseEntity<ResponseMessage>(responseMapper.responseMessage(HttpStatus.OK, "OK", 1), HttpStatus.OK);
    }
}
