# Logstash filter
### Test filter
Uncomment necessary lines in conf-file to setup input and output.

Correct the path to the log files used for test.
  
Run the command 'logstash.bat -f logstash-adapterfilter.conf' standing in this directory. 
Depending on how you setup up logstash you might need to add the path to the logstash binaries. 


### Deliver
The conf-file contains everything necessary to deliver. Input and output fields should not be active.