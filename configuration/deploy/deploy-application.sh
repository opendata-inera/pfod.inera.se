#!/usr/bin/env bash

set -e

NAMESPACE=$1
APPLICATION=$2
IMAGE_VERSION=$3

INFRASTRUCTURE_BRANCH=$(git symbolic-ref --short HEAD | sed -e "s/^heads\///")
INFRASTRUCTURE_COMMIT=$(git log -n 1 --pretty=format:'%h')
DEPLOYMENT_TIMESTAMP=$(date +%Y-%m-%dT%H-%M-%S)

cd $APPLICATION

mkdir -p tmp

oc project $NAMESPACE

oc process -f ./$APPLICATION.yml     --param NAMESPACE="$NAMESPACE" \
                                     --param IMAGE_VERSION="$IMAGE_VERSION" \
                                     --param INFRASTRUCTURE_BRANCH="$INFRASTRUCTURE_BRANCH" \
                                     --param INFRASTRUCTURE_COMMIT="$INFRASTRUCTURE_COMMIT" \
                                     --param DEPLOYMENT_TIMESTAMP="$DEPLOYMENT_TIMESTAMP" \
                                     --param-file="./$NAMESPACE/template-params.env" > tmp/odp-resolved-template.yml

oc apply -f "./$NAMESPACE/config-maps.yml"
oc label -f "./$NAMESPACE/config-maps.yml" --all deployment-timestamp="$DEPLOYMENT_TIMESTAMP" infrastructure-branch="$INFRASTRUCTURE_BRANCH" infrastructure-commit="$INFRASTRUCTURE_COMMIT"  --overwrite=true
oc apply -f "tmp/odp-resolved-template.yml"