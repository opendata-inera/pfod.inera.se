# Dockerized Solr
This folder contains everything needed to start a Solr docker container.

## Solr version and CKAN compatibility

|Solr version|CKAN version|tested|working|
|---|---|---|---|
| 6.6.2 | 2.8.0 | yes | yes |

## Directory structure

|path|description|
|---|---|
|conf/|Contains certain configuration files for solr version 6.6.2.|
|ckan/| Contains CKAN 2.8.0 specific schema and solrconfig files.|
|Dockerfile|The dockerfile used to build the Solr image.|
